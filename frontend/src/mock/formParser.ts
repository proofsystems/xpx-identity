export const formParser: any = {
    fullName: 'Full name',
    sex: 'Sex',
    dateOfBirth: 'Date of birth',
    placeOfBirth: 'Place of birth',
    bloodType: 'Blood type',
    address: 'Address',
    permanentAddress: 'Permanent address',
    presentAddress: 'Present address',
    filipinoOrResidentAlien: 'Filipino or Resident Alien',
    maritalStatus: 'Marital Status',
    mobileNumber: 'Mobile Number',
    email: 'Email',
    id: 'ID'
};
