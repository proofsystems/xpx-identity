import { IFormControl } from '../models';
import {calculateAge} from "../functions";

export const form: IFormControl[] = [
    {
        id: 'fullName',
        label: 'Full name',
        type: 'text',
        validator: (value) => {
            return !!value;
        }
    },
    {
        id: 'sex',
        label: 'Sex',
        type: 'select',
        options: ['Male', 'Female'],
        validator: () => true
    },
    {
        id: 'dateOfBirth',
        label: 'Date of birth',
        placeholder: 'Age should be biggest then 14',
        type: 'date',
        validator: (value) => {
            const age = calculateAge(new Date(value));
            return age > 14;
        }
    },
    {
        id :'placeOfBirth',
        label: 'Place of birth',
        type: 'text',
        validator: (value) => {
            return !!value;
        }
    },
    {
        id: 'bloodType',
        label: 'Blood type\'',
        placeholder: "Should be in ABO blood group system",
        type: 'select',
        options: ['A+', 'A-', 'B+', 'B-', 'O+', 'O-', 'AB+', 'AB-'],
        validator: () => true
    },
    {
        id: 'address',
        label: 'Address',
        type: 'text',
        validator: (value) => {
            return !!value;
        }
    },
    {
        id: 'permanentAddress',
        label: 'Permanent address',
        type: 'text',
        validator: (value) => {
            return !!value;
        }
    },
    {
        id: 'presentAddress',
        label: 'Present address',
        type: 'text',
        validator: (value) => {
            return !!value;
        }
    },
    {
        id: 'filipinoOrResidentAlien',
        label: 'Filipino or Resident Alien',
        type: 'select',
        options: ['Filipino', 'Resident Alien'],
        validator: () => true
    },
    {
        id: 'maritalStatus',
        label: 'Marital Status',
        type: 'select',
        options: ['Married', 'Widowed', 'Divorced', 'Single'],
        validator: () => true
    },
    {
        id: 'mobileNumber',
        label: 'Mobile Number',
        placeholder: 'check number in https://regex101.com/r/fX2bZ8/1',
        type: 'text',
        validator: (value) => {
            return /^(\+\d{1,3}[- ]?)?\d{10}$/.test(value);
        }
    },
    {
        id: 'email',
        label: 'Email address',
        type: 'email',
        validator: (value) => {
            return /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/.test(value);
        }
    },
];
