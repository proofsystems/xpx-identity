import { IUser } from '../models';

export const users: IUser[] = [
    {
        id: '123567',
        fullName: 'Test User 1',
        address: 'test address',
        bloodType: 'IV',
        emailAddress: 'test@test.com',
        mobileNumber: '08000800800'
    },
    {
        id: '124567',
        fullName: 'Test User 2',
        address: 'test address',
        permanentAddress: 'test',
        bloodType: 'IV',
        emailAddress: 'test@test.com',
        mobileNumber: '08000800800'
    },
    {
        id: '123567',
        fullName: 'Test User 3',
        address: 'test address',
        bloodType: 'IV',
        emailAddress: 'test@test.com',
        mobileNumber: '08000800800'
    },
    {
        id: '123567',
        fullName: 'Test User 4',
        address: 'test address',
        permanentAddress: 'test',
        bloodType: 'IV',
        emailAddress: 'test@test.com',
        mobileNumber: '08000800800'
    },
    {
        id: '123567',
        fullName: 'Test User 5',
        address: 'test address',
        bloodType: 'IV',
        emailAddress: 'test@test.com',
        mobileNumber: '08000800800'
    },
    {
        id: '123567',
        fullName: 'Test User 6',
        address: 'test address',
        bloodType: 'IV',
        emailAddress: 'test@test.com',
        mobileNumber: '08000800800'
    },    {
        id: '123567',
        fullName: 'Test User 7',
        address: 'test address',
        bloodType: 'IV',
        emailAddress: 'test@test.com',
        mobileNumber: '08000800800'
    },    {
        id: '123567',
        fullName: 'Test User 8',
        address: 'test address',
        bloodType: 'IV',
        emailAddress: 'test@test.com',
        mobileNumber: '08000800800'
    },    {
        id: '123567',
        fullName: 'Test User 9',
        address: 'test address',
        bloodType: 'IV',
        emailAddress: 'test@test.com',
        mobileNumber: '08000800800'
    },
    {
        id: '123567',
        fullName: 'Test User 10',
        address: 'test address',
        bloodType: 'IV',
        emailAddress: 'test@test.com',
        mobileNumber: '08000800800'
    },
    {
        id: '123567',
        fullName: 'Test User 11',
        address: 'test address',
        bloodType: 'IV',
        emailAddress: 'test@test.com',
        mobileNumber: '08000800800'
    },
    {
        id: '123567',
        fullName: 'Test User 12',
        permanentAddress: 'test',
        address: 'test address',
        bloodType: 'IV',
        emailAddress: 'test@test.com',
        mobileNumber: '08000800800'
    },
    {
        id: '123567',
        fullName: 'Test User 13',
        address: 'test address',
        bloodType: 'IV',
        emailAddress: 'test@test.com',
        mobileNumber: '08000800800'
    },

    {
        id: '123567',
        fullName: 'Test User 14',
        address: 'test address',
        bloodType: 'IV',
        emailAddress: 'test@test.com',
        mobileNumber: '08000800800'
    },{
        id: '123567',
        fullName: 'Test User 15',
        address: 'test address',
        bloodType: 'IV',
        emailAddress: 'test@test.com',
        mobileNumber: '08000800800'
    },


];
