import React, { Component, Fragment } from 'react';

import Button from '../Button';
import { ws } from "../../functions";
import './index.scss';
import Modal from "../Modal";
import {Hand, HandsFingers, IFingerprints} from "../../models";
import ShowFingerprints from "../ShowFingerprints";

interface IMatchFingerProps {
    getResult: (r: boolean) => void;
    fingerprint: HandsFingers;
}

interface IMatchFingerState {
    fp: string,
    error: string;
    isClicked: boolean
}

class MatchFinger extends Component<IMatchFingerProps, IMatchFingerState> {
    state = {
        fp: "",
        error: "",
        isClicked: false,
    };

    componentDidMount(): void {
        ws.addEventListener("message", this.subscribeOnSocket);
    }

    componentWillUnmount(): void {
        ws.removeEventListener("message", this.subscribeOnSocket);
    }

    openModal = (fp: string) => {
        this.setState((state) => ({
            ...state,
            fp,
            isClicked: false,
        }))
    };

    subscribeOnSocket = (ev: any) => {
        if (ev.data === 'Hello') {
            console.log('Websocket connected');
        } else {
            const { result, status } = JSON.parse(ev.data);
            if (status === "Successfull Reading")
                this.openModal(result);
            else if (status === "Successfull Matching")
                if (result) {
                    this.props.getResult(result);
                } else {
                    console.log("I get result is equal to", result);
                    this.setState((state) => ({
                        ...state,
                        fp: "",
                        error: "Fingerprint does not match.",
                        isClicked: false,
                    }))
                }
            else {
                console.log("Error with getting fingerprint", status);
                this.setState((state) => ({
                    ...state,
                    isClicked: false,
                    error: "Error occurred, try again"
                }))
            }
        }
    };

    takeFingerprint = () => {
        this.setState({ isClicked: true });
        const data = {
            jsonrpc: "2.0",
            method: "read_fingerprint",
            id: "1"
        };
        if (ws.readyState === 1) {
            ws.send(JSON.stringify(data));
            return;
        }
    };

    matchFingerprint = () => {
        const data = {
            jsonrpc: "2.0",
            method: "match_fingerprint",
            params: {
              proof: this.state.fp,
              verify: [
                  ...this.props.fingerprint[Hand.Left].map((v: IFingerprints) => v.print),
                  ...this.props.fingerprint[Hand.Right].map((v: IFingerprints) => v.print),
              ]
            },
            id: "1"
        };
        if (ws.readyState === 1) {
            ws.send(JSON.stringify(data));
            return;
        }
    };

    closeModal = () => {
        this.setState((state) => ({
            ...state,
            fp: "",
            isClicked: false,
        }));
    };

    changeFingerprint = () => {
        this.takeFingerprint();
        this.setState((state) => ({
            ...state,
            fp: "",
            isClicked: true,
        }));
    };

    render(): React.ReactNode {
        const { fp, error, isClicked } = this.state;
        return (
            <Fragment>
                {
                    fp ? (
                        <Modal onClose={this.closeModal}>
                            <div className="fl">
                                <div className="fb">
                                    <img src={`data:image/png;base64,${fp}`} alt="photo" width={400}/>
                                    <ShowFingerprints fingerprint={this.props.fingerprint} isBig={true} />
                                </div>
                                <div>
                                    <Button label={"Match a fingerprint"} onClick={this.matchFingerprint} />
                                    <Button label={"Change a fingerprint"} onClick={this.changeFingerprint} />
                                </div>
                            </div>
                        </Modal>
                    ) : (
                        <>
                            {
                                error && !isClicked && <div className="box-error">{error}</div>
                            }
                            {
                                isClicked ? (
                                    <div>Please bring your finger to the scanner</div>
                                ) : (
                                    <Button onClick={this.takeFingerprint} label="Take a fingerprint for matching" />
                                )
                            }
                        </>
                    )
                }
            </Fragment>
        );
    }
}

export default MatchFinger;
