import React from 'react';
import Loader from 'react-loader-spinner';
import './index.scss';

export default () => (
    <div className="global-loader">
        <Loader type="Circles" color="#1c9f97" height={30} width={30} />  
    </div>
)