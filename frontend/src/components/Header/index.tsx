import React from 'react';
import './index.scss';

import Button from '../Button';

import logo from '../../assets/img/proxima.png';
import { connect } from 'react-redux';
import { getAuthState } from '../../store/selectors/login';
import {logout} from "../../store/actions";
import {Link} from "react-router-dom";

interface IHeaderProps {
    logout: () => void;
    isLoggedIn: boolean;
}

const Header = ( { logout, isLoggedIn }: IHeaderProps ) => (
    <div className="header">
        <Link to="/all_users">
            <img src={logo} alt="logo"  />
        </Link>
        {
            isLoggedIn && (
                <Button onClick={logout} label='Logout' />
            )
        }
    </div>
);

export default connect(getAuthState, { logout })(Header);
