import React, { Component, Fragment } from 'react';

import Button from '../Button';
import { ws } from "../../functions";
import './index.scss';

interface IFingerprintProps {
    getFingerprint: (fp: string) => void;
}

interface IFingerprintState {
    isClicked: boolean
}

class Fingerprint extends Component<IFingerprintProps, IFingerprintState> {
    state = {
        isClicked: false
    };

    componentWillUnmount(): void {
        ws.removeEventListener("message", this.subscribeOnSocket);
    }

    subscribeOnSocket = (ev: any) => {
        if (ev.data === 'Hello') {
            console.log('Websocket connected');
        } else {
            const { result, status } = JSON.parse(ev.data);
            if (status === "Successfull Reading") {
                this.props.getFingerprint(result);
            }
            else {
                console.log("Error with getting fingerprint");
            }
            this.setState({ isClicked: false });
            ws.removeEventListener("message", this.subscribeOnSocket);
        }
    };

    takeFingerprint = () => {
        ws.addEventListener("message", this.subscribeOnSocket);
        this.setState({ isClicked: true });
        const data = {
            jsonrpc: "2.0",
            method: "read_fingerprint",
            id: "1"
        };
        if (ws.readyState === 1) {
            ws.send(JSON.stringify(data));
            return;
        }
    };

    render(): React.ReactNode {
        return (
            <Fragment>
                { this.state.isClicked ? (
                    <div>Please bring your finger to the scanner</div>
                ): (
                    <Button onClick={this.takeFingerprint} label="Take a fingerprint" />
                )}
            </Fragment>
        );
    }
}

export default Fingerprint;
