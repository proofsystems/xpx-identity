import React, { Component, ChangeEvent, Fragment } from 'react';
import { IFormControl } from '../../models';

import Button from '../Button';
import { formParser } from '../../mock/formParser';

import './index.scss';

interface IFromBuilderProps {
    form: IFormControl[];
    onSubmit: (formObject: { [p: string]: any }) => void;
    formState: any;
}

interface IFormBuilderState {
    form: {
        [p: string]: any;
    };
    formErrors: {
        [p: string]: boolean;
    };
    formValid: boolean;
}

class FormBuilder extends Component<IFromBuilderProps, IFormBuilderState> {
    state: IFormBuilderState = {
        form: {},
        formErrors: {},
        formValid: true
    };

    componentDidMount(): void {
        const defaultValues = Object.getOwnPropertyNames(this.props.formState).length !== 0 ?
            this.props.formState
            :
            this.props.form.reduce((prev, curr) => ({
                ...prev,
                [curr.id]: curr.type === "select" ? curr.options![0] : ""
            }), {});

        this.setState((state) => ({
            ...state,
            form: defaultValues
        }));
    }

    onSubmit = async () => {
        if (await this._checkFormValidity()) {
            this.props.onSubmit(this.state.form)
        }
    };

    onChange = (event: ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
        const { name, value } = event.target;
        this.setState((state) => ({
            ...state,
            form: {
                ...state.form,
                [name]: value,
            }
        }))
    };

    onBlur = (event: any, id: string, validator: any) => {
        this.setState((state) => ({
            ...state,
            formErrors: {
                ...state.formErrors,
                [id]: !validator(state.form[id])
            }
        }))
    };

    private _checkFormValidity = async () => {
        const errors = this.props.form.reduce((prev, curr) => ({
            ...prev,
            [curr.id]: !curr.validator(this.state.form[curr.id])
        }), {});

        await this.setState((state) => ({
            ...state,
            formErrors: errors
        }));

        return Object
            .keys(this.state.formErrors)
            .filter((key: string) => this.state.formErrors[key]).length === 0;
    };

    render(): React.ReactNode {
        const { form } = this.props;

        return (
            <Fragment>
                <div className="form_wrapper">
                    <form className="form_builder">
                        {
                            this.state && form.map((fc, i) => (
                                <div className="form_control" key={i}>
                                    <label>{fc.label}
                                        {
                                            fc.type !== "select" && (
                                                <div className="input_control">
                                                    <input type={fc.type}
                                                           className={this.state.formErrors[fc.id] ? "invalid" : "valid"}
                                                           onChange={this.onChange}
                                                           onBlur={e => this.onBlur(e, fc.id, fc.validator)}
                                                           name={fc.id}
                                                           id={fc.id}
                                                           value={this.state.form[fc.id]}
                                                    />
                                                    {
                                                        this.state.formErrors[fc.id] && (
                                                            <span className="required">
                                                                 Value "{formParser[fc.id]}" is Required!
                                                            </span>
                                                        )
                                                    }
                                                </div>
                                            )
                                        }
                                        {
                                            fc.type === "select" && (
                                                <select
                                                  className={this.state.formErrors[fc.id] ? "invalid" : "valid"}
                                                  onChange={this.onChange}
                                                  onBlur={e => this.onBlur(e, fc.id, fc.validator)}
                                                  name={fc.id}
                                                  value={this.state.form[fc.id] || fc.options![0]}
                                                >
                                                    {fc.options!.map((item: string) => (
                                                        <option value={item} id={item}>{item.toUpperCase()}</option>
                                                    ))}
                                                </select>
                                            )
                                        }
                                    </label>
                                    {
                                        fc.placeholder && (
                                            <span className="placeholder">{fc.placeholder}</span>
                                        )
                                    }
                                </div>
                            ))
                        }
                    </form>
                </div>
                <div className="button_group">
                    {this.props.children}
                    <Button onClick={this.onSubmit} label="Continue">Continue</Button>
                </div>
            </Fragment>
        )
    }
}

export default FormBuilder;
