import React, {Component, Fragment} from 'react';
import {Actions, IUserKeys, Owner, WsStatuses} from '../../models';
import { ws } from "../../functions";

interface ICardReaderProps {
    onRead: (userKeys: IUserKeys) => void;
    action: Actions
    owner: Owner
}

interface ICardReaderState {
    privateKey: string;
    publicKey: string;
}

class CardReader extends Component<ICardReaderProps, ICardReaderState> {

    componentDidMount(): void {
        ws.addEventListener("message", this.subscribeOnSocket);
        ws.readyState === 1
            ? this.getKeyPair()
            : ws.onopen = () => this.getKeyPair();
    }

    componentWillUnmount(): void {
        ws.removeEventListener("message", this.subscribeOnSocket);
    }

    subscribeOnSocket = (ev: any) => {
        if (ev.data === 'Hello') {
            console.log('Websocket connected');
        } else {
            const { key, status } = JSON.parse(ev.data);
            if (status !== "Stored to card")
                key
                    ? this.setKey(status, key)
                    : this.getPublicKey();
            else if (status === "Stored to card")
                this.props.onRead(this.state);
        }
    };

    getPublicKey = () => {
        const data = {
            jsonrpc: "2.0",
            method: "read_public"
        };
        if (ws.readyState === 1) {
            ws.send(JSON.stringify(data));
            return;
        }
    };

    getPrivateKey = () => {
        const data = {
            jsonrpc: "2.0",
            method: "read_private"
        };
        if (ws.readyState === 1) {
            return ws.send(JSON.stringify(data));
        }
    };

    setKey = async (status: WsStatuses, key: string) => {
        switch (status) {
            case WsStatuses.PUBLIC_KEY:
                await this.setState(state => ({
                    ...state,
                    publicKey: key
                }));
                return this.props.onRead(this.state);
            case WsStatuses.PRIVATE_KEY:
                await this.setState(state => ({
                    ...state,
                    privateKey: key
                }));
                return this.props.onRead(this.state);
        }
    };

    getKeyPair = async () => {
        switch (this.props.action) {
            case Actions.ReadPrivate:
                return this.getPrivateKey();
            case Actions.ReadPublic:
                return this.getPublicKey();
            case Actions.Write:
                return;
        }
    };

    render(): React.ReactNode {
        const { action, owner } = this.props;
        return (
            <Fragment>
                <p>
                    {`
                        Put ${ owner === Owner.Officer ? "officer" : "user" }
                        card on reader
                        ${ action !== Actions.Write  ? 'for scanning' : 'for writing' }
                    `}
                </p>
            </Fragment>
        )
    }
}

export default CardReader;
