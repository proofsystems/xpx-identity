import React, {ChangeEvent} from "react";
import './index.scss';

interface ISearchProps {
    onSearch: (value: string) => void;
}
const search = ({ onSearch }: ISearchProps) => (
    <div className="search-control">
        <label>Search:</label>
        <input type="text" onChange={(ev: ChangeEvent<HTMLInputElement>) => onSearch(ev.target.value)}/>
    </div>
);

export default search;
