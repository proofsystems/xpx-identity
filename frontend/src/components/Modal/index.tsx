import React, { Component } from 'react';
import Box from '../Box';

import './index.scss';

interface IModalProps {
    onClose?: () => void;
}

class Modal extends Component<IModalProps> {
    log = (ev: any) => {
        const target: HTMLDivElement = ev.target;
        if (target.id === "modal" && this.props.onClose) {
            return this.props.onClose!();
        }
    };
    render(): React.ReactNode {
        return (
            <div className="modal" onClick={this.log} id="modal">
                <Box className="box-centered">
                    {this.props.children}
                </Box>
            </div>
        );
    }
}

export default Modal;
