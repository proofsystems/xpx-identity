import React from 'react';

import './index.scss';

export default () => (
    <div className="footer">
        <p>© ProximaX 2019</p>
    </div>
)
