import React, {Component} from 'react';
import Loader from 'react-loader-spinner';
import {Actions, Hand, IFingerprints, IUser, IUserKeys, Owner} from '../../models';
import {formParser} from "../../mock/formParser";
import './index.scss'
import Button from "../Button";
import CardReader from "../CardReader";
import axios from "axios";
import {saveToCard} from "../../functions";
import MatchFinger from "../MatchFinger/MacthFinger";
import ShowFingerprints from "../ShowFingerprints";

interface IUserCardProps {
    user: IUser;
    recover: boolean;
}

interface IUserCardState {
    action?: Actions;
    newUsersKeys?: IUserKeys;
}

class UserCard extends Component<IUserCardProps, IUserCardState> {
    state = {
        action: Actions.None,
        newUsersKeys: undefined
    };

    recover = () => {
        this.setState((state) => ({
            ...state,
            action: Actions.MatchFingerprint
        }))
    };

    onSuccess = () => {
        this.setState((state) => ({
            ...state,
            action: Actions.Done
        }));
    };


    onRead = async ({ privateKey: officerPrivateKey }: IUserKeys) => {
        const { user } = this.props;
        const res = await axios.get<IUserKeys>('http://localhost:3000/keys');
        try {
            this.setState((state) => ({
                ...state,
                action: Actions.Loading,
            }));
            await axios.put('http://localhost:3000/user_info', {
                ...res.data,
                officerPrivateKey: officerPrivateKey!.toUpperCase(),
                fullName: user.userInfo.fullName
            });
            this.setState((state) => ({
                ...state,
                action: Actions.Write,
            }));
            saveToCard(res.data);
        } catch (e) {
            console.log(e);
        }
    };

    getMatchingResult = (res: boolean) => {
        if (res) {
            this.setState((state) => ({
                ...state,
                action: Actions.ReadPrivate
            }))
        }
    };

    render(): React.ReactNode {
        const { user, recover } = this.props;
        const { action } = this.state;
        console.log(user.fingerprint);
        return (
            <>
                {
                    action === Actions.None && (
                        <div>
                            <div className="box-centered">
                                <div className="user-card">
                                    {
                                        Object.keys(user.userInfo).map((key) => (
                                            <div className="user-field" id={key}>
                                                <span>
                                                    <b>{formParser[key]}</b>
                                                </span>
                                                <p className="user-value">{user.userInfo[key]}</p>
                                            </div>
                                        ))
                                    }
                                </div>
                                <div className="user-photo">
                                    {
                                        user.image && <img src={user.image} alt="user photo" width={500}/>
                                    }
                                    <ShowFingerprints fingerprint={user.fingerprint} />
                                </div>
                            </div>
                            {
                                recover && <Button label="Recover Id" onClick={this.recover}/>
                            }
                        </div>
                    )
                }
                {
                    action === Actions.MatchFingerprint && (
                        <MatchFinger
                            fingerprint={user.fingerprint}
                            getResult={this.getMatchingResult}
                        />
                    )
                }
                {
                    action === Actions.ReadPrivate && <CardReader onRead={this.onRead} owner={Owner.Officer} action={Actions.ReadPrivate}/>
                }
                {
                    action === Actions.Write && <CardReader onRead={this.onSuccess} owner={Owner.User} action={Actions.Write}/>
                }
                {
                    action === Actions.Loading && (
                        <>
                            <div
                                style={{ marginRight: "1rem" }}
                            >
                                Loading
                            </div>
                            <Loader type="Circles" color="#1c9f97" height={30} width={30} />
                        </>
                    )
                }
                {
                    action === Actions.Done && <p>Recovery successful</p>
                }
            </>
        );
    }
}

export default UserCard;
