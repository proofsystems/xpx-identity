import React, { Component, Fragment } from 'react';
import Webcam from 'react-webcam';
import Button from '../Button';

import './index.scss';

interface ICameraState {
    canTake: boolean;
}

interface ICameraProps {
    getPhoto: (image: string) => void;
}


class Camera extends Component<ICameraProps, ICameraState> {
    webcam: any;

    state = {
        canTake: false,
    };

    selfRef = (webcam: Webcam) => {
       this.webcam = webcam;
    };

    takePhoto = async () => {
        this.setState((state) => ({
            canTake: !state.canTake
        }));
    };

    getPhoto = async ()  => {
        const image = await this.webcam.getScreenshot();
        this.setState({
            canTake: false
        });
        this.props.getPhoto(image);
    };

    render(): React.ReactNode {
        const { canTake } = this.state;
        return (
            <Fragment>
                {
                    canTake && (
                        <Fragment>
                            <Webcam className="webcam" screenshotFormat="image/png" ref={this.selfRef} />
                            <Button onClick={this.getPhoto} className="camera-button" label="Take a photo" />
                        </Fragment>
                    )
                }
                <Button onClick={this.takePhoto} label="Take a photo" />
            </Fragment>
        );
    }
}

export default Camera;
