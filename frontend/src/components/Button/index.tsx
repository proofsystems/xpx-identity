import React, { Component } from 'react';
import './index.scss';

interface IButtonProps {
    label: string;
    onClick: () => void;
    className?: string;
    icon?: string;
    disable?: boolean;
}

class Button extends Component<IButtonProps> {
    render(): React.ReactNode {
        const { onClick, label, className, disable = false } = this.props;

        return (
            <button className={`custom-button ${className || ""}`}
                    onClick={onClick} disabled={disable}>
                {
                    label.toUpperCase()
                }
            </button>
        );
    }
}

export default Button;
