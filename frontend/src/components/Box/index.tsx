import React, { Component } from 'react';
import './index.scss';

interface IBoxProps {
    className?: string;
}

class Box extends Component<IBoxProps> {
    render(): React.ReactNode {
        const { className } = this.props;

        return (
            <div className={className || "box"}>
                {this.props.children}
            </div>
        )
    }
}

export default Box;
