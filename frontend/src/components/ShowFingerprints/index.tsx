import * as React from "react";
import {Hand, IFingerprints, HandsFingers} from "../../models";

export default ({ fingerprint, isBig = false }: { fingerprint: HandsFingers, isBig?: boolean }) => (
    <div className="fl mt20">
        Fingerprints
        {
            Object.keys(fingerprint)
                .map(h => (
                        <div className="button-group fs10">
                            {h}<br/>HAND
                            <div className="button-group">
                                {
                                    fingerprint[h as Hand].map((v: IFingerprints) => (
                                            <div className={`${isBig ? "max-finger" : "mini-finger"} fs10 fl`}>
                                                {v.finger}
                                                <img src={`data:image/png;base64,${v.print}`} alt={v.finger} width={isBig ? 100 : 60}/>
                                            </div>
                                        )
                                    )
                                }
                            </div>
                        </div>
                    )
                )
        }
    </div>
)