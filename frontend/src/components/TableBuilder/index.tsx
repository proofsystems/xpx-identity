import React, { Component } from 'react';
import { IUserInfo } from '../../models';

import './index.scss';

interface ITableBuilderProps {
    onClick: (userInfo: IUserInfo) => void;
    tableKeys: { [p: string]: string };
    table: [];
}

class TableBuilder extends Component<ITableBuilderProps> {
    render(): React.ReactNode {
        const { table, tableKeys } = this.props;
        console.log(table);
        return (
            <div className="table-wrapper">
                <table cellSpacing="0">
                    <thead>
                    <tr>
                        <th>#</th>
                        {
                            Object.values(tableKeys).map((value) => (
                                <th align="left">{value}</th>
                            ))
                        }
                    </tr>
                    </thead>
                    <tbody>
                    {
                        table && table.map((tr: IUserInfo ,i) => (
                            <tr onClick={() => this.props.onClick(tr)}>
                                <td>{i + 1}</td>
                                {
                                    Object.keys(tableKeys).map((key: string) => (
                                        <td align="left">{tr[key]}</td>
                                    ))
                                }
                            </tr>
                        ))
                    }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default TableBuilder;
