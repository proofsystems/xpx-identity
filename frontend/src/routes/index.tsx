import React from 'react';
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";

import PrivateRoute from './PrivateRoute';
import App from '../App';
import NewUserPage from '../pages/NewUser';
import UsersPage from '../pages/Users';
import MainPage from '../pages/MainPage';

export default () => (
    <Router>
        <App>
            <Switch>
                <Route exact path="/" component={MainPage}/>
                <PrivateRoute exact path="/new_user" component={NewUserPage} />
                <PrivateRoute exact path="/all_users" component={UsersPage} />
                <Route><Redirect to="/all_users"/></Route>
            </Switch>
        </App>
    </Router>
)
