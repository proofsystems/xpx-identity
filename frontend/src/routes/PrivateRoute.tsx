import React, { Component } from 'react';
import { Redirect, Route, withRouter } from 'react-router';
import { connect } from 'react-redux';
import { getAuthState } from '../store/selectors/login';

// @ts-ignore
const PrivateRoute = ({component: Component, ...rest}) => (
    <Route {...rest} render={ props => (
        rest.isLoggedIn ? (
            <Component {...props} />
        ) : (
            <Redirect
                to={{
                    pathname: '/',
                    state: {from: props.location}
                }}
            />
        ))
    }/>
);

//@ts-ignore
export default withRouter(connect(getAuthState)(PrivateRoute));

