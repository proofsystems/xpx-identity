import { ActionWithPayload, IUserKeys } from '../../models';
import { LOGIN, LOGOUT } from '../types';
import {Action} from "redux";

export const login = (payload: IUserKeys): ActionWithPayload<LOGIN, IUserKeys> => ({
    type: LOGIN,
    payload
});

export const logout = (): Action<LOGOUT> => ({
    type: LOGOUT
});
