import {ActionWithPayload } from '../../models';
import {LOADING} from '../types';

export const setLoading = (payload: boolean ): ActionWithPayload<LOADING, boolean> => ({
    type: LOADING,
    payload
});