import {ADD_USER, GET_USERS, SEARCH} from '../types';
import { ActionWithPayload, IUser, IUserInfo } from '../../models';

export const addUser = (payload: IUser): ActionWithPayload<ADD_USER, IUser> => ({
    type: ADD_USER,
    payload
});

export const getUsers = (payload: IUserInfo[]): ActionWithPayload<GET_USERS, IUserInfo[]> => ({
    type: GET_USERS,
    payload
});

export const searchUser = (payload: string): ActionWithPayload<SEARCH, string> => ({
    type: SEARCH,
    payload
});