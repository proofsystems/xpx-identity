import { IRootState } from "../reducers";
import { IUiState } from "../reducers/ui";

export const selectUi = (state: IRootState): IUiState => state.ui;