import { IRootState } from '../reducers';
import { IUserKeys } from '../../models';

export const selectUserKeys = (state: IRootState): IUserKeys => state.auth.user!;

export const getAuthState = (state: IRootState) => ({
    isLoggedIn: state.auth.isLoggedIn
});
