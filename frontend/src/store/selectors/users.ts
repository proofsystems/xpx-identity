import { IRootState } from '../reducers';
import { IUser } from '../../models';

export const selectUser = (state: IRootState, props: { id: string }): IUser => state.users.usersMap[props.id];

export const getAllUserKeys = (state: IRootState) => ({
    userKeys: state.users.userKeys
});

export const searchUser = (state: IRootState) => ({
    userKeys: state.users.userKeys.filter(
        (user) => user.fullName && user.fullName.toLowerCase().indexOf(state.users.searchValue.toLowerCase()) === 0
    )
});