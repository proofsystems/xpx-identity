export const ADD_USER = 'USERS: ADD_USER';
export type ADD_USER = typeof ADD_USER;

export const GET_USERS = 'USERS: GET_USERS';
export type GET_USERS = typeof GET_USERS;

export const SEARCH = 'USERS: SEARCH';
export type SEARCH = typeof SEARCH