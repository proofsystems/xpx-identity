export const LOGIN = 'AUTH: LOGIN';
export type LOGIN = typeof LOGIN;
export const LOGOUT = 'AUTH: LOGOUT';
export type LOGOUT = typeof LOGOUT;
