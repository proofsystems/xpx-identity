import {ADD_USER, GET_USERS, SEARCH} from '../types';
import { ActionWithPayload, IUser, IUserInfo } from '../../models';

export interface IUsersState {
    usersMap: {
        [p: string]: IUser;
    },
    userKeys: IUserInfo[];
    searchValue: string;
}

const initialState = {
    usersMap: undefined,
    userKeys: [],
    searchValue: ''
};

const addUser = (state: IUsersState, user: IUser ) => ({
    ...state,
    usersMap: {
        ...state.usersMap,
        [user.id!]: user
    }
});

const getUsers = (state: IUsersState, userKeys: IUserInfo[]) => ({
    ...state,
    userKeys
});

const searchUser = (state: IUserInfo, value: string) => ({
    ...state,
    searchValue: value
});

const handlers: any = {
    [ADD_USER]: addUser,
    [GET_USERS]: getUsers,
    [SEARCH]: searchUser
};

export default (state = initialState, action: ActionWithPayload<ADD_USER | GET_USERS, IUserInfo[]>) => {
    const handler = handlers[action.type];
    return handler ? handler(state, action.payload) : state;
};
