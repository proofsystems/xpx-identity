import {LOGIN, LOGOUT} from '../types';
import { ActionWithPayload, IUserKeys } from '../../models';

export interface ILoginState {
    isLoggedIn: boolean;
    user?: IUserKeys;
}

const initialState: ILoginState = {
    isLoggedIn: false,
    user: undefined
};

const handleLogin = (state: ILoginState, userKeys: IUserKeys ) => ({
    isLoggedIn: true,
    user: userKeys
});

const handleLogout = (state: ILoginState) => initialState;

const handlers: any = {
    [LOGIN]: handleLogin,
    [LOGOUT]: handleLogout
};

export default (state = initialState, action: ActionWithPayload<LOGIN, IUserKeys>): ILoginState => {
    const handler = handlers[action.type];
    return handler ? handler(state, action.payload) : state;
};
