import { LOADING } from '../types';
import { ActionWithPayload, IUserKeys } from '../../models';

export interface IUiState {
    loading: boolean;
}

const initialState: IUiState = {
    loading: false,
};

const handleLoding = (loading: boolean) => ({
    loading,
});

const handlers: any = {
    [LOADING]: handleLoding,
};

export default (state = initialState, action: ActionWithPayload<LOADING, IUserKeys>): IUiState => {
    const handler = handlers[action.type];
    return handler ? handler(action.payload) : state;
};
