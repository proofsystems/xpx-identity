import { combineReducers } from 'redux';
import userReducer, { IUsersState } from './users';
import authReducer, { ILoginState } from './login';
import uiReducer, { IUiState } from './ui';

export interface IRootState {
    users: IUsersState;
    auth: ILoginState;
    ui: IUiState;
}

export default combineReducers({
    users: userReducer,
    auth: authReducer,
    ui: uiReducer,
})
