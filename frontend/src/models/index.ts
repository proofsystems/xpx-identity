export * from './form-control';
export * from './user';
export * from './action-with-payload';
export * from './user-keys'
export * from './user-info'
export * from './ws-status'
export * from './fingerprints'

export enum Actions {
    None,
    MatchFingerprint,
    ReadPrivate,
    ReadPublic,
    Write,
    Loading,
    Done
}
export enum Owner {
    Officer,
    User
}