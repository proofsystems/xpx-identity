export interface IUser {
    fullName?: string;
    sex?: string;
    dateOfBirth?: Date;
    placeOfBirth?: string;
    bloodType?: string;
    address?: string;
    permanentAddress?: string;
    presentAddress?: string;
    filipinoOrResidentAlien?: string;
    maritalStatus?: string;
    mobileNumber?: string;
    emailAddress?: string;
    id?: string;
    frontFacing?: string;
    [p: string]: any;
}
