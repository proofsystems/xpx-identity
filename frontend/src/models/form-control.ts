export interface IFormControl {
    id: string;
    value?: any;
    options?: string[];
    placeholder?: string;
    label: string;
    type: string;
    validator: (value?: any | undefined) => boolean;
}
