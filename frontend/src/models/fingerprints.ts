export enum Fingers {
    Thumb = "THUMB",
    Index = "INDEX FINGER",
    Middle = "MIDDLE FINGER",
    Ring = "RING FINGER",
    Pinky = "PINKY FINGER"
}

export enum Hand {
    Left = "LEFT",
    Right = "RIGHT"
}

export interface IFingerprints {
    finger: Fingers,
    print?: string
}

export type HandsFingers = {
    [key in Hand]: IFingerprints[]
}