export interface IUserKeys {
    publicKey?: string | undefined;
    privateKey?: string | undefined;
}
