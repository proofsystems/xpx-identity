import { IUser } from './user';

export interface IUserInfo {
    publicKey: string;
    _id: IUser;
    fullName: string;
    psn: string;
    [p: string]: any;
}
