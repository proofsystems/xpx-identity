import React, {Component} from 'react';
import {withRouter} from "react-router";

import store from '../../store';
import {login} from '../../store/actions';
import {saveToCard} from "../../functions";

import {Actions, IUserKeys, Owner} from '../../models';

import CardReader from '../../components/CardReader';
import Box from '../../components/Box';
import Button from "../../components/Button";


class MainPage extends Component<any, any> {
    state = {
        actions: Actions.None,
        isReady: false,
        error: ""
    };

    login = () => {
        this.setState({
            actions: Actions.ReadPublic,
            isReady: true,
            error: ""
        });
    };

    onCardRead = async (userKeys?: IUserKeys) => {
        if (this.state.actions === Actions.Write) {
            this.setState({
                actions: Actions.None,
                isReady: false,
                error: ""
            });
            return;
        }
        if (userKeys) {
            if (userKeys.publicKey!.toUpperCase() === "F1734B2B1FC334CFFC109D15A8F69ABE6A26282A25BE22F889055CB1DBF24F96") {
                await store.dispatch(login(userKeys));
                this.getUsers();
            } else {
                this.setState({
                    actions: Actions.None,
                    isReady: false,
                    error: "Invalid officer card. You can register officer to another card by click Register button and put your card on reader"
                })
            }
        }
    };

    getUsers = async () => {
        const { history } = this.props;
        history.replace('/all_users');
    };

    register = () => {
        this.setState({
            actions: Actions.Write,
            isReady: true
        });
        const keys: IUserKeys = {
            privateKey: "F1F3FCFF91F7E353DA8EE24A3261A3686EDFC38297477F23BC06A6C997939C1E",
            publicKey: "F1734B2B1FC334CFFC109D15A8F69ABE6A26282A25BE22F889055CB1DBF24F96",
        };
        saveToCard(keys);
    };

    render(): React.ReactNode {
        const { actions, isReady, error } = this.state;
        return (
            <>
                <Box className="box-centered">
                    {
                        !isReady && (
                            <>
                                <Button label="Log In as Officer" onClick={this.login}/>
                                <Button label="Register New Officer" onClick={this.register}/>
                            </>
                        )
                    }
                    {
                        isReady && <CardReader action={actions} owner={Owner.Officer} onRead={this.onCardRead}/>
                    }
                </Box>
                {
                    error && (
                        <div className="box-error">{error}</div>
                    )
                }
            </>
        );
    }
}

export default withRouter(MainPage);
