import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router';
import Loader from 'react-loader-spinner';
import axios from 'axios';

import Box from '../../components/Box';
import {Actions, IUser, IUserKeys, Owner, IFingerprints, Fingers, Hand} from '../../models';
import FormBuilder from '../../components/FormBuilder';
import Button from '../../components/Button';

import Camera from '../../components/Camera';
import Fingerprint from '../../components/Fingerprint';

import { form } from '../../mock/form';
import { formParser } from '../../mock/formParser';

import Modal from "../../components/Modal";
import CardReader from "../../components/CardReader";
import { saveToCard} from "../../functions";

import './index.scss';

enum Steps {
    RegisterUser,
    ConfirmData,
    GetFingerprints,
    SignData,
    Loading,
    WriteData,
    Success
}

type IHandsFingers = {
    [key in Hand]: IFingerprints[]
}

interface IUserState {
    step: Steps;
    user?: IUser;
    loadingToChain: boolean;
    image?: string;
    noImage?: boolean;
    fingerprints: IHandsFingers;
    noFingerprint: boolean;
}

class NewUserPage extends Component<any, IUserState> {

    state = {
        step: Steps.RegisterUser,
        user: {} as IUser,
        image: undefined,
        loadingToChain: false,
        noImage: undefined,
        fingerprints: {
            [Hand.Left]: Object.keys(Fingers).map((i) => ({
                finger: Fingers[i as keyof typeof Fingers]
            })),
            [Hand.Right]: Object.keys(Fingers).map(i => ({
                finger: Fingers[i as keyof typeof Fingers]
            }))
        },
        noFingerprint: false
    };

    confirmData = (data: any) => {
        if (!this.state.image) {
            this.setState((state) => ({
                ...state,
                noImage: !this.state.image,
            }));
            return;
        }
        this.setState((state) => ({
            ...state,
            step: Steps.ConfirmData,
            user: data
        }));
    };

    getPhoto = (image: string) => {
        this.setState((state) => ({
            ...state,
            image
        }))
    };

    getFingerprint = (hand: Hand, finger: Fingers) => (fp: string) => {
        console.log(hand, finger);
        console.log(this.state.fingerprints[hand]);
        this.setState((state) => ({
            ...state,
            fingerprints: {
                ...state.fingerprints,
                [hand]: state.fingerprints[hand].map(v => {
                    if (v.finger === finger) {
                        v.print = fp
                    }
                    return v;
                })
            }
        }))
    };

    amendData = () => {
        this.setState((state) => ({
            ...state,
            step: Steps.RegisterUser
        }));
    };

    getFingerprintsStep = () => {
        this.setState((state) => ({
            ...state,
            step: Steps.GetFingerprints
        }))
    };

    verifyFingers = () => {
        const { fingerprints } = this.state;
        const v = [
            ...fingerprints[Hand.Left].filter((i: IFingerprints) => !i.print),
            ...fingerprints[Hand.Right].filter((i: IFingerprints) => !i.print),
        ];
        if (v.length) {
            this.setState((s) => ({
                ...s,
                noFingerprint: true
            }))
        }
        return !!v.length;
    };

    signData = () => {
        this.setState((state) => ({
            ...state,
            step: Steps.SignData,
            noFingerprint: false,
        }))
    };

    getUserKeys = async ({ privateKey: officerPrivateKey }: IUserKeys) => {
        const { image, user, fingerprints } = this.state;
        const res = await axios.get<IUserKeys>('http://localhost:3000/keys');
        try {
            this.setState((state) => ({
                ...state,
                step: Steps.Loading
            }));
            const file = new FormData();
            file.append("userInfo", JSON.stringify(user));
            file.append("image", image!);
            file.append("officerPrivateKey", officerPrivateKey!.toUpperCase());
            file.append("publicKey", res.data.publicKey!);
            file.append("fingerprint", JSON.stringify(fingerprints));
            await axios.post('http://localhost:3000/user_info', file, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            });
            console.log("file was stored");
            this.setState((state) => ({
                ...state,
                step: Steps.WriteData
            }));
            saveToCard(res.data);
        } catch (e) {
            console.log(e);
        }
    };

    renderFinger = (hand: Hand, v: IFingerprints) => {
        const { noFingerprint } = this.state;
        return (
            <div className="fl" key={v.finger + hand}>
                <div className="fl image">
                    {
                        v.print ? (
                            <div className="fl">
                                {v.finger}
                                <img src={`data:image/png;base64,${v.print}`} alt={v.finger} width={200}/>
                            </div>
                        ) : v.finger
                    }
                    {
                        noFingerprint && !v.print && <div className="box-error">Fingerprint is required</div>
                    }
                </div>
                <Fingerprint getFingerprint={this.getFingerprint(hand, v.finger)}/>
            </div>
        );
    };

    render(): React.ReactNode {
        const { step, user, image, noImage, fingerprints } = this.state;
        const { history } = this.props;

        return (
            <Box>
                {
                    step === Steps.RegisterUser && (
                        <Fragment>
                            <FormBuilder form={form} formState={user} onSubmit={this.confirmData}>
                                {
                                    image && <img src={image} alt="photo" width={400}/>
                                }
                                {
                                    !image && noImage && <p className="box-error">Image is required</p>
                                }
                                <Camera getPhoto={this.getPhoto}/>
                            </FormBuilder>
                        </Fragment>
                    )
                }
                {
                    step === Steps.ConfirmData && (
                        <Fragment>
                            <div className="profile-wrapper">
                                <div className="userData">
                                    {
                                        user && Object.keys(user).map((key, i) => (
                                            <div key={i}>
                                                <span><b>{formParser[key]}</b></span>
                                                <span> {user[key]}</span>
                                            </div>
                                        ))
                                    }
                                </div>
                                <div className="user-picture">
                                    {
                                        image && <img src={image} alt="photo" width={400}/>
                                    }
                                </div>
                            </div>
                            <div className="button-group">
                                <Button label="Amend data" onClick={this.amendData}/>
                                <Button label="Confirm" onClick={this.getFingerprintsStep}/>
                            </div>
                        </Fragment>
                    )
                }
                {
                    step === Steps.GetFingerprints && (
                        <div className="fl">
                            {
                                Object.keys(fingerprints).map(h => (
                                    <>
                                        {h} HAND
                                        <div className="button-group">
                                            {
                                                fingerprints[h as Hand].map((v) => this.renderFinger(h as Hand, v))
                                            }
                                        </div>
                                    </>
                                ))
                            }
                            <Button label="Confirm" onClick={() => {
                                const isError = this.verifyFingers();
                                if (isError) {
                                    return;
                                }
                                this.signData();
                            }}/>
                        </div>
                    )
                }
                {
                    step === Steps.Loading && (
                        <Modal>
                            <div
                                style={{marginRight: "1rem"}}
                            >
                                Loading
                            </div>
                            <Loader type="Circles" color="#1c9f97" height={30} width={30} />
                        </Modal>
                    )
                }
                {
                    step === Steps.SignData && (
                        <Modal>
                            <CardReader action={Actions.ReadPrivate} owner={Owner.Officer} onRead={this.getUserKeys} />
                        </Modal>
                    )
                }
                {
                    step === Steps.WriteData && (
                        <Modal>
                            <CardReader
                                action={Actions.Write}
                                owner={Owner.User}
                                onRead={() => {
                                    this.setState((prevState) => ({
                                        ...prevState,
                                        step: Steps.Success,
                                    }))
                                }}
                            />
                        </Modal>
                    )
                }
                {
                    step === Steps.Success && user && (
                        <Fragment>
                            <button onClick={history.push('/all_users')}>Ok</button>
                        </Fragment>
                    )
                }
            </Box>
        );
    }
}

export default withRouter(NewUserPage);
