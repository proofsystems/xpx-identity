import React, {Component, Fragment} from 'react';
import axios from 'axios';

import {Actions, IUser, IUserInfo, IUserKeys, Owner} from '../../models';
import {getUsers, setLoading} from '../../store/actions';
import store from '../../store';

import Box from '../../components/Box';
import Button from '../../components/Button';
import Modal from '../../components/Modal';
import UserCard from '../../components/UserCard';
import UsersContainer from "../../containers/Users";
import CardReader from "../../components/CardReader";
import SearchContainer from "../../containers/Search";

import './index.scss';

interface IUsersState {
    readCard: boolean;
    user?: IUser;
    recover: boolean;
}

class UsersPage extends Component<any, IUsersState> {

    state = {
        readCard: false,
        recover: false,
        user: undefined,
        error: ""
    };

    componentDidMount = () => {
        this.getUsers();
    }

    readIDCard = () => {
        this.setState((state) => ({
            ...state,
            readCard: true
        }))
    };

    createUser = () => {
        this.props.history.replace('/new_user')
    };

    getUsers = async () => {
        await store.dispatch(setLoading(true));
        const { data: users } = await axios.get<IUserInfo[]>('http://localhost:3000/users');
        setTimeout(async () => { 
            await store.dispatch(getUsers(users));
            await store.dispatch(setLoading(false));
         }, 2000);
    };

    getUser = async (publicKey: string, recover: boolean) => {
        await store.dispatch(setLoading(true));
        if (publicKey!.toUpperCase() === "F1734B2B1FC334CFFC109D15A8F69ABE6A26282A25BE22F889055CB1DBF24F96") {
            this.setState((state) => ({
                ...state,
                recover,
                readCard: false,
                user: undefined,
                error: "Officer information do not show, please use User card"
            }));
            await store.dispatch(setLoading(false));
            return;
        }
        const { data } = await axios.get(`http://localhost:3000/user_info/${publicKey!.toUpperCase()}`);
        if (data.fileContent) {
            const user = data.fileContent;
            this.setState((state) => ({
                ...state,
                readCard: false,
                recover,
                user
            }))
        } else {
            this.setState((state) => ({
                ...state,
                recover,
                readCard: false,
                user: undefined,
                error: data.error
            }))
        }
        await store.dispatch(setLoading(false));
    };

    closeModal = () => {
        this.setState((state) => ({
            ...state,
            readCard: false,
            user: undefined,
            error: ""
        }))
    };

    render(): React.ReactNode {
        const { readCard, user, recover, error } = this.state;
        return (
            <Fragment>
                <div className="box-header">
                    <SearchContainer />
                    <div className="button-group">
                        <Button label="Create new individual account" onClick={this.createUser}/>
                        <Button label="Read ID Card" onClick={this.readIDCard}/>
                        <Button label="Sync" onClick={this.getUsers}/>
                    </div>
                </div>
                {
                    user && (
                        <Modal onClose={this.closeModal}>
                            <UserCard user={user} recover={recover}/>
                        </Modal>
                    )
                }
                {
                    readCard && (
                        <Modal onClose={this.closeModal}>
                            <CardReader
                                owner={Owner.User}
                                onRead={(userKeys) => this.getUser(userKeys.publicKey!, false)}
                                action={Actions.ReadPublic}
                            />
                        </Modal>
                    )
                }
                {
                    error && (
                        <Modal onClose={this.closeModal}>
                            <div className="box-error">{error}</div>
                        </Modal>
                    )
                }
                <Box>
                    <UsersContainer onClick={(userInfo: IUserInfo) => this.getUser(userInfo.publicKey, true)} />
                </Box>
            </Fragment>
        );
    }
}

// @ts-ignore
export default UsersPage;
