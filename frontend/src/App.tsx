import React, { Component } from 'react';
import Header from './components/Header';
import Footer from './components/Footer';
import LoadingContainer from './containers/Loading';
import './App.css';

class App extends Component {
  render() {
    return (
        <div className="wrapper">
            <LoadingContainer />
            <Header />
            <div className="content_wrapper">
                {this.props.children}
            </div>
            <Footer />
        </div>
    );
  }
}

export default App;
