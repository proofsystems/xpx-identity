import React, {Component} from "react";
import {connect} from "react-redux";

import GlobalLoading from "../components/GlobalLoading";
import { selectUi } from "../store/selectors";

interface ILoadingProps {
    loading: boolean;
}

class LoadingContainer extends Component<ILoadingProps, any>{
    render(): React.ReactNode {
        const { loading } = this.props;
        return (
            loading && <GlobalLoading />
        )
    }
}

// @ts-ignore
export default connect(selectUi)(LoadingContainer);