import React, {Component} from "react";
import {connect} from "react-redux";
import {searchUser} from "../store/selectors";
import TableBuilder from "../components/TableBuilder";
import {tableParser} from "../mock/tableParser";
import {IUserInfo} from "../models";

interface IUsersProps {
    onClick: (userInfo: IUserInfo) => void;
    userKeys: [];
}

class UsersContainer extends Component<IUsersProps, any>{
    render(): React.ReactNode {
        const { userKeys } = this.props;
        return (
            userKeys
                ? <TableBuilder onClick={this.props.onClick} table={userKeys} tableKeys={tableParser}/>
                : <p>No users</p>
        )
    }
}

// @ts-ignore
export default connect(searchUser)(UsersContainer);