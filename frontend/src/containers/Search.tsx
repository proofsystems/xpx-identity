import React, {Component} from "react";
import Search from "../components/Search";
import {searchUser} from "../store/actions";

import store from '../store';

class SearchContainer extends Component{
    onSearch = (value: string) => {
        store.dispatch(searchUser(value))
    };

    render(): React.ReactNode {
        return (
            <Search onSearch={this.onSearch}/>
        );
    }
}

export default SearchContainer;