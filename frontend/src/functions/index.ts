import {IUserKeys} from "../models";

export const ws = new WebSocket("ws://127.0.0.1:9002");

export const saveToCard = ({ publicKey: public_key, privateKey: private_key }: IUserKeys) => {
    const data = {
        jsonrpc : "2.0",
        method: "store",
        params: {
            public_key,
            private_key
        }
    };
    ws.send(JSON.stringify(data))
};

export const calculateAge = (birthday: Date) => {
    const ageDifMs = Date.now() - birthday.getTime();
    const ageDate = new Date(ageDifMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
};
