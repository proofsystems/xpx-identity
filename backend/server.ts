import * as Koa from 'koa';
import * as Router from 'koa-router';
import * as bodyParser from 'koa-body';
import * as cors from '@koa/cors';
import * as koaStatic from 'koa-static';
import * as koaSend from 'koa-send';
import * as path from "path";

import {Account, NetworkType} from './proximax-sdk/src/model/model';
import {readUserInfoController, recoverUserInfoController, storeUserInfoController} from "./controllers";
import {findUserController, getAllUsersController} from "./controllers/user";

const app = new Koa();
const router = new Router();
app.use(bodyParser());
app.use(cors({
    origin: "*"
}));
app.use(koaStatic(path.resolve(__dirname, "../../frontend/build")));

router
    .get('/keys', async (ctx) => {
        const account = Account.generateNewAccount(NetworkType.MIJIN_TEST);
        ctx.body = {
            publicKey: account.publicKey,
            privateKey: account.privateKey,
        };
    })
    .get('/user_info/:publicKey', readUserInfoController)
    .put('/user_info', recoverUserInfoController)
    .post('/user_info', bodyParser({
        multipart: true
    }), storeUserInfoController)
    .get('/user', findUserController)
    .get('/users', getAllUsersController);

app.use(router.routes());
app.use(async (ctx) => {
    await koaSend(ctx, path.resolve("/frontend/build/index.html"))
});

app.listen(3000, "0.0.0.0");

console.log("Server is running on localhost:" + 3000);