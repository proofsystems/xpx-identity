"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var TransactionInfo_1 = require("./TransactionInfo");
/**
 * Inner transaction information model included in all aggregate inner transactions
 */
var AggregateTransactionInfo = /** @class */ (function (_super) {
    __extends(AggregateTransactionInfo, _super);
    /**
     * @param height
     * @param index
     * @param id
     * @param aggregateHash
     * @param aggregateId
     */
    function AggregateTransactionInfo(height, index, id, 
    /**
     * The hash of the aggregate transaction.
     */
    aggregateHash, 
    /**
     * The id of the aggregate transaction.
     */
    aggregateId) {
        var _this = _super.call(this, height, index, id) || this;
        _this.aggregateHash = aggregateHash;
        _this.aggregateId = aggregateId;
        return _this;
    }
    return AggregateTransactionInfo;
}(TransactionInfo_1.TransactionInfo));
exports.AggregateTransactionInfo = AggregateTransactionInfo;
//# sourceMappingURL=AggregateTransactionInfo.js.map