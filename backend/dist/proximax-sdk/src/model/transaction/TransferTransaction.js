"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var proximax_nem2_library_1 = require("proximax-nem2-library");
var UInt64_1 = require("../UInt64");
var Transaction_1 = require("./Transaction");
var TransactionType_1 = require("./TransactionType");
/**
 * Transfer transactions contain data about transfers of mosaics and message to another account.
 */
var TransferTransaction = /** @class */ (function (_super) {
    __extends(TransferTransaction, _super);
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param fee
     * @param recipient
     * @param mosaics
     * @param message
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    function TransferTransaction(networkType, version, deadline, fee, 
    /**
     * The address of the recipient.
     */
    recipient, 
    /**
     * The array of Mosaic objects.
     */
    mosaics, 
    /**
     * The transaction message of 2048 characters.
     */
    message, signature, signer, transactionInfo) {
        var _this = _super.call(this, TransactionType_1.TransactionType.TRANSFER, networkType, version, deadline, fee, signature, signer, transactionInfo) || this;
        _this.recipient = recipient;
        _this.mosaics = mosaics;
        _this.message = message;
        return _this;
    }
    /**
     * Create a transfer transaction object
     * @param deadline - The deadline to include the transaction.
     * @param recipient - The recipient of the transaction.
     * @param mosaics - The array of mosaics.
     * @param message - The transaction message.
     * @param networkType - The network type.
     * @returns {TransferTransaction}
     */
    TransferTransaction.create = function (deadline, recipient, mosaics, message, networkType) {
        return new TransferTransaction(networkType, 3, deadline, new UInt64_1.UInt64([0, 0]), recipient, mosaics, message);
    };
    /**
     * @internal
     * @returns {VerifiableTransaction}
     */
    TransferTransaction.prototype.buildTransaction = function () {
        return new proximax_nem2_library_1.TransferTransaction.Builder()
            .addDeadline(this.deadline.toDTO())
            .addFee(this.fee.toDTO())
            .addVersion(this.versionToDTO())
            .addRecipient(this.recipient.plain())
            .addMosaics(this.mosaics.map(function (mosaic) { return mosaic.toDTO(); }))
            .addMessage(this.message)
            .build();
    };
    return TransferTransaction;
}(Transaction_1.Transaction));
exports.TransferTransaction = TransferTransaction;
//# sourceMappingURL=TransferTransaction.js.map