/**
 * Static class containing transaction type constants.
 */
export declare class TransactionType {
    /**
     * Transfer Transaction transaction type.
     * @type {number}
     */
    static readonly TRANSFER = 16724;
    /**
     * Register namespace transaction type.
     * @type {number}
     */
    static readonly REGISTER_NAMESPACE = 16718;
    /**
     * Mosaic definition transaction type.
     * @type {number}
     */
    static readonly MOSAIC_DEFINITION = 16717;
    /**
     * Mosaic supply change transaction.
     * @type {number}
     */
    static readonly MOSAIC_SUPPLY_CHANGE = 16973;
    /**
     * Modify multisig account transaction type.
     * @type {number}
     */
    static readonly MODIFY_MULTISIG_ACCOUNT = 16725;
    /**
     * Aggregate complete transaction type.
     * @type {number}
     */
    static readonly AGGREGATE_COMPLETE = 16705;
    /**
     * Aggregate bonded transaction type
     */
    static readonly AGGREGATE_BONDED = 16961;
    /**
     * Lock transaction type
     * @type {number}
     */
    static readonly LOCK = 16712;
    /**
     * Secret Lock Transaction type
     * @type {number}
     */
    static readonly SECRET_LOCK = 16972;
    /**
     * Secret Proof transaction type
     * @type {number}
     */
    static readonly SECRET_PROOF = 17228;
}
