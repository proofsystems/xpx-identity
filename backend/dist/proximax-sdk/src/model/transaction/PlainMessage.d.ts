import { Message } from './Message';
/**
 * The plain message model defines a plain string. When sending it to the network we transform the payload to hex-string.
 */
export declare class PlainMessage extends Message {
    /**
     * Create plain message object.
     * @returns PlainMessage
     */
    static create(message: string): PlainMessage;
    /**
     * @internal
     */
    static createFromDTO(payload: string): PlainMessage;
    /**
     * @internal
     * @param payload
     * @param hexEncodedPayload
     */
    constructor(payload: string, hexEncodedPayload?: string);
}
/**
 * Plain message containing an empty string
 * @type {PlainMessage}
 */
export declare const EmptyMessage: PlainMessage;
