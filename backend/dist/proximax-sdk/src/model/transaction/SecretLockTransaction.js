"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var proximax_nem2_library_1 = require("proximax-nem2-library");
var UInt64_1 = require("../UInt64");
var HashType_1 = require("./HashType");
var Transaction_1 = require("./Transaction");
var TransactionType_1 = require("./TransactionType");
var SecretLockTransaction = /** @class */ (function (_super) {
    __extends(SecretLockTransaction, _super);
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param fee
     * @param mosaic
     * @param duration
     * @param hashType
     * @param secret
     * @param recipient
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    function SecretLockTransaction(networkType, version, deadline, fee, 
    /**
     * The locked mosaic.
     */
    mosaic, 
    /**
     * The duration for the funds to be released or returned.
     */
    duration, 
    /**
     * The hash algorithm, secret is generated with.
     */
    hashType, 
    /**
     * The proof hashed.
     */
    secret, 
    /**
     * The recipient of the funds.
     */
    recipient, signature, signer, transactionInfo) {
        var _this = _super.call(this, TransactionType_1.TransactionType.SECRET_LOCK, networkType, version, deadline, fee, signature, signer, transactionInfo) || this;
        _this.mosaic = mosaic;
        _this.duration = duration;
        _this.hashType = hashType;
        _this.secret = secret;
        _this.recipient = recipient;
        if (!HashType_1.HashTypeLengthValidator(hashType, _this.secret)) {
            throw new Error('HashType and Secret have incompatible length or not hexadecimal string');
        }
        return _this;
    }
    /**
     * Create a secret lock transaction object.
     *
     * @param deadline - The deadline to include the transaction.
     * @param mosaic - The locked mosaic.
     * @param duration - The funds lock duration.
     * @param hashType - The hash algorithm secret is generated with.
     * @param secret - The proof hashed.
     * @param recipient - The recipient of the funds.
     * @param networkType - The network type.
     *
     * @return a SecretLockTransaction instance
     */
    SecretLockTransaction.create = function (deadline, mosaic, duration, hashType, secret, recipient, networkType) {
        return new SecretLockTransaction(networkType, 3, deadline, UInt64_1.UInt64.fromUint(0), mosaic, duration, hashType, secret, recipient);
    };
    /**
     * @internal
     * @returns {VerifiableTransaction}
     */
    SecretLockTransaction.prototype.buildTransaction = function () {
        return new proximax_nem2_library_1.SecretLockTransaction.Builder()
            .addDeadline(this.deadline.toDTO())
            .addType(this.type)
            .addFee(this.fee.toDTO())
            .addVersion(this.versionToDTO())
            .addMosaicId(this.mosaic.id.id.toDTO())
            .addMosaicAmount(this.mosaic.amount.toDTO())
            .addDuration(this.duration.toDTO())
            .addHashAlgorithm(this.hashType)
            .addSecret(this.secret)
            .addRecipient(this.recipient.plain())
            .build();
    };
    return SecretLockTransaction;
}(Transaction_1.Transaction));
exports.SecretLockTransaction = SecretLockTransaction;
//# sourceMappingURL=SecretLockTransaction.js.map