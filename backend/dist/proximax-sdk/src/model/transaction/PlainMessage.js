"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var proximax_nem2_library_1 = require("proximax-nem2-library");
var Message_1 = require("./Message");
/**
 * The plain message model defines a plain string. When sending it to the network we transform the payload to hex-string.
 */
var PlainMessage = /** @class */ (function (_super) {
    __extends(PlainMessage, _super);
    /**
     * @internal
     * @param payload
     * @param hexEncodedPayload
     */
    function PlainMessage(payload, hexEncodedPayload) {
        return _super.call(this, 0, hexEncodedPayload || proximax_nem2_library_1.convert.utf8ToHex(payload), payload) || this;
    }
    /**
     * Create plain message object.
     * @returns PlainMessage
     */
    PlainMessage.create = function (message) {
        return new PlainMessage(message);
    };
    /**
     * @internal
     */
    PlainMessage.createFromDTO = function (payload) {
        return new PlainMessage(this.decodeHex(payload), payload);
    };
    return PlainMessage;
}(Message_1.Message));
exports.PlainMessage = PlainMessage;
/**
 * Plain message containing an empty string
 * @type {PlainMessage}
 */
exports.EmptyMessage = PlainMessage.create('');
//# sourceMappingURL=PlainMessage.js.map