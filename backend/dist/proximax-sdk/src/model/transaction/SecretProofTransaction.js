"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var proximax_nem2_library_1 = require("proximax-nem2-library");
var UInt64_1 = require("../UInt64");
var HashType_1 = require("./HashType");
var Transaction_1 = require("./Transaction");
var TransactionType_1 = require("./TransactionType");
var SecretProofTransaction = /** @class */ (function (_super) {
    __extends(SecretProofTransaction, _super);
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param fee
     * @param hashType
     * @param secret
     * @param proof
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    function SecretProofTransaction(networkType, version, deadline, fee, hashType, secret, proof, signature, signer, transactionInfo) {
        var _this = _super.call(this, TransactionType_1.TransactionType.SECRET_PROOF, networkType, version, deadline, fee, signature, signer, transactionInfo) || this;
        _this.hashType = hashType;
        _this.secret = secret;
        _this.proof = proof;
        if (!HashType_1.HashTypeLengthValidator(hashType, _this.secret)) {
            throw new Error('HashType and Secret have incompatible length or not hexadecimal string');
        }
        return _this;
    }
    /**
     * Create a secret proof transaction object.
     *
     * @param deadline - The deadline to include the transaction.
     * @param hashType - The hash algorithm secret is generated with.
     * @param secret - The seed proof hashed.
     * @param proof - The seed proof.
     * @param networkType - The network type.
     *
     * @return a SecretProofTransaction instance
     */
    SecretProofTransaction.create = function (deadline, hashType, secret, proof, networkType) {
        return new SecretProofTransaction(networkType, 3, deadline, UInt64_1.UInt64.fromUint(0), hashType, secret, proof);
    };
    /**
     * @internal
     * @returns {VerifiableTransaction}
     */
    SecretProofTransaction.prototype.buildTransaction = function () {
        return new proximax_nem2_library_1.SecretProofTransaction.Builder()
            .addDeadline(this.deadline.toDTO())
            .addType(this.type)
            .addFee(this.fee.toDTO())
            .addVersion(this.versionToDTO())
            .addHashAlgorithm(this.hashType)
            .addSecret(this.secret)
            .addProof(this.proof)
            .build();
    };
    return SecretProofTransaction;
}(Transaction_1.Transaction));
exports.SecretProofTransaction = SecretProofTransaction;
//# sourceMappingURL=SecretProofTransaction.js.map