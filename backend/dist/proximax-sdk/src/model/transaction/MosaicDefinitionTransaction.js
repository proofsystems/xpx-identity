"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var proximax_nem2_library_1 = require("proximax-nem2-library");
var MosaicId_1 = require("../mosaic/MosaicId");
var NamespaceId_1 = require("../namespace/NamespaceId");
var UInt64_1 = require("../UInt64");
var Transaction_1 = require("./Transaction");
var TransactionType_1 = require("./TransactionType");
/**
 * Before a mosaic can be created or transferred, a corresponding definition of the mosaic has to be created and published to the network.
 * This is done via a mosaic definition transaction.
 */
var MosaicDefinitionTransaction = /** @class */ (function (_super) {
    __extends(MosaicDefinitionTransaction, _super);
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param fee
     * @param parentId
     * @param mosaicId
     * @param mosaicName
     * @param mosaicProperties
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    function MosaicDefinitionTransaction(networkType, version, deadline, fee, 
    /**
     * The namespace id.
     */
    parentId, 
    /**
     * The mosaic id.
     */
    mosaicId, 
    /**
     * The name of the mosaic.
     */
    mosaicName, 
    /**
     * The mosaic properties.
     */
    mosaicProperties, signature, signer, transactionInfo) {
        var _this = _super.call(this, TransactionType_1.TransactionType.MOSAIC_DEFINITION, networkType, version, deadline, fee, signature, signer, transactionInfo) || this;
        _this.parentId = parentId;
        _this.mosaicId = mosaicId;
        _this.mosaicName = mosaicName;
        _this.mosaicProperties = mosaicProperties;
        return _this;
    }
    /**
     * Create a mosaic creation transaction object
     * @param deadline - The deadline to include the transaction.
     * @param mosaicName - The mosaic name ex: xem.
     * @param namespaceName - The namespace where mosaic will be included ex: nem.
     * @param mosaicProperties - The mosaic properties.
     * @param networkType - The network type.
     * @returns {MosaicDefinitionTransaction}
     */
    MosaicDefinitionTransaction.create = function (deadline, mosaicName, namespaceName, mosaicProperties, networkType) {
        return new MosaicDefinitionTransaction(networkType, 2, deadline, new UInt64_1.UInt64([0, 0]), new NamespaceId_1.NamespaceId(namespaceName), new MosaicId_1.MosaicId(proximax_nem2_library_1.mosaicId(namespaceName, mosaicName)), mosaicName, mosaicProperties);
    };
    /**
     * @internal
     * @returns {VerifiableTransaction}
     */
    MosaicDefinitionTransaction.prototype.buildTransaction = function () {
        var mosaicDefinitionTransaction = new proximax_nem2_library_1.MosaicCreationTransaction.Builder()
            .addDeadline(this.deadline.toDTO())
            .addFee(this.fee.toDTO())
            .addVersion(this.versionToDTO())
            .addDivisibility(this.mosaicProperties.divisibility)
            .addDuration(this.mosaicProperties.duration.toDTO())
            .addParentId(this.parentId.id.toDTO())
            .addMosaicId(this.mosaicId.id.toDTO())
            .addMosaicName(this.mosaicName);
        if (this.mosaicProperties.supplyMutable === true) {
            mosaicDefinitionTransaction = mosaicDefinitionTransaction.addSupplyMutable();
        }
        if (this.mosaicProperties.transferable === true) {
            mosaicDefinitionTransaction = mosaicDefinitionTransaction.addTransferability();
        }
        if (this.mosaicProperties.levyMutable === true) {
            mosaicDefinitionTransaction = mosaicDefinitionTransaction.addLevyMutable();
        }
        return mosaicDefinitionTransaction.build();
    };
    return MosaicDefinitionTransaction;
}(Transaction_1.Transaction));
exports.MosaicDefinitionTransaction = MosaicDefinitionTransaction;
//# sourceMappingURL=MosaicDefinitionTransaction.js.map