"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var js_joda_1 = require("js-joda");
var UInt64_1 = require("../UInt64");
/**
 * The deadline of the transaction. The deadline is given as the number of seconds elapsed since the creation of the nemesis block.
 * If a transaction does not get included in a block before the deadline is reached, it is deleted.
 */
var Deadline = /** @class */ (function () {
    /**
     * @param deadline
     */
    function Deadline(deadline) {
        this.value = deadline;
    }
    /**
     * Create deadline model
     * @param deadline
     * @param chronoUnit
     * @returns {Deadline}
     */
    Deadline.create = function (deadline, chronoUnit) {
        if (deadline === void 0) { deadline = 12; }
        if (chronoUnit === void 0) { chronoUnit = js_joda_1.ChronoUnit.HOURS; }
        var networkTimeStamp = (new Date()).getTime();
        var timeStampDateTime = js_joda_1.LocalDateTime.ofInstant(js_joda_1.Instant.ofEpochMilli(networkTimeStamp), js_joda_1.ZoneId.SYSTEM);
        var deadlineDateTime = timeStampDateTime.plus(deadline, chronoUnit);
        if (deadline <= 0) {
            throw new Error('deadline should be greater than 0');
        }
        else if (timeStampDateTime.plus(24, js_joda_1.ChronoUnit.HOURS).compareTo(deadlineDateTime) !== 1) {
            throw new Error('deadline should be less than 24 hours');
        }
        return new Deadline(deadlineDateTime);
    };
    /**
     * @internal
     * @param value
     * @returns {Deadline}
     */
    Deadline.createFromDTO = function (value) {
        var dateSeconds = (new UInt64_1.UInt64(value)).compact();
        var deadline = js_joda_1.LocalDateTime.ofInstant(js_joda_1.Instant.ofEpochMilli(Math.round(dateSeconds + Deadline.timestampNemesisBlock * 1000)), js_joda_1.ZoneId.SYSTEM);
        return new Deadline(deadline);
    };
    /**
     * @internal
     */
    Deadline.prototype.toDTO = function () {
        return UInt64_1.UInt64.fromUint((this.value.atZone(js_joda_1.ZoneId.SYSTEM).toInstant().toEpochMilli() - Deadline.timestampNemesisBlock * 1000)).toDTO();
    };
    /**
     * @type {number}
     */
    Deadline.timestampNemesisBlock = 1459468800;
    return Deadline;
}());
exports.Deadline = Deadline;
//# sourceMappingURL=Deadline.js.map