"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var proximax_nem2_library_1 = require("proximax-nem2-library");
var UInt64_1 = require("../UInt64");
var Transaction_1 = require("./Transaction");
var TransactionType_1 = require("./TransactionType");
/**
 * In case a mosaic has the flag 'supplyMutable' set to true, the creator of the mosaic can change the supply,
 * i.e. increase or decrease the supply.
 */
var MosaicSupplyChangeTransaction = /** @class */ (function (_super) {
    __extends(MosaicSupplyChangeTransaction, _super);
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param fee
     * @param mosaicId
     * @param direction
     * @param delta
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    function MosaicSupplyChangeTransaction(networkType, version, deadline, fee, 
    /**
     * The mosaic id.
     */
    mosaicId, 
    /**
     * The supply type.
     */
    direction, 
    /**
     * The supply change in units for the mosaic.
     */
    delta, signature, signer, transactionInfo) {
        var _this = _super.call(this, TransactionType_1.TransactionType.MOSAIC_SUPPLY_CHANGE, networkType, version, deadline, fee, signature, signer, transactionInfo) || this;
        _this.mosaicId = mosaicId;
        _this.direction = direction;
        _this.delta = delta;
        return _this;
    }
    /**
     * Create a mosaic supply change transaction object
     * @param deadline - The deadline to include the transaction.
     * @param mosaicId - The mosaic id.
     * @param direction - The supply type.
     * @param delta - The supply change in units for the mosaic.
     * @param networkType - The network type.
     * @returns {MosaicSupplyChangeTransaction}
     */
    MosaicSupplyChangeTransaction.create = function (deadline, mosaicId, direction, delta, networkType) {
        return new MosaicSupplyChangeTransaction(networkType, 2, deadline, new UInt64_1.UInt64([0, 0]), mosaicId, direction, delta);
    };
    /**
     * @internal
     * @returns {VerifiableTransaction}
     */
    MosaicSupplyChangeTransaction.prototype.buildTransaction = function () {
        return new proximax_nem2_library_1.MosaicSupplyChangeTransaction.Builder()
            .addDeadline(this.deadline.toDTO())
            .addFee(this.fee.toDTO())
            .addVersion(this.versionToDTO())
            .addMosaicId(this.mosaicId.id.toDTO())
            .addDirection(this.direction)
            .addDelta(this.delta.toDTO())
            .build();
    };
    return MosaicSupplyChangeTransaction;
}(Transaction_1.Transaction));
exports.MosaicSupplyChangeTransaction = MosaicSupplyChangeTransaction;
//# sourceMappingURL=MosaicSupplyChangeTransaction.js.map