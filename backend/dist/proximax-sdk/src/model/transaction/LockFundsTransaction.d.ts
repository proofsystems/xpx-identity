import { VerifiableTransaction } from 'proximax-nem2-library';
import { PublicAccount } from '../account/PublicAccount';
import { NetworkType } from '../blockchain/NetworkType';
import { Mosaic } from '../mosaic/Mosaic';
import { UInt64 } from '../UInt64';
import { Deadline } from './Deadline';
import { SignedTransaction } from './SignedTransaction';
import { Transaction } from './Transaction';
import { TransactionInfo } from './TransactionInfo';
/**
 * Lock funds transaction is used before sending an Aggregate bonded transaction, as a deposit to announce the transaction.
 * When aggregate bonded transaction is confirmed funds are returned to LockFundsTransaction signer.
 *
 * @since 1.0
 */
export declare class LockFundsTransaction extends Transaction {
    /**
     * The locked mosaic.
     */
    readonly mosaic: Mosaic;
    /**
     * The funds lock duration.
     */
    readonly duration: UInt64;
    /**
     * Aggregate bonded hash.
     */
    readonly hash: string;
    /**
     * Create a Lock funds transaction object
     * @param deadline - The deadline to include the transaction.
     * @param mosaic - The locked mosaic.
     * @param duration - The funds lock duration.
     * @param signedTransaction - The signed transaction for which funds are locked.
     * @param networkType - The network type.
     */
    static create(deadline: Deadline, mosaic: Mosaic, duration: UInt64, signedTransaction: SignedTransaction, networkType: NetworkType): LockFundsTransaction;
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param fee
     * @param mosaic
     * @param duration
     * @param signedTransaction
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    constructor(networkType: NetworkType, version: number, deadline: Deadline, fee: UInt64, 
    /**
     * The locked mosaic.
     */
    mosaic: Mosaic, 
    /**
     * The funds lock duration.
     */
    duration: UInt64, signedTransaction: SignedTransaction, signature?: string, signer?: PublicAccount, transactionInfo?: TransactionInfo);
    /**
     * @internal
     * @return {VerifiableTransaction}
     */
    protected buildTransaction(): VerifiableTransaction;
}
