"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Hash type. Supported types are:
 * 0: SHA3_512.
 */
var proximax_nem2_library_1 = require("proximax-nem2-library");
var HashType;
(function (HashType) {
    HashType[HashType["SHA3_512"] = 0] = "SHA3_512";
})(HashType = exports.HashType || (exports.HashType = {}));
function HashTypeLengthValidator(hashType, input) {
    if (hashType === HashType.SHA3_512 && proximax_nem2_library_1.convert.isHexString(input)) {
        return input.length === 128;
    }
    return false;
}
exports.HashTypeLengthValidator = HashTypeLengthValidator;
//# sourceMappingURL=HashType.js.map