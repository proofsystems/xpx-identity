/**
 * Transaction response of an announce transaction
 */
export declare class TransactionAnnounceResponse {
    /**
     * The success or error message.
     */
    readonly message: string;
    /**
     * @internal
     * @param message
     */
    constructor(
    /**
     * The success or error message.
     */
    message: string);
}
