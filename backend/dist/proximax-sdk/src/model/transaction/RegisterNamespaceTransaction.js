"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var proximax_nem2_library_1 = require("proximax-nem2-library");
var NamespaceId_1 = require("../namespace/NamespaceId");
var NamespaceType_1 = require("../namespace/NamespaceType");
var UInt64_1 = require("../UInt64");
var Transaction_1 = require("./Transaction");
var TransactionType_1 = require("./TransactionType");
/**
 * Accounts can rent a namespace for an amount of blocks and after a this renew the contract.
 * This is done via a RegisterNamespaceTransaction.
 */
var RegisterNamespaceTransaction = /** @class */ (function (_super) {
    __extends(RegisterNamespaceTransaction, _super);
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param fee
     * @param namespaceType
     * @param namespaceName
     * @param namespaceId
     * @param duration
     * @param parentId
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    function RegisterNamespaceTransaction(networkType, version, deadline, fee, 
    /**
     * The namespace type could be namespace or sub namespace
     */
    namespaceType, 
    /**
     * The namespace name
     */
    namespaceName, 
    /**
     * The id of the namespace derived from namespaceName.
     * When creating a sub namespace the namespaceId is derived from namespaceName and parentName.
     */
    namespaceId, 
    /**
     * The number of blocks a namespace is active
     */
    duration, 
    /**
     * The id of the parent sub namespace
     */
    parentId, signature, signer, transactionInfo) {
        var _this = _super.call(this, TransactionType_1.TransactionType.REGISTER_NAMESPACE, networkType, version, deadline, fee, signature, signer, transactionInfo) || this;
        _this.namespaceType = namespaceType;
        _this.namespaceName = namespaceName;
        _this.namespaceId = namespaceId;
        _this.duration = duration;
        _this.parentId = parentId;
        return _this;
    }
    /**
     * Create a root namespace object
     * @param deadline - The deadline to include the transaction.
     * @param namespaceName - The namespace name.
     * @param duration - The duration of the namespace.
     * @param networkType - The network type.
     * @returns {RegisterNamespaceTransaction}
     */
    RegisterNamespaceTransaction.createRootNamespace = function (deadline, namespaceName, duration, networkType) {
        return new RegisterNamespaceTransaction(networkType, 2, deadline, new UInt64_1.UInt64([0, 0]), NamespaceType_1.NamespaceType.RootNamespace, namespaceName, new NamespaceId_1.NamespaceId(namespaceName), duration);
    };
    /**
     * Create a sub namespace object
     * @param deadline - The deadline to include the transaction.
     * @param namespaceName - The namespace name.
     * @param parentNamespace - The parent namespace name.
     * @param networkType - The network type.
     * @returns {RegisterNamespaceTransaction}
     */
    RegisterNamespaceTransaction.createSubNamespace = function (deadline, namespaceName, parentNamespace, networkType) {
        var parentId;
        if (typeof parentNamespace === 'string') {
            parentId = new NamespaceId_1.NamespaceId(proximax_nem2_library_1.subnamespaceParentId(parentNamespace, namespaceName));
        }
        else {
            parentId = parentNamespace;
        }
        return new RegisterNamespaceTransaction(networkType, 2, deadline, new UInt64_1.UInt64([0, 0]), NamespaceType_1.NamespaceType.SubNamespace, namespaceName, new NamespaceId_1.NamespaceId(proximax_nem2_library_1.subnamespaceNamespaceId(parentNamespace, namespaceName)), undefined, parentId);
    };
    /**
     * @internal
     * @returns {VerifiableTransaction}
     */
    RegisterNamespaceTransaction.prototype.buildTransaction = function () {
        var registerNamespacetransaction = new proximax_nem2_library_1.NamespaceCreationTransaction.Builder()
            .addDeadline(this.deadline.toDTO())
            .addFee(this.fee.toDTO())
            .addVersion(this.versionToDTO())
            .addNamespaceType(this.namespaceType)
            .addNamespaceId(this.namespaceId.id.toDTO())
            .addNamespaceName(this.namespaceName);
        if (this.namespaceType === NamespaceType_1.NamespaceType.RootNamespace) {
            registerNamespacetransaction = registerNamespacetransaction.addDuration(this.duration.toDTO());
        }
        else {
            registerNamespacetransaction = registerNamespacetransaction.addParentId(this.parentId.id.toDTO());
        }
        return registerNamespacetransaction.build();
    };
    return RegisterNamespaceTransaction;
}(Transaction_1.Transaction));
exports.RegisterNamespaceTransaction = RegisterNamespaceTransaction;
//# sourceMappingURL=RegisterNamespaceTransaction.js.map