"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var proximax_nem2_library_1 = require("proximax-nem2-library");
var Message_1 = require("./Message");
var SecureMessage = /** @class */ (function (_super) {
    __extends(SecureMessage, _super);
    /**
     * @internal
     * @param hexEncodedPayload
     * @param payload
     */
    function SecureMessage(hexEncodedPayload, payload) {
        return _super.call(this, 1, hexEncodedPayload, payload) || this;
    }
    SecureMessage.create = function (message, publicKey, privateKey) {
        var encodedMessage = proximax_nem2_library_1.crypto.nemencrypt(privateKey, publicKey, proximax_nem2_library_1.convert.hexToUint8(proximax_nem2_library_1.convert.utf8ToHex(message)));
        return new SecureMessage(proximax_nem2_library_1.convert.uint8ToHex(encodedMessage), message);
    };
    /**
     *
     */
    SecureMessage.createFromDTO = function (payload) {
        return new SecureMessage(payload);
    };
    SecureMessage.prototype.decrypt = function (publicKey, privateKey) {
        var decodedMessage = proximax_nem2_library_1.crypto.nemdecrypt(privateKey, publicKey, proximax_nem2_library_1.convert.hexToUint8(this.hexEncodedPayload));
        return Message_1.Message.decodeHex(proximax_nem2_library_1.convert.uint8ToHex(decodedMessage));
    };
    return SecureMessage;
}(Message_1.Message));
exports.SecureMessage = SecureMessage;
//# sourceMappingURL=SecureMessage.js.map