import { Message } from './Message';
export declare class SecureMessage extends Message {
    static create(message: string, publicKey: string, privateKey: string): SecureMessage;
    /**
     *
     */
    static createFromDTO(payload: string): SecureMessage;
    /**
     * @internal
     * @param hexEncodedPayload
     * @param payload
     */
    constructor(hexEncodedPayload: string, payload?: string);
    decrypt(publicKey: string, privateKey: string): string;
}
