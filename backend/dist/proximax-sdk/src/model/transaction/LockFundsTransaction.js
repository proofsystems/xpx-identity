"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var proximax_nem2_library_1 = require("proximax-nem2-library");
var UInt64_1 = require("../UInt64");
var Transaction_1 = require("./Transaction");
var TransactionType_1 = require("./TransactionType");
/**
 * Lock funds transaction is used before sending an Aggregate bonded transaction, as a deposit to announce the transaction.
 * When aggregate bonded transaction is confirmed funds are returned to LockFundsTransaction signer.
 *
 * @since 1.0
 */
var LockFundsTransaction = /** @class */ (function (_super) {
    __extends(LockFundsTransaction, _super);
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param fee
     * @param mosaic
     * @param duration
     * @param signedTransaction
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    function LockFundsTransaction(networkType, version, deadline, fee, 
    /**
     * The locked mosaic.
     */
    mosaic, 
    /**
     * The funds lock duration.
     */
    duration, signedTransaction, signature, signer, transactionInfo) {
        var _this = _super.call(this, TransactionType_1.TransactionType.LOCK, networkType, version, deadline, fee, signature, signer, transactionInfo) || this;
        _this.mosaic = mosaic;
        _this.duration = duration;
        _this.hash = signedTransaction.hash;
        if (signedTransaction.type !== TransactionType_1.TransactionType.AGGREGATE_BONDED) {
            throw new Error('Signed transaction must be Aggregate Bonded Transaction');
        }
        return _this;
    }
    /**
     * Create a Lock funds transaction object
     * @param deadline - The deadline to include the transaction.
     * @param mosaic - The locked mosaic.
     * @param duration - The funds lock duration.
     * @param signedTransaction - The signed transaction for which funds are locked.
     * @param networkType - The network type.
     */
    LockFundsTransaction.create = function (deadline, mosaic, duration, signedTransaction, networkType) {
        return new LockFundsTransaction(networkType, 3, deadline, UInt64_1.UInt64.fromUint(0), mosaic, duration, signedTransaction);
    };
    /**
     * @internal
     * @return {VerifiableTransaction}
     */
    LockFundsTransaction.prototype.buildTransaction = function () {
        return new proximax_nem2_library_1.HashLockTransaction.Builder()
            .addDeadline(this.deadline.toDTO())
            .addType(this.type)
            .addFee(this.fee.toDTO())
            .addVersion(this.versionToDTO())
            .addMosaicId(this.mosaic.id.id.toDTO())
            .addMosaicAmount(this.mosaic.amount.toDTO())
            .addDuration(this.duration.toDTO())
            .addHash(this.hash)
            .build();
    };
    return LockFundsTransaction;
}(Transaction_1.Transaction));
exports.LockFundsTransaction = LockFundsTransaction;
//# sourceMappingURL=LockFundsTransaction.js.map