"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var proximax_nem2_library_1 = require("proximax-nem2-library");
var UInt64_1 = require("../UInt64");
var SignedTransaction_1 = require("./SignedTransaction");
var Transaction_1 = require("./Transaction");
var TransactionType_1 = require("./TransactionType");
/**
 * Aggregate innerTransactions contain multiple innerTransactions that can be initiated by different accounts.
 */
var AggregateTransaction = /** @class */ (function (_super) {
    __extends(AggregateTransaction, _super);
    /**
     * @param networkType
     * @param type
     * @param version
     * @param deadline
     * @param fee
     * @param innerTransactions
     * @param cosignatures
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    function AggregateTransaction(networkType, type, version, deadline, fee, 
    /**
     * The array of innerTransactions included in the aggregate transaction.
     */
    innerTransactions, 
    /**
     * The array of transaction cosigners signatures.
     */
    cosignatures, signature, signer, transactionInfo) {
        var _this = _super.call(this, type, networkType, version, deadline, fee, signature, signer, transactionInfo) || this;
        _this.innerTransactions = innerTransactions;
        _this.cosignatures = cosignatures;
        return _this;
    }
    /**
     * Create an aggregate complete transaction object
     * @param deadline - The deadline to include the transaction.
     * @param innerTransactions - The array of inner innerTransactions.
     * @param networkType - The network type.
     * @param cosignatures
     * @returns {AggregateTransaction}
     */
    AggregateTransaction.createComplete = function (deadline, innerTransactions, networkType, cosignatures) {
        return new AggregateTransaction(networkType, TransactionType_1.TransactionType.AGGREGATE_COMPLETE, 2, deadline, new UInt64_1.UInt64([0, 0]), innerTransactions, cosignatures);
    };
    /**
     * Create an aggregate bonded transaction object
     * @param {Deadline} deadline
     * @param {InnerTransaction[]} innerTransactions
     * @param {NetworkType} networkType
     * @param {AggregateTransactionCosignature[]} cosignatures
     * @return {AggregateTransaction}
     */
    AggregateTransaction.createBonded = function (deadline, innerTransactions, networkType, cosignatures) {
        if (cosignatures === void 0) { cosignatures = []; }
        return new AggregateTransaction(networkType, TransactionType_1.TransactionType.AGGREGATE_BONDED, 2, deadline, new UInt64_1.UInt64([0, 0]), innerTransactions, cosignatures);
    };
    /**
     * @internal
     * @returns {AggregateTransaction}
     */
    AggregateTransaction.prototype.buildTransaction = function () {
        return new proximax_nem2_library_1.AggregateTransaction.Builder()
            .addDeadline(this.deadline.toDTO())
            .addType(this.type)
            .addFee(this.fee.toDTO())
            .addVersion(this.versionToDTO())
            .addTransactions(this.innerTransactions.map(function (transaction) {
            return transaction.aggregateTransaction();
        })).build();
    };
    /**
     * @internal
     * Sign transaction with cosignatories creating a new SignedTransaction
     * @param initiatorAccount - Initiator account
     * @param cosignatories - The array of accounts that will cosign the transaction
     * @returns {SignedTransaction}
     */
    AggregateTransaction.prototype.signTransactionWithCosignatories = function (initiatorAccount, cosignatories) {
        var aggregateTransaction = this.buildTransaction();
        var signedTransactionRaw = aggregateTransaction.signTransactionWithCosigners(initiatorAccount, cosignatories);
        return new SignedTransaction_1.SignedTransaction(signedTransactionRaw.payload, signedTransactionRaw.hash, initiatorAccount.publicKey, this.type, this.networkType);
    };
    /**
     * Check if account has signed transaction
     * @param publicAccount - Signer public account
     * @returns {boolean}
     */
    AggregateTransaction.prototype.signedByAccount = function (publicAccount) {
        return this.cosignatures.find(function (cosignature) { return cosignature.signer.equals(publicAccount); }) !== undefined
            || (this.signer !== undefined && this.signer.equals(publicAccount));
    };
    return AggregateTransaction;
}(Transaction_1.Transaction));
exports.AggregateTransaction = AggregateTransaction;
//# sourceMappingURL=AggregateTransaction.js.map