export declare enum HashType {
    SHA3_512 = 0
}
export declare function HashTypeLengthValidator(hashType: HashType, input: string): boolean;
