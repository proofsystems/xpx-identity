"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var proximax_nem2_library_1 = require("proximax-nem2-library");
var UInt64_1 = require("../UInt64");
var Transaction_1 = require("./Transaction");
var TransactionType_1 = require("./TransactionType");
/**
 * Modify multisig account transactions are part of the NEM's multisig account system.
 * A modify multisig account transaction holds an array of multisig cosignatory modifications,
 * min number of signatures to approve a transaction and a min number of signatures to remove a cosignatory.
 * @since 1.0
 */
var ModifyMultisigAccountTransaction = /** @class */ (function (_super) {
    __extends(ModifyMultisigAccountTransaction, _super);
    /**
     * @param networkType
     * @param version
     * @param deadline
     * @param fee
     * @param minApprovalDelta
     * @param minRemovalDelta
     * @param modifications
     * @param signature
     * @param signer
     * @param transactionInfo
     */
    function ModifyMultisigAccountTransaction(networkType, version, deadline, fee, 
    /**
     * The number of signatures needed to approve a transaction.
     * If we are modifying and existing multi-signature account this indicates the relative change of the minimum cosignatories.
     */
    minApprovalDelta, 
    /**
     * The number of signatures needed to remove a cosignatory.
     * If we are modifying and existing multi-signature account this indicates the relative change of the minimum cosignatories.
     */
    minRemovalDelta, 
    /**
     * The array of cosigner accounts added or removed from the multi-signature account.
     */
    modifications, signature, signer, transactionInfo) {
        var _this = _super.call(this, TransactionType_1.TransactionType.MODIFY_MULTISIG_ACCOUNT, networkType, version, deadline, fee, signature, signer, transactionInfo) || this;
        _this.minApprovalDelta = minApprovalDelta;
        _this.minRemovalDelta = minRemovalDelta;
        _this.modifications = modifications;
        return _this;
    }
    /**
     * Create a modify multisig account transaction object
     * @param deadline - The deadline to include the transaction.
     * @param minApprovalDelta - The min approval relative change.
     * @param minRemovalDelta - The min removal relative change.
     * @param modifications - The array of modifications.
     * @param networkType - The network type.
     * @returns {ModifyMultisigAccountTransaction}
     */
    ModifyMultisigAccountTransaction.create = function (deadline, minApprovalDelta, minRemovalDelta, modifications, networkType) {
        return new ModifyMultisigAccountTransaction(networkType, 3, deadline, new UInt64_1.UInt64([0, 0]), minApprovalDelta, minRemovalDelta, modifications);
    };
    /**
     * @internal
     * @returns {VerifiableTransaction}
     */
    ModifyMultisigAccountTransaction.prototype.buildTransaction = function () {
        return new proximax_nem2_library_1.MultisigModificationTransaction.Builder()
            .addDeadline(this.deadline.toDTO())
            .addFee(this.fee.toDTO())
            .addVersion(this.versionToDTO())
            .addMinApprovalDelta(this.minApprovalDelta)
            .addMinRemovalDelta(this.minRemovalDelta)
            .addModifications(this.modifications.map(function (modification) { return modification.toDTO(); }))
            .build();
    };
    return ModifyMultisigAccountTransaction;
}(Transaction_1.Transaction));
exports.ModifyMultisigAccountTransaction = ModifyMultisigAccountTransaction;
//# sourceMappingURL=ModifyMultisigAccountTransaction.js.map