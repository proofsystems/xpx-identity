/**
 * An abstract message class that serves as the base class of all message types.
 */
export declare abstract class Message {
    readonly type: number;
    /**
     * Message encoded payload in hex
     */
    readonly hexEncodedPayload: string;
    /**
     * Message raw payload
     */
    readonly payload?: string;
    /**
     * @internal
     * @param hex
     * @returns {string}
     */
    static decodeHex(hex: string): string;
    /**
     * @internal
     * @param type
     * @param hexEncodedPayload
     * @param payload
     */
    constructor(/**
                 * Message type
                 */ type: number, 
    /**
     * Message encoded payload in hex
     */
    hexEncodedPayload: string, 
    /**
     * Message raw payload
     */
    payload?: string);
}
