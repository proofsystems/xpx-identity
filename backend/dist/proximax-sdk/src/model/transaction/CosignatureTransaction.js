"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var proximax_nem2_library_1 = require("proximax-nem2-library");
var CosignatureSignedTransaction_1 = require("./CosignatureSignedTransaction");
/**
 * Cosignature transaction is used to sign an aggregate transactions with missing cosignatures.
 */
var CosignatureTransaction = /** @class */ (function () {
    /**
     * @param transactionToCosign
     */
    function CosignatureTransaction(/**
                 * Transaction to cosign.
                 */ transactionToCosign) {
        this.transactionToCosign = transactionToCosign;
    }
    /**
     * Create a cosignature transaction
     * @param transactionToCosign - Transaction to cosign.
     * @returns {CosignatureTransaction}
     */
    CosignatureTransaction.create = function (transactionToCosign) {
        if (transactionToCosign.isUnannounced()) {
            throw new Error('transaction to cosign should be announced first');
        }
        return new CosignatureTransaction(transactionToCosign);
    };
    /**
     * @internal
     * Serialize and sign transaction creating a new SignedTransaction
     * @param account
     * @returns {CosignatureSignedTransaction}
     */
    CosignatureTransaction.prototype.signWith = function (account) {
        var aggregateSignatureTransaction = new proximax_nem2_library_1.CosignatureTransaction(this.transactionToCosign.transactionInfo.hash);
        var signedTransactionRaw = aggregateSignatureTransaction.signCosignatoriesTransaction(account);
        return new CosignatureSignedTransaction_1.CosignatureSignedTransaction(signedTransactionRaw.parentHash, signedTransactionRaw.signature, signedTransactionRaw.signer);
    };
    return CosignatureTransaction;
}());
exports.CosignatureTransaction = CosignatureTransaction;
//# sourceMappingURL=CosignatureTransaction.js.map