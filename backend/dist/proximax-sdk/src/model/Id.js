"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var UInt64_1 = require("./UInt64");
/**
 * This class is used to define mosaicIds and namespaceIds
 */
var Id = /** @class */ (function (_super) {
    __extends(Id, _super);
    function Id() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Id.fromHex = function (hexId) {
        var higher = parseInt(hexId.substr(0, 8), 16);
        var lower = parseInt(hexId.substr(8, 8), 16);
        return new Id([lower, higher]);
    };
    /**
     * Get string value of id
     * @returns {string}
     */
    Id.prototype.toHex = function () {
        var part1 = this.higher.toString(16);
        var part2 = this.lower.toString(16);
        return this.pad(part1, 8) + this.pad(part2, 8);
    };
    /**
     * @param str
     * @param maxVal
     * @returns {string}
     */
    Id.prototype.pad = function (str, maxVal) {
        return (str.length < maxVal ? this.pad("0" + str, maxVal) : str);
    };
    return Id;
}(UInt64_1.UInt64));
exports.Id = Id;
//# sourceMappingURL=Id.js.map