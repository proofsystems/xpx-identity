"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var proximax_nem2_library_1 = require("proximax-nem2-library");
var Address_1 = require("./Address");
var PublicAccount_1 = require("./PublicAccount");
/**
 * The account structure describes an account private key, public key, address and allows signing transactions.
 */
var Account = /** @class */ (function () {
    /**
     * @internal
     * @param address
     * @param keyPair
     */
    function Account(
    /**
     * The account address.
     */
    address, 
    /**
     * The account keyPair, public and private key.
     */
    keyPair) {
        this.address = address;
        this.keyPair = keyPair;
    }
    /**
     * Create an Account from a given private key
     * @param privateKey - Private key from an account
     * @param networkType - Network type
     * @return {Account}
     */
    Account.createFromPrivateKey = function (privateKey, networkType) {
        var keyPair = proximax_nem2_library_1.KeyPair.createKeyPairFromPrivateKeyString(privateKey);
        var address = proximax_nem2_library_1.address.addressToString(proximax_nem2_library_1.address.publicKeyToAddress(keyPair.publicKey, networkType));
        return new Account(Address_1.Address.createFromRawAddress(address), keyPair);
    };
    Account.generateNewAccount = function (networkType) {
        // Create random bytes
        var randomBytesArray = proximax_nem2_library_1.nacl_catapult.randomBytes(32);
        // Hash random bytes with entropy seed
        // Finalize and keep only 32 bytes
        var hashKey = proximax_nem2_library_1.convert.uint8ToHex(randomBytesArray); // TODO: derive private key correctly
        // Create KeyPair from hash key
        var keyPair = proximax_nem2_library_1.KeyPair.createKeyPairFromPrivateKeyString(hashKey);
        var address = Address_1.Address.createFromPublicKey(proximax_nem2_library_1.convert.uint8ToHex(keyPair.publicKey), networkType);
        return new Account(address, keyPair);
    };
    Object.defineProperty(Account.prototype, "publicKey", {
        /**
         * Account public key.
         * @return {string}
         */
        get: function () {
            return proximax_nem2_library_1.convert.uint8ToHex(this.keyPair.publicKey);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Account.prototype, "publicAccount", {
        /**
         * Public account.
         * @return {PublicAccount}
         */
        get: function () {
            return PublicAccount_1.PublicAccount.createFromPublicKey(this.publicKey, this.address.networkType);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Account.prototype, "privateKey", {
        /**
         * Account private key.
         * @return {string}
         */
        get: function () {
            return proximax_nem2_library_1.convert.uint8ToHex(this.keyPair.privateKey);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Sign a transaction
     * @param transaction - The transaction to be signed.
     * @return {SignedTransaction}
     */
    Account.prototype.sign = function (transaction) {
        return transaction.signWith(this);
    };
    /**
     * Sign transaction with cosignatories creating a new SignedTransaction
     * @param transaction - The aggregate transaction to be signed.
     * @param cosignatories - The array of accounts that will cosign the transaction
     * @return {SignedTransaction}
     */
    Account.prototype.signTransactionWithCosignatories = function (transaction, cosignatories) {
        return transaction.signTransactionWithCosignatories(this, cosignatories);
    };
    /**
     * Sign aggregate signature transaction
     * @param cosignatureTransaction - The aggregate signature transaction.
     * @return {CosignatureSignedTransaction}
     */
    Account.prototype.signCosignatureTransaction = function (cosignatureTransaction) {
        return cosignatureTransaction.signWith(this);
    };
    /**
     * Sign raw data
     * @param data - Data to be signed
     * @return {string} - Signed data result
     */
    Account.prototype.signData = function (data) {
        return proximax_nem2_library_1.convert.uint8ToHex(proximax_nem2_library_1.KeyPair.sign(this.keyPair, proximax_nem2_library_1.convert.hexToUint8(data)));
    };
    return Account;
}());
exports.Account = Account;
//# sourceMappingURL=Account.js.map