"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * The mosaic info structure describes a mosaic.
 */
var MosaicInfo = /** @class */ (function () {
    /**
     * @param active
     * @param index
     * @param metaId
     * @param namespaceId
     * @param mosaicId
     * @param supply
     * @param height
     * @param owner
     * @param properties
     * @param levy
     */
    function MosaicInfo(/**
                 * Mosaic is active.
                 */ active, 
    /**
     * The mosaic index.
     */
    index, 
    /**
     * The meta data id.
     */
    metaId, 
    /**
     * The namespace id.
     */
    namespaceId, 
    /**
     * The mosaic id.
     */
    mosaicId, 
    /**
     * The mosaic supply.
     */
    supply, 
    /**
     * The block height were mosaic was created.
     */
    height, 
    /**
     * The public key of the mosaic creator.
     */
    owner, 
    /**
     * The mosaic properties.
     */
    properties, 
    /**
     * The optional levy for the mosaic. A creator can demand that each mosaic transfer induces an additional fee.
     */
    levy) {
        this.active = active;
        this.index = index;
        this.metaId = metaId;
        this.namespaceId = namespaceId;
        this.mosaicId = mosaicId;
        this.supply = supply;
        this.height = height;
        this.owner = owner;
        this.properties = properties;
        this.levy = levy;
    }
    Object.defineProperty(MosaicInfo.prototype, "divisibility", {
        /**
         * Mosaic divisibility
         * @returns {number}
         */
        get: function () {
            return this.properties.divisibility;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MosaicInfo.prototype, "duration", {
        /**
         * Mosaic duration
         * @returns {UInt64}
         */
        get: function () {
            return this.properties.duration;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Is mosaic supply mutable
     * @returns {boolean}
     */
    MosaicInfo.prototype.isSupplyMutable = function () {
        return this.properties.supplyMutable;
    };
    /**
     * Is mosaic transferable
     * @returns {boolean}
     */
    MosaicInfo.prototype.isTransferable = function () {
        return this.properties.transferable;
    };
    /**
     * Is levy mutable
     * @returns {boolean}
     */
    MosaicInfo.prototype.isLevyMutable = function () {
        return this.properties.levyMutable;
    };
    return MosaicInfo;
}());
exports.MosaicInfo = MosaicInfo;
//# sourceMappingURL=MosaicInfo.js.map