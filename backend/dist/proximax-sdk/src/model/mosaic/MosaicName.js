"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * The mosaic name info structure describes basic information of a mosaic and name.
 */
var MosaicName = /** @class */ (function () {
    /**
     * @param mosaicId
     * @param namespaceId
     * @param name
     */
    function MosaicName(/**
                 * The mosaic id.
                 */ mosaicId, 
    /**
     * The namespace id.
     */
    namespaceId, 
    /**
     * The mosaic name.
     */
    name) {
        this.mosaicId = mosaicId;
        this.namespaceId = namespaceId;
        this.name = name;
    }
    return MosaicName;
}());
exports.MosaicName = MosaicName;
//# sourceMappingURL=MosaicName.js.map