"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var NamespaceId_1 = require("../namespace/NamespaceId");
var UInt64_1 = require("../UInt64");
var Mosaic_1 = require("./Mosaic");
var MosaicId_1 = require("./MosaicId");
/**
 * XEM mosaic
 */
var XPX = /** @class */ (function (_super) {
    __extends(XPX, _super);
    /**
     * constructor
     * @param amount
     */
    function XPX(amount) {
        return _super.call(this, XPX.MOSAIC_ID, amount) || this;
    }
    /**
     * Create xem with using xem as unit.
     *
     * @param amount
     * @returns {XPX}
     */
    XPX.createRelative = function (amount) {
        if (typeof amount === 'number') {
            return new XPX(UInt64_1.UInt64.fromUint(amount * Math.pow(10, XPX.DIVISIBILITY)));
        }
        return new XPX(UInt64_1.UInt64.fromUint(amount.compact() * Math.pow(10, XPX.DIVISIBILITY)));
    };
    /**
     * Create xem with using micro xem as unit, 1 XEM = 1000000 micro XEM.
     *
     * @param amount
     * @returns {XPX}
     */
    XPX.createAbsolute = function (amount) {
        if (typeof amount === 'number') {
            return new XPX(UInt64_1.UInt64.fromUint(amount));
        }
        return new XPX(amount);
    };
    /**
     * Divisiblity
     * @type {number}
     */
    XPX.DIVISIBILITY = 6;
    /**
     * Initial supply
     * @type {number}
     */
    XPX.INITIAL_SUPPLY = 8999999999;
    /**
     * Is tranferable
     * @type {boolean}
     */
    XPX.TRANSFERABLE = true;
    /**
     * Is mutable
     * @type {boolean}
     */
    XPX.SUPPLY_MUTABLE = false;
    /**
     * mosaicId
     * @type {Id}
     */
    XPX.MOSAIC_ID = new MosaicId_1.MosaicId([
        481110499,
        231112638
    ]);
    /**
     * namespaceId
     * @type {Id}
     */
    XPX.NAMESPACE_ID = new NamespaceId_1.NamespaceId('prx');
    return XPX;
}(Mosaic_1.Mosaic));
exports.XPX = XPX;
//# sourceMappingURL=XPX.js.map