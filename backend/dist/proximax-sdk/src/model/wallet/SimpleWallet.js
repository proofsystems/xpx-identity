"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var proximax_nem2_library_1 = require("proximax-nem2-library");
var js_joda_1 = require("js-joda");
var Account_1 = require("../account/Account");
var Address_1 = require("../account/Address");
var EncryptedPrivateKey_1 = require("./EncryptedPrivateKey");
var Wallet_1 = require("./Wallet");
/**
 * Simple wallet model generates a private key from a PRNG
 */
var SimpleWallet = /** @class */ (function (_super) {
    __extends(SimpleWallet, _super);
    /**
     * @internal
     * @param name
     * @param network
     * @param address
     * @param creationDate
     * @param encryptedPrivateKey
     */
    function SimpleWallet(name, network, address, creationDate, 
    /**
     * The encrypted private key and information to decrypt it
     */
    encryptedPrivateKey) {
        var _this = _super.call(this, name, network, address, creationDate, 'simple_v1') || this;
        _this.encryptedPrivateKey = encryptedPrivateKey;
        return _this;
    }
    /**
     * Create a Simple wallet
     * @param name - Wallet name
     * @param password - Password to encrypt wallet
     * @param network - Network id
     * @returns {SimpleWallet}
     */
    SimpleWallet.create = function (name, password, network) {
        // Create random bytes
        var randomBytesArray = proximax_nem2_library_1.nacl_catapult.randomBytes(32);
        // Hash random bytes with entropy seed
        // Finalize and keep only 32 bytes
        var hashKey = proximax_nem2_library_1.convert.uint8ToHex(randomBytesArray); // TODO: derive private key correctly
        // Create KeyPair from hash key
        var keyPair = proximax_nem2_library_1.KeyPair.createKeyPairFromPrivateKeyString(hashKey);
        // Create address from public key
        var address = Address_1.Address.createFromPublicKey(proximax_nem2_library_1.convert.uint8ToHex(keyPair.publicKey), network);
        // Encrypt private key using password
        var encrypted = proximax_nem2_library_1.crypto.encodePrivKey(hashKey, password.value);
        var encryptedPrivateKey = new EncryptedPrivateKey_1.EncryptedPrivateKey(encrypted.ciphertext, encrypted.iv);
        return new SimpleWallet(name, network, address, js_joda_1.LocalDateTime.now(), encryptedPrivateKey);
    };
    /**
     * Create a SimpleWallet from private key
     * @param name - Wallet name
     * @param password - Password to encrypt wallet
     * @param privateKey - Wallet private key
     * @param network - Network id
     * @returns {SimpleWallet}
     */
    SimpleWallet.createFromPrivateKey = function (name, password, privateKey, network) {
        // Create KeyPair from hash key
        var keyPair = proximax_nem2_library_1.KeyPair.createKeyPairFromPrivateKeyString(privateKey);
        // Create address from public key
        var address = Address_1.Address.createFromPublicKey(proximax_nem2_library_1.convert.uint8ToHex(keyPair.publicKey), network);
        // Encrypt private key using password
        var encrypted = proximax_nem2_library_1.crypto.encodePrivKey(privateKey, password.value);
        var encryptedPrivateKey = new EncryptedPrivateKey_1.EncryptedPrivateKey(encrypted.ciphertext, encrypted.iv);
        return new SimpleWallet(name, network, address, js_joda_1.LocalDateTime.now(), encryptedPrivateKey);
    };
    /**
     * Open a wallet and generate an Account
     * @param password - Password to decrypt private key
     * @returns {Account}
     */
    SimpleWallet.prototype.open = function (password) {
        return Account_1.Account.createFromPrivateKey(this.encryptedPrivateKey.decrypt(password), this.network);
    };
    return SimpleWallet;
}(Wallet_1.Wallet));
exports.SimpleWallet = SimpleWallet;
//# sourceMappingURL=SimpleWallet.js.map