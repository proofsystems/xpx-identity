"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Object containing information of a namespace.
 */
var NamespaceInfo = /** @class */ (function () {
    /**
     * @param active
     * @param index
     * @param metaId
     * @param type
     * @param depth
     * @param levels
     * @param parentId
     * @param owner
     * @param startHeight
     * @param endHeight
     */
    function NamespaceInfo(/**
                 * Namespace is active.
                 */ active, 
    /**
     * The namespace index.
     */
    index, 
    /**
     * The meta data id.
     */
    metaId, 
    /**
     * The namespace type, namespace and sub namespace.
     */
    type, 
    /**
     * The level of namespace.
     */
    depth, 
    /**
     * The namespace id levels.
     */
    levels, 
    /**
     * The namespace parent id.
     */
    parentId, 
    /**
     * The owner of the namespace.
     */
    owner, 
    /**
     * The height at which the ownership begins.
     */
    startHeight, 
    /**
     * The height at which the ownership ends.
     */
    endHeight) {
        this.active = active;
        this.index = index;
        this.metaId = metaId;
        this.type = type;
        this.depth = depth;
        this.levels = levels;
        this.parentId = parentId;
        this.owner = owner;
        this.startHeight = startHeight;
        this.endHeight = endHeight;
    }
    Object.defineProperty(NamespaceInfo.prototype, "id", {
        /**
         * Namespace id
         * @returns {Id}
         */
        get: function () {
            return this.levels[this.levels.length - 1];
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Is root namespace
     * @returns {boolean}
     */
    NamespaceInfo.prototype.isRoot = function () {
        return this.type === 0;
    };
    /**
     * Is sub namepsace
     * @returns {boolean}
     */
    NamespaceInfo.prototype.isSubnamespace = function () {
        return this.type === 1;
    };
    /**
     * Get parent id
     * @returns {Id}
     */
    NamespaceInfo.prototype.parentNamespaceId = function () {
        if (this.isRoot()) {
            throw new Error('Is a Root Namespace');
        }
        return this.parentId;
    };
    return NamespaceInfo;
}());
exports.NamespaceInfo = NamespaceInfo;
//# sourceMappingURL=NamespaceInfo.js.map