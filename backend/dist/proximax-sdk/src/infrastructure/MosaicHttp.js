"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var proximax_nem2_library_1 = require("proximax-nem2-library");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var PublicAccount_1 = require("../model/account/PublicAccount");
var MosaicId_1 = require("../model/mosaic/MosaicId");
var MosaicInfo_1 = require("../model/mosaic/MosaicInfo");
var MosaicName_1 = require("../model/mosaic/MosaicName");
var MosaicProperties_1 = require("../model/mosaic/MosaicProperties");
var NamespaceId_1 = require("../model/namespace/NamespaceId");
var UInt64_1 = require("../model/UInt64");
var Http_1 = require("./Http");
var NetworkHttp_1 = require("./NetworkHttp");
/**
 * Mosaic http repository.
 *
 * @since 1.0
 */
var MosaicHttp = /** @class */ (function (_super) {
    __extends(MosaicHttp, _super);
    /**
     * Constructor
     * @param url
     * @param networkHttp
     */
    function MosaicHttp(url, networkHttp) {
        var _this = this;
        networkHttp = networkHttp == null ? new NetworkHttp_1.NetworkHttp(url) : networkHttp;
        _this = _super.call(this, url, networkHttp) || this;
        _this.mosaicRoutesApi = new proximax_nem2_library_1.MosaicRoutesApi(_this.apiClient);
        return _this;
    }
    /**
     * Gets the MosaicInfo for a given mosaicId
     * @param mosaicId - Mosaic id
     * @returns Observable<MosaicInfo>
     */
    MosaicHttp.prototype.getMosaic = function (mosaicId) {
        var _this = this;
        return this.getNetworkTypeObservable().pipe(operators_1.mergeMap(function (networkType) { return rxjs_1.from(_this.mosaicRoutesApi.getMosaic(mosaicId.toHex())).pipe(operators_1.map(function (mosaicInfoDTO) {
            return new MosaicInfo_1.MosaicInfo(mosaicInfoDTO.meta.active, mosaicInfoDTO.meta.index, mosaicInfoDTO.meta.id, new NamespaceId_1.NamespaceId(mosaicInfoDTO.mosaic.namespaceId), new MosaicId_1.MosaicId(mosaicInfoDTO.mosaic.mosaicId), new UInt64_1.UInt64(mosaicInfoDTO.mosaic.supply), new UInt64_1.UInt64(mosaicInfoDTO.mosaic.height), PublicAccount_1.PublicAccount.createFromPublicKey(mosaicInfoDTO.mosaic.owner, networkType), new MosaicProperties_1.MosaicProperties(new UInt64_1.UInt64(mosaicInfoDTO.mosaic.properties[0]), (new UInt64_1.UInt64(mosaicInfoDTO.mosaic.properties[1])).compact(), new UInt64_1.UInt64(mosaicInfoDTO.mosaic.properties[2])), mosaicInfoDTO.mosaic.levy);
        })); }));
    };
    /**
     * Gets MosaicInfo for different mosaicIds.
     * @param mosaicIds - Array of mosaic ids
     * @returns Observable<MosaicInfo[]>
     */
    MosaicHttp.prototype.getMosaics = function (mosaicIds) {
        var _this = this;
        var mosaicIdsBody = {
            mosaicIds: mosaicIds.map(function (id) { return id.toHex(); }),
        };
        return this.getNetworkTypeObservable().pipe(operators_1.mergeMap(function (networkType) { return rxjs_1.from(_this.mosaicRoutesApi.getMosaics(mosaicIdsBody)).pipe(operators_1.map(function (mosaicInfosDTO) {
            return mosaicInfosDTO.map(function (mosaicInfoDTO) {
                return new MosaicInfo_1.MosaicInfo(mosaicInfoDTO.meta.active, mosaicInfoDTO.meta.index, mosaicInfoDTO.meta.id, new NamespaceId_1.NamespaceId(mosaicInfoDTO.mosaic.namespaceId), new MosaicId_1.MosaicId(mosaicInfoDTO.mosaic.mosaicId), new UInt64_1.UInt64(mosaicInfoDTO.mosaic.supply), new UInt64_1.UInt64(mosaicInfoDTO.mosaic.height), PublicAccount_1.PublicAccount.createFromPublicKey(mosaicInfoDTO.mosaic.owner, networkType), new MosaicProperties_1.MosaicProperties(new UInt64_1.UInt64(mosaicInfoDTO.mosaic.properties[0]), (new UInt64_1.UInt64(mosaicInfoDTO.mosaic.properties[1])).compact(), new UInt64_1.UInt64(mosaicInfoDTO.mosaic.properties[2])), mosaicInfoDTO.mosaic.levy);
            });
        })); }));
    };
    /**
     * Gets array of MosaicInfo from mosaics created with provided namespace
     * @param namespaceId - Namespace id
     * @param queryParams - (Optional) Query params
     * @returns Observable<MosaicInfo[]>
     */
    MosaicHttp.prototype.getMosaicsFromNamespace = function (namespaceId, queryParams) {
        var _this = this;
        return this.getNetworkTypeObservable().pipe(operators_1.mergeMap(function (networkType) { return rxjs_1.from(_this.mosaicRoutesApi.getMosaicsFromNamespace(namespaceId.toHex(), queryParams != null ? queryParams : {})).pipe(operators_1.map(function (mosaicsDefinitionDTO) {
            return mosaicsDefinitionDTO.map(function (mosaicInfoDTO) {
                return new MosaicInfo_1.MosaicInfo(mosaicInfoDTO.meta.active, mosaicInfoDTO.meta.index, mosaicInfoDTO.meta.id, new NamespaceId_1.NamespaceId(mosaicInfoDTO.mosaic.namespaceId), new MosaicId_1.MosaicId(mosaicInfoDTO.mosaic.mosaicId), new UInt64_1.UInt64(mosaicInfoDTO.mosaic.supply), new UInt64_1.UInt64(mosaicInfoDTO.mosaic.height), PublicAccount_1.PublicAccount.createFromPublicKey(mosaicInfoDTO.mosaic.owner, networkType), new MosaicProperties_1.MosaicProperties(new UInt64_1.UInt64(mosaicInfoDTO.mosaic.properties[0]), (new UInt64_1.UInt64(mosaicInfoDTO.mosaic.properties[1])).compact(), new UInt64_1.UInt64(mosaicInfoDTO.mosaic.properties[2])), mosaicInfoDTO.mosaic.levy);
            });
        })); }));
    };
    /**
     * Gets array of MosaicName for different mosaicIds
     * @param mosaicIds - Array of mosaic ids
     * @returns Observable<MosaicName[]>
     */
    MosaicHttp.prototype.getMosaicsName = function (mosaicIds) {
        var mosaicIdsBody = {
            mosaicIds: mosaicIds.map(function (id) { return id.toHex(); }),
        };
        return rxjs_1.from(this.mosaicRoutesApi.getMosaicsName(mosaicIdsBody)).pipe(operators_1.map(function (mosaicInfosDTO) {
            return mosaicInfosDTO.map(function (mosaicInfoDTO) {
                return new MosaicName_1.MosaicName(new MosaicId_1.MosaicId(mosaicInfoDTO.mosaicId), new NamespaceId_1.NamespaceId(mosaicInfoDTO.parentId), mosaicInfoDTO.name);
            });
        }));
    };
    return MosaicHttp;
}(Http_1.Http));
exports.MosaicHttp = MosaicHttp;
//# sourceMappingURL=MosaicHttp.js.map