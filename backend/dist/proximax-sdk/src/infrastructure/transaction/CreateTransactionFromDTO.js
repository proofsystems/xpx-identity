"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var Address_1 = require("../../model/account/Address");
var PublicAccount_1 = require("../../model/account/PublicAccount");
var NetworkType_1 = require("../../model/blockchain/NetworkType");
var Mosaic_1 = require("../../model/mosaic/Mosaic");
var MosaicId_1 = require("../../model/mosaic/MosaicId");
var MosaicProperties_1 = require("../../model/mosaic/MosaicProperties");
var NamespaceId_1 = require("../../model/namespace/NamespaceId");
var AggregateTransaction_1 = require("../../model/transaction/AggregateTransaction");
var AggregateTransactionCosignature_1 = require("../../model/transaction/AggregateTransactionCosignature");
var AggregateTransactionInfo_1 = require("../../model/transaction/AggregateTransactionInfo");
var Deadline_1 = require("../../model/transaction/Deadline");
var LockFundsTransaction_1 = require("../../model/transaction/LockFundsTransaction");
var ModifyMultisigAccountTransaction_1 = require("../../model/transaction/ModifyMultisigAccountTransaction");
var MosaicDefinitionTransaction_1 = require("../../model/transaction/MosaicDefinitionTransaction");
var MosaicSupplyChangeTransaction_1 = require("../../model/transaction/MosaicSupplyChangeTransaction");
var MultisigCosignatoryModification_1 = require("../../model/transaction/MultisigCosignatoryModification");
var PlainMessage_1 = require("../../model/transaction/PlainMessage");
var RegisterNamespaceTransaction_1 = require("../../model/transaction/RegisterNamespaceTransaction");
var SecretLockTransaction_1 = require("../../model/transaction/SecretLockTransaction");
var SecretProofTransaction_1 = require("../../model/transaction/SecretProofTransaction");
var SignedTransaction_1 = require("../../model/transaction/SignedTransaction");
var TransactionInfo_1 = require("../../model/transaction/TransactionInfo");
var TransactionType_1 = require("../../model/transaction/TransactionType");
var TransferTransaction_1 = require("../../model/transaction/TransferTransaction");
var UInt64_1 = require("../../model/UInt64");
var SecureMessage_1 = require("../../model/transaction/SecureMessage");
/**
 * @internal
 * @param transactionDTO
 * @returns {Transaction}
 * @constructor
 */
exports.CreateTransactionFromDTO = function (transactionDTO) {
    if (transactionDTO.transaction.type === TransactionType_1.TransactionType.AGGREGATE_COMPLETE ||
        transactionDTO.transaction.type === TransactionType_1.TransactionType.AGGREGATE_BONDED) {
        var innerTransactions = transactionDTO.transaction.transactions.map(function (innerTransactionDTO) {
            var aggregateTransactionInfo = innerTransactionDTO.meta ? new AggregateTransactionInfo_1.AggregateTransactionInfo(new UInt64_1.UInt64(innerTransactionDTO.meta.height), innerTransactionDTO.meta.index, innerTransactionDTO.meta.id, innerTransactionDTO.meta.aggregateHash, innerTransactionDTO.meta.aggregateId) : undefined;
            innerTransactionDTO.transaction.fee = transactionDTO.transaction.fee;
            innerTransactionDTO.transaction.deadline = transactionDTO.transaction.deadline;
            innerTransactionDTO.transaction.signature = transactionDTO.transaction.signature;
            return CreateStandaloneTransactionFromDTO(innerTransactionDTO.transaction, aggregateTransactionInfo);
        });
        return new AggregateTransaction_1.AggregateTransaction(extractNetworkType(transactionDTO.transaction.version), transactionDTO.transaction.type, extractTransactionVersion(transactionDTO.transaction.version), Deadline_1.Deadline.createFromDTO(transactionDTO.transaction.deadline), new UInt64_1.UInt64(transactionDTO.transaction.fee), innerTransactions, transactionDTO.transaction.cosignatures ? transactionDTO.transaction.cosignatures
            .map(function (aggregateCosignatureDTO) {
            return new AggregateTransactionCosignature_1.AggregateTransactionCosignature(aggregateCosignatureDTO.signature, PublicAccount_1.PublicAccount.createFromPublicKey(aggregateCosignatureDTO.signer, extractNetworkType(transactionDTO.transaction.version)));
        }) : [], transactionDTO.transaction.signature, PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.transaction.signer, extractNetworkType(transactionDTO.transaction.version)), new TransactionInfo_1.TransactionInfo(new UInt64_1.UInt64(transactionDTO.meta.height), transactionDTO.meta.index, transactionDTO.meta.id, transactionDTO.meta.hash, transactionDTO.meta.merkleComponentHash));
    }
    else {
        var transactionInfo = new TransactionInfo_1.TransactionInfo(new UInt64_1.UInt64(transactionDTO.meta.height), transactionDTO.meta.index, transactionDTO.meta.id, transactionDTO.meta.hash, transactionDTO.meta.merkleComponentHash);
        return CreateStandaloneTransactionFromDTO(transactionDTO.transaction, transactionInfo);
    }
};
/**
 * @internal
 * @param transactionDTO
 * @param transactionInfo
 * @returns {any}
 * @constructor
 */
var CreateStandaloneTransactionFromDTO = function (transactionDTO, transactionInfo) {
    if (transactionDTO.type === TransactionType_1.TransactionType.TRANSFER) {
        var message = void 0;
        if (transactionDTO.message && transactionDTO.message.type === 0) {
            message = PlainMessage_1.PlainMessage.createFromDTO(transactionDTO.message.payload);
        }
        else if (transactionDTO.message && transactionDTO.message.type === 1) {
            message = SecureMessage_1.SecureMessage.createFromDTO(transactionDTO.message.payload);
        }
        else {
            message = PlainMessage_1.EmptyMessage;
        }
        return new TransferTransaction_1.TransferTransaction(extractNetworkType(transactionDTO.version), extractTransactionVersion(transactionDTO.version), Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), new UInt64_1.UInt64(transactionDTO.maxFee), Address_1.Address.createFromEncoded(transactionDTO.recipient), transactionDTO.mosaics === undefined ? [] :
            transactionDTO.mosaics
                .map(function (mosaicDTO) { return new Mosaic_1.Mosaic(new MosaicId_1.MosaicId(mosaicDTO.id), new UInt64_1.UInt64(mosaicDTO.amount)); }), message, transactionDTO.signature, PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signer, extractNetworkType(transactionDTO.version)), transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.REGISTER_NAMESPACE) {
        return new RegisterNamespaceTransaction_1.RegisterNamespaceTransaction(extractNetworkType(transactionDTO.version), extractTransactionVersion(transactionDTO.version), Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), new UInt64_1.UInt64(transactionDTO.maxFee), transactionDTO.namespaceType, transactionDTO.name, new NamespaceId_1.NamespaceId(transactionDTO.namespaceId), transactionDTO.namespaceType === 0 ? new UInt64_1.UInt64(transactionDTO.duration) : undefined, transactionDTO.namespaceType === 1 ? new NamespaceId_1.NamespaceId(transactionDTO.parentId) : undefined, transactionDTO.signature, PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signer, extractNetworkType(transactionDTO.version)), transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.MOSAIC_DEFINITION) {
        return new MosaicDefinitionTransaction_1.MosaicDefinitionTransaction(extractNetworkType(transactionDTO.version), extractTransactionVersion(transactionDTO.version), Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), new UInt64_1.UInt64(transactionDTO.maxFee), new NamespaceId_1.NamespaceId(transactionDTO.parentId), new MosaicId_1.MosaicId(transactionDTO.mosaicId), transactionDTO.name, new MosaicProperties_1.MosaicProperties(new UInt64_1.UInt64(transactionDTO.properties[0].value), (new UInt64_1.UInt64(transactionDTO.properties[1].value)).compact(), new UInt64_1.UInt64(transactionDTO.properties.length === 3 ? transactionDTO.properties[2].value : [0, 0])), transactionDTO.signature, PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signer, extractNetworkType(transactionDTO.version)), transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.MOSAIC_SUPPLY_CHANGE) {
        return new MosaicSupplyChangeTransaction_1.MosaicSupplyChangeTransaction(extractNetworkType(transactionDTO.version), extractTransactionVersion(transactionDTO.version), Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), new UInt64_1.UInt64(transactionDTO.maxFee), new MosaicId_1.MosaicId(transactionDTO.mosaicId), transactionDTO.direction, new UInt64_1.UInt64(transactionDTO.delta), transactionDTO.signature, PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signer, extractNetworkType(transactionDTO.version)), transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.MODIFY_MULTISIG_ACCOUNT) {
        return new ModifyMultisigAccountTransaction_1.ModifyMultisigAccountTransaction(extractNetworkType(transactionDTO.version), extractTransactionVersion(transactionDTO.version), Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), new UInt64_1.UInt64(transactionDTO.maxFee), transactionDTO.minApprovalDelta, transactionDTO.minRemovalDelta, transactionDTO.modifications ? transactionDTO.modifications.map(function (modificationDTO) { return new MultisigCosignatoryModification_1.MultisigCosignatoryModification(modificationDTO.type, PublicAccount_1.PublicAccount.createFromPublicKey(modificationDTO.cosignatoryPublicKey, extractNetworkType(transactionDTO.version))); }) : [], transactionDTO.signature, PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signer, extractNetworkType(transactionDTO.version)), transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.LOCK) {
        var networkType = extractNetworkType(transactionDTO.version);
        return new LockFundsTransaction_1.LockFundsTransaction(networkType, extractTransactionVersion(transactionDTO.version), Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), new UInt64_1.UInt64(transactionDTO.maxFee), new Mosaic_1.Mosaic(new MosaicId_1.MosaicId(transactionDTO.mosaicId), new UInt64_1.UInt64(transactionDTO.amount)), new UInt64_1.UInt64(transactionDTO.duration), new SignedTransaction_1.SignedTransaction('', transactionDTO.hash, '', TransactionType_1.TransactionType.AGGREGATE_BONDED, networkType), transactionDTO.signature, PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signer, networkType), transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.SECRET_LOCK) {
        return new SecretLockTransaction_1.SecretLockTransaction(extractNetworkType(transactionDTO.version), extractTransactionVersion(transactionDTO.version), Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), new UInt64_1.UInt64(transactionDTO.maxFee), new Mosaic_1.Mosaic(new MosaicId_1.MosaicId(transactionDTO.mosaicId), new UInt64_1.UInt64(transactionDTO.amount)), new UInt64_1.UInt64(transactionDTO.duration), transactionDTO.hashAlgorithm, transactionDTO.secret, Address_1.Address.createFromEncoded(transactionDTO.recipient), transactionDTO.signature, PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signer, extractNetworkType(transactionDTO.version)), transactionInfo);
    }
    else if (transactionDTO.type === TransactionType_1.TransactionType.SECRET_PROOF) {
        return new SecretProofTransaction_1.SecretProofTransaction(extractNetworkType(transactionDTO.version), extractTransactionVersion(transactionDTO.version), Deadline_1.Deadline.createFromDTO(transactionDTO.deadline), new UInt64_1.UInt64(transactionDTO.maxFee), transactionDTO.hashAlgorithm, transactionDTO.secret, transactionDTO.proof, transactionDTO.signature, PublicAccount_1.PublicAccount.createFromPublicKey(transactionDTO.signer, extractNetworkType(transactionDTO.version)), transactionInfo);
    }
    throw new Error('Unimplemented transaction with type ' + transactionDTO.type);
};
var extractNetworkType = function (version) {
    var networkType = parseInt(version.toString(16).substr(0, 2), 16);
    if (networkType === NetworkType_1.NetworkType.MAIN_NET) {
        return NetworkType_1.NetworkType.MAIN_NET;
    }
    else if (networkType === NetworkType_1.NetworkType.TEST_NET) {
        return NetworkType_1.NetworkType.TEST_NET;
    }
    else if (networkType === NetworkType_1.NetworkType.PRIVATE) {
        return NetworkType_1.NetworkType.PRIVATE;
    }
    else if (networkType === NetworkType_1.NetworkType.PRIVATE_TEST) {
        return NetworkType_1.NetworkType.PRIVATE_TEST;
    }
    else if (networkType === NetworkType_1.NetworkType.MIJIN) {
        return NetworkType_1.NetworkType.MIJIN;
    }
    else if (networkType === NetworkType_1.NetworkType.MIJIN_TEST) {
        return NetworkType_1.NetworkType.MIJIN_TEST;
    }
    throw new Error('Unimplemented network type');
};
var extractTransactionVersion = function (version) {
    return parseInt(version.toString(16).substr(2, 2), 16);
};
//# sourceMappingURL=CreateTransactionFromDTO.js.map