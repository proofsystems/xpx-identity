import { Transaction } from '../../model/transaction/Transaction';
/**
 * @internal
 * @param transactionDTO
 * @returns {Transaction}
 * @constructor
 */
export declare const CreateTransactionFromDTO: (transactionDTO: any) => Transaction;
