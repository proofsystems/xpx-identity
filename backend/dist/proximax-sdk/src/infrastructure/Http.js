"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var proximax_nem2_library_1 = require("proximax-nem2-library");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
/**
 * Http extended by all http services
 */
var Http = /** @class */ (function () {
    /**
     * Constructor
     * @param url
     * @param networkHttp
     */
    function Http(url, networkHttp) {
        this.apiClient = new proximax_nem2_library_1.ApiClient();
        if (url) {
            this.apiClient.basePath = url;
        }
        if (networkHttp) {
            this.networkHttp = networkHttp;
        }
    }
    Http.prototype.getNetworkTypeObservable = function () {
        var _this = this;
        var networkTypeResolve;
        if (this.networkType == null) {
            networkTypeResolve = this.networkHttp.getNetworkType().pipe(operators_1.map(function (networkType) {
                _this.networkType = networkType;
                return networkType;
            }));
        }
        else {
            networkTypeResolve = rxjs_1.of(this.networkType);
        }
        return networkTypeResolve;
    };
    return Http;
}());
exports.Http = Http;
//# sourceMappingURL=Http.js.map