"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var proximax_nem2_library_1 = require("proximax-nem2-library");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var NetworkType_1 = require("../model/blockchain/NetworkType");
var Http_1 = require("./Http");
/**
 * Network http repository.
 *
 * @since 1.0
 */
var NetworkHttp = /** @class */ (function (_super) {
    __extends(NetworkHttp, _super);
    /**
     * Constructor
     * @param url
     */
    function NetworkHttp(url) {
        var _this = _super.call(this, url) || this;
        _this.networkRoutesApi = new proximax_nem2_library_1.NetworkRoutesApi(_this.apiClient);
        return _this;
    }
    /**
     * Get current network type.
     *
     * @return network type enum.
     */
    NetworkHttp.prototype.getNetworkType = function () {
        return rxjs_1.from(this.networkRoutesApi.getNetworkType()).pipe(operators_1.map(function (networkTypeDTO) {
            if (networkTypeDTO.name === 'mijinTest') {
                return NetworkType_1.NetworkType.MIJIN_TEST;
            }
            else if (networkTypeDTO.name === 'mijin') {
                return NetworkType_1.NetworkType.MIJIN;
            }
            else if (networkTypeDTO.name === 'testnet' || networkTypeDTO.name === 'publicTest') {
                return NetworkType_1.NetworkType.TEST_NET;
            }
            else if (networkTypeDTO.name === 'mainnet' || networkTypeDTO.name === 'public') {
                return NetworkType_1.NetworkType.MAIN_NET;
            }
            else if (networkTypeDTO.name === 'privateTest') {
                return NetworkType_1.NetworkType.PRIVATE_TEST;
            }
            else if (networkTypeDTO.name === 'private') {
                return NetworkType_1.NetworkType.PRIVATE;
            }
            else {
                throw new Error('network ' + networkTypeDTO.name + ' is not supported yet by the sdk');
            }
        }));
    };
    return NetworkHttp;
}(Http_1.Http));
exports.NetworkHttp = NetworkHttp;
//# sourceMappingURL=NetworkHttp.js.map