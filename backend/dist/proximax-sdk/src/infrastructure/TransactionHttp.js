"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var proximax_nem2_library_1 = require("proximax-nem2-library");
var requestPromise = require("request-promise-native");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var PublicAccount_1 = require("../model/account/PublicAccount");
var Deadline_1 = require("../model/transaction/Deadline");
var TransactionAnnounceResponse_1 = require("../model/transaction/TransactionAnnounceResponse");
var TransactionStatus_1 = require("../model/transaction/TransactionStatus");
var TransactionType_1 = require("../model/transaction/TransactionType");
var UInt64_1 = require("../model/UInt64");
var Http_1 = require("./Http");
var CreateTransactionFromDTO_1 = require("./transaction/CreateTransactionFromDTO");
/**
 * Transaction http repository.
 *
 * @since 1.0
 */
var TransactionHttp = /** @class */ (function (_super) {
    __extends(TransactionHttp, _super);
    /**
     * Constructor
     * @param url
     */
    function TransactionHttp(url) {
        var _this = _super.call(this, url) || this;
        _this.url = url;
        _this.transactionRoutesApi = new proximax_nem2_library_1.TransactionRoutesApi(_this.apiClient);
        return _this;
    }
    /**
     * Gets a transaction for a transactionId
     * @param transactionId - Transaction id or hash.
     * @returns Observable<Transaction>
     */
    TransactionHttp.prototype.getTransaction = function (transactionId) {
        return rxjs_1.from(this.transactionRoutesApi.getTransaction(transactionId)).pipe(operators_1.map(function (transactionDTO) {
            return CreateTransactionFromDTO_1.CreateTransactionFromDTO(transactionDTO);
        }));
    };
    /**
     * Gets an array of transactions for different transaction ids
     * @param transactionIds - Array of transactions id and/or hash.
     * @returns Observable<Transaction[]>
     */
    TransactionHttp.prototype.getTransactions = function (transactionIds) {
        var transactionIdsBody = {
            transactionIds: transactionIds,
        };
        return rxjs_1.from(this.transactionRoutesApi.getTransactions(transactionIdsBody)).pipe(operators_1.map(function (transactionsDTO) {
            return transactionsDTO.map(function (transactionDTO) {
                return CreateTransactionFromDTO_1.CreateTransactionFromDTO(transactionDTO);
            });
        }));
    };
    /**
     * Gets a transaction status for a transaction hash
     * @param transactionHash - Transaction hash.
     * @returns Observable<TransactionStatus>
     */
    TransactionHttp.prototype.getTransactionStatus = function (transactionHash) {
        return rxjs_1.from(this.transactionRoutesApi.getTransactionStatus(transactionHash)).pipe(operators_1.map(function (transactionStatusDTO) {
            return new TransactionStatus_1.TransactionStatus(transactionStatusDTO.group, transactionStatusDTO.status, transactionStatusDTO.hash, Deadline_1.Deadline.createFromDTO(transactionStatusDTO.deadline), new UInt64_1.UInt64(transactionStatusDTO.height));
        }));
    };
    /**
     * Gets an array of transaction status for different transaction hashes
     * @param transactionHashes - Array of transaction hash
     * @returns Observable<TransactionStatus[]>
     */
    TransactionHttp.prototype.getTransactionsStatuses = function (transactionHashes) {
        var transactionHashesBody = {
            hashes: transactionHashes,
        };
        return rxjs_1.from(this.transactionRoutesApi.getTransactionsStatuses(transactionHashesBody)).pipe(operators_1.map(function (transactionStatusesDTO) {
            return transactionStatusesDTO.map(function (transactionStatusDTO) {
                return new TransactionStatus_1.TransactionStatus(transactionStatusDTO.group, transactionStatusDTO.status, transactionStatusDTO.hash, Deadline_1.Deadline.createFromDTO(transactionStatusDTO.deadline), new UInt64_1.UInt64(transactionStatusDTO.height));
            });
        }));
    };
    /**
     * Send a signed transaction
     * @param signedTransaction - Signed transaction
     * @returns Observable<TransactionAnnounceResponse>
     */
    TransactionHttp.prototype.announce = function (signedTransaction) {
        return rxjs_1.from(this.transactionRoutesApi.announceTransaction(signedTransaction)).pipe(operators_1.map(function (transactionAnnounceResponseDTO) {
            return new TransactionAnnounceResponse_1.TransactionAnnounceResponse(transactionAnnounceResponseDTO.message);
        }));
    };
    /**
     * Send a signed transaction with missing signatures
     * @param signedTransaction - Signed transaction
     * @returns Observable<TransactionAnnounceResponse>
     */
    TransactionHttp.prototype.announceAggregateBonded = function (signedTransaction) {
        if (signedTransaction.type !== TransactionType_1.TransactionType.AGGREGATE_BONDED) {
            return rxjs_1.from(new Promise(function (resolve, reject) {
                reject('Only Transaction Type 0x4241 is allowed for announce aggregate bonded');
            }));
        }
        return rxjs_1.from(this.transactionRoutesApi.announcePartialTransaction(signedTransaction)).pipe(operators_1.map(function (transactionAnnounceResponseDTO) {
            return new TransactionAnnounceResponse_1.TransactionAnnounceResponse(transactionAnnounceResponseDTO.message);
        }));
    };
    /**
     * Send a cosignature signed transaction of an already announced transaction
     * @param cosignatureSignedTransaction - Cosignature signed transaction
     * @returns Observable<TransactionAnnounceResponse>
     */
    TransactionHttp.prototype.announceAggregateBondedCosignature = function (cosignatureSignedTransaction) {
        return rxjs_1.from(this.transactionRoutesApi.announceCosignatureTransaction(cosignatureSignedTransaction)).pipe(operators_1.map(function (transactionAnnounceResponseDTO) {
            return new TransactionAnnounceResponse_1.TransactionAnnounceResponse(transactionAnnounceResponseDTO.message);
        }));
    };
    TransactionHttp.prototype.announceSync = function (signedTx) {
        var address = PublicAccount_1.PublicAccount.createFromPublicKey(signedTx.signer, signedTx.networkType).address;
        var syncAnnounce = new SyncAnnounce(signedTx.payload, signedTx.hash, address.plain());
        return rxjs_1.from(requestPromise.put({ url: this.url + "/transaction/sync", body: syncAnnounce, json: true })).pipe(operators_1.map(function (response) {
            if (response.status !== undefined) {
                throw new TransactionStatus_1.TransactionStatus('failed', response.status, response.hash, Deadline_1.Deadline.createFromDTO(response.deadline), UInt64_1.UInt64.fromUint(0));
            }
            else {
                return CreateTransactionFromDTO_1.CreateTransactionFromDTO(response);
            }
        }), operators_1.catchError(function (err) {
            if (err.statusCode === 405) {
                return rxjs_1.throwError('non sync server');
            }
            return rxjs_1.throwError(err);
        }));
    };
    return TransactionHttp;
}(Http_1.Http));
exports.TransactionHttp = TransactionHttp;
var SyncAnnounce = /** @class */ (function () {
    function SyncAnnounce(/**
                 * Transaction serialized data
                 */ payload, 
    /**
     * Transaction hash
     */
    hash, 
    /**
     * Transaction address
     */
    address) {
        this.payload = payload;
        this.hash = hash;
        this.address = address;
    }
    return SyncAnnounce;
}());
//# sourceMappingURL=TransactionHttp.js.map