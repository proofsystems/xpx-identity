"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var WebSocket = require("ws");
var PublicAccount_1 = require("../model/account/PublicAccount");
var BlockInfo_1 = require("../model/blockchain/BlockInfo");
var AggregateTransaction_1 = require("../model/transaction/AggregateTransaction");
var CosignatureSignedTransaction_1 = require("../model/transaction/CosignatureSignedTransaction");
var Deadline_1 = require("../model/transaction/Deadline");
var Transaction_1 = require("../model/transaction/Transaction");
var TransactionStatusError_1 = require("../model/transaction/TransactionStatusError");
var TransferTransaction_1 = require("../model/transaction/TransferTransaction");
var UInt64_1 = require("../model/UInt64");
var CreateTransactionFromDTO_1 = require("./transaction/CreateTransactionFromDTO");
var ListenerChannelName;
(function (ListenerChannelName) {
    ListenerChannelName["block"] = "block";
    ListenerChannelName["confirmedAdded"] = "confirmedAdded";
    ListenerChannelName["unconfirmedAdded"] = "unconfirmedAdded";
    ListenerChannelName["unconfirmedRemoved"] = "unconfirmedRemoved";
    ListenerChannelName["aggregateBondedAdded"] = "partialAdded";
    ListenerChannelName["aggregateBondedRemoved"] = "partialRemoved";
    ListenerChannelName["cosignature"] = "cosignature";
    ListenerChannelName["status"] = "status";
})(ListenerChannelName || (ListenerChannelName = {}));
/**
 * Listener service
 */
var Listener = /** @class */ (function () {
    /**
     * Constructor
     * @param config - Listener configuration
     * @param websocketInjected - (Optional) WebSocket injected when using listeners in client
     */
    function Listener(/**
                 * Listener configuration.
                 */ config, 
    /**
     * WebSocket injected when using listeners in client.
     */
    websocketInjected) {
        this.config = config;
        this.websocketInjected = websocketInjected;
        this.config = config.replace(/\/$/, '');
        this.url = this.config + "/ws";
        this.messageSubject = new rxjs_1.Subject();
    }
    /**
     * Open web socket connection.
     * @returns Promise<Void>
     */
    Listener.prototype.open = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.webSocket === undefined || _this.webSocket.readyState === WebSocket.CLOSED) {
                if (_this.websocketInjected) {
                    _this.webSocket = new _this.websocketInjected(_this.url);
                }
                else {
                    _this.webSocket = new WebSocket(_this.url);
                }
                _this.webSocket.onopen = function () {
                    console.log('connection open');
                };
                _this.webSocket.onerror = function (err) {
                    console.log('WebSocket Error ');
                    console.log(err);
                };
                _this.webSocket.onmessage = function (msg) {
                    var message = JSON.parse(msg.data);
                    if (message.uid) {
                        _this.uid = message.uid;
                        resolve();
                    }
                    else if (message.transaction) {
                        _this.messageSubject.next({ channelName: message.meta.channelName, message: CreateTransactionFromDTO_1.CreateTransactionFromDTO(message) });
                    }
                    else if (message.block) {
                        var networkType = parseInt(message.block.version.toString(16).substr(0, 2), 16);
                        _this.messageSubject.next({
                            channelName: ListenerChannelName.block, message: new BlockInfo_1.BlockInfo(message.meta.hash, message.meta.generationHash, message.meta.totalFee ? new UInt64_1.UInt64(message.meta.totalFee) : new UInt64_1.UInt64([0, 0]), message.meta.numTransactions, message.block.signature, PublicAccount_1.PublicAccount.createFromPublicKey(message.block.signer, networkType), networkType, parseInt(message.block.version.toString(16).substr(2, 2), 16), // Tx version
                            message.block.type, new UInt64_1.UInt64(message.block.height), new UInt64_1.UInt64(message.block.timestamp), new UInt64_1.UInt64(message.block.difficulty), message.block.previousBlockHash, message.block.blockTransactionsHash),
                        });
                    }
                    else if (message.status) {
                        _this.messageSubject.next({
                            channelName: ListenerChannelName.status, message: new TransactionStatusError_1.TransactionStatusError(message.hash, message.status, Deadline_1.Deadline.createFromDTO(message.deadline)),
                        });
                    }
                    else if (message.meta) {
                        _this.messageSubject.next({ channelName: message.meta.channelName, message: message.meta.hash });
                    }
                    else if (message.parentHash) {
                        _this.messageSubject.next({
                            channelName: ListenerChannelName.cosignature,
                            message: new CosignatureSignedTransaction_1.CosignatureSignedTransaction(message.parentHash, message.signature, message.signer),
                        });
                    }
                };
            }
            else {
                resolve();
            }
        });
    };
    /**
     * Close web socket connection.
     * @returns void
     */
    Listener.prototype.close = function () {
        if (this.webSocket && (this.webSocket.readyState === WebSocket.OPEN || this.webSocket.readyState === WebSocket.CONNECTING)) {
            this.webSocket.close();
        }
    };
    /**
     * Terminate web socket connection.
     * @returns void
     */
    Listener.prototype.terminate = function () {
        if (this.webSocket) {
            this.webSocket.terminate();
        }
    };
    /**
     * Returns an observable stream of BlockInfo.
     * Each time a new Block is added into the blockchain,
     * it emits a new BlockInfo in the event stream.
     *
     * @return an observable stream of BlockInfo
     */
    Listener.prototype.newBlock = function () {
        this.subscribeTo('block');
        return this.messageSubject
            .asObservable().pipe(operators_1.share(), operators_1.filter(function (_) { return _.channelName === ListenerChannelName.block; }), operators_1.filter(function (_) { return _.message instanceof BlockInfo_1.BlockInfo; }), operators_1.map(function (_) { return _.message; }));
    };
    /**
     * Returns an observable stream of Transaction for a specific address.
     * Each time a transaction is in confirmed state an it involves the address,
     * it emits a new Transaction in the event stream.
     *
     * @param address address we listen when a transaction is in confirmed state
     * @return an observable stream of Transaction with state confirmed
     */
    Listener.prototype.confirmed = function (address) {
        var _this = this;
        this.subscribeTo("confirmedAdded/" + address.plain());
        return this.messageSubject.asObservable().pipe(operators_1.filter(function (_) { return _.channelName === ListenerChannelName.confirmedAdded; }), operators_1.filter(function (_) { return _.message instanceof Transaction_1.Transaction; }), operators_1.map(function (_) { return _.message; }), operators_1.filter(function (_) { return _this.transactionFromAddress(_, address); }));
    };
    /**
     * Returns an observable stream of Transaction for a specific address.
     * Each time a transaction is in unconfirmed state an it involves the address,
     * it emits a new Transaction in the event stream.
     *
     * @param address address we listen when a transaction is in unconfirmed state
     * @return an observable stream of Transaction with state unconfirmed
     */
    Listener.prototype.unconfirmedAdded = function (address) {
        var _this = this;
        this.subscribeTo("unconfirmedAdded/" + address.plain());
        return this.messageSubject.asObservable().pipe(operators_1.filter(function (_) { return _.channelName === ListenerChannelName.unconfirmedAdded; }), operators_1.filter(function (_) { return _.message instanceof Transaction_1.Transaction; }), operators_1.map(function (_) { return _.message; }), operators_1.filter(function (_) { return _this.transactionFromAddress(_, address); }));
    };
    /**
     * Returns an observable stream of Transaction Hashes for specific address.
     * Each time a transaction with state unconfirmed changes its state,
     * it emits a new message with the transaction hash in the event stream.
     *
     * @param address address we listen when a transaction is removed from unconfirmed state
     * @return an observable stream of Strings with the transaction hash
     */
    Listener.prototype.unconfirmedRemoved = function (address) {
        this.subscribeTo("unconfirmedRemoved/" + address.plain());
        return this.messageSubject.asObservable().pipe(operators_1.filter(function (_) { return _.channelName === ListenerChannelName.unconfirmedRemoved; }), operators_1.filter(function (_) { return typeof _.message === 'string'; }), operators_1.map(function (_) { return _.message; }));
    };
    /**
     * Return an observable of {@link AggregateTransaction} for specific address.
     * Each time an aggregate bonded transaction is announced,
     * it emits a new {@link AggregateTransaction} in the event stream.
     *
     * @param address address we listen when a transaction with missing signatures state
     * @return an observable stream of AggregateTransaction with missing signatures state
     */
    Listener.prototype.aggregateBondedAdded = function (address) {
        var _this = this;
        this.subscribeTo("partialAdded/" + address.plain());
        return this.messageSubject.asObservable().pipe(operators_1.filter(function (_) { return _.channelName === ListenerChannelName.aggregateBondedAdded; }), operators_1.filter(function (_) { return _.message instanceof AggregateTransaction_1.AggregateTransaction; }), operators_1.map(function (_) { return _.message; }), operators_1.filter(function (_) { return _this.transactionFromAddress(_, address); }));
    };
    /**
     * Returns an observable stream of Transaction Hashes for specific address.
     * Each time an aggregate bonded transaction is announced,
     * it emits a new message with the transaction hash in the event stream.
     *
     * @param address address we listen when a transaction is confirmed or rejected
     * @return an observable stream of Strings with the transaction hash
     */
    Listener.prototype.aggregateBondedRemoved = function (address) {
        this.subscribeTo("partialRemoved/" + address.plain());
        return this.messageSubject.asObservable().pipe(operators_1.filter(function (_) { return _.channelName === ListenerChannelName.aggregateBondedRemoved; }), operators_1.filter(function (_) { return typeof _.message === 'string'; }), operators_1.map(function (_) { return _.message; }));
    };
    /**
     * Returns an observable stream of {@link TransactionStatusError} for specific address.
     * Each time a transaction contains an error,
     * it emits a new message with the transaction status error in the event stream.
     *
     * @param address address we listen to be notified when some error happened
     * @return an observable stream of {@link TransactionStatusError}
     */
    Listener.prototype.status = function (address) {
        this.subscribeTo("status/" + address.plain());
        return this.messageSubject.asObservable().pipe(operators_1.filter(function (_) { return _.channelName === ListenerChannelName.status; }), operators_1.filter(function (_) { return _.message instanceof TransactionStatusError_1.TransactionStatusError; }), operators_1.map(function (_) { return _.message; }));
    };
    /**
     * Returns an observable stream of {@link CosignatureSignedTransaction} for specific address.
     * Each time a cosigner signs a transaction the address initialized,
     * it emits a new message with the cosignatory signed transaction in the even stream.
     *
     * @param address address we listen when a cosignatory is added to some transaction address sent
     * @return an observable stream of {@link CosignatureSignedTransaction}
     */
    Listener.prototype.cosignatureAdded = function (address) {
        this.subscribeTo("cosignature/" + address.plain());
        return this.messageSubject.asObservable().pipe(operators_1.filter(function (_) { return _.channelName === ListenerChannelName.cosignature; }), operators_1.filter(function (_) { return _.message instanceof CosignatureSignedTransaction_1.CosignatureSignedTransaction; }), operators_1.map(function (_) { return _.message; }));
    };
    /**
     * @internal
     * Subscribes to a channelName.
     * @param channel - Channel subscribed to.
     */
    Listener.prototype.subscribeTo = function (channel) {
        var subscriptionMessage = {
            uid: this.uid,
            subscribe: channel,
        };
        this.webSocket.send(JSON.stringify(subscriptionMessage));
    };
    /**
     * @internal
     * Filters if a transaction has been initiated or signed by an address
     * @param transaction - Transaction object
     * @param address - Address
     * @returns boolean
     */
    Listener.prototype.transactionFromAddress = function (transaction, address) {
        var _this = this;
        var transactionFromAddress = this.transactionHasSignerOrReceptor(transaction, address);
        if (transaction instanceof AggregateTransaction_1.AggregateTransaction) {
            transaction.cosignatures.map(function (_) {
                if (_.signer.address.equals(address)) {
                    transactionFromAddress = true;
                }
            });
            transaction.innerTransactions.map(function (innerTransaction) {
                if (_this.transactionHasSignerOrReceptor(innerTransaction, address)) {
                    transactionFromAddress = true;
                }
            });
        }
        return transactionFromAddress;
    };
    /**
     * @internal
     * @param transaction
     * @param address
     * @returns {boolean}
     */
    Listener.prototype.transactionHasSignerOrReceptor = function (transaction, address) {
        return transaction.signer.address.equals(address) ||
            (transaction instanceof TransferTransaction_1.TransferTransaction && transaction.recipient.equals(address));
    };
    return Listener;
}());
exports.Listener = Listener;
//# sourceMappingURL=Listener.js.map