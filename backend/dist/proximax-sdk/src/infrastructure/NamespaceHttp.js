"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var proximax_nem2_library_1 = require("proximax-nem2-library");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var PublicAccount_1 = require("../model/account/PublicAccount");
var NamespaceId_1 = require("../model/namespace/NamespaceId");
var NamespaceInfo_1 = require("../model/namespace/NamespaceInfo");
var NamespaceName_1 = require("../model/namespace/NamespaceName");
var UInt64_1 = require("../model/UInt64");
var Http_1 = require("./Http");
var NetworkHttp_1 = require("./NetworkHttp");
/**
 * Namespace http repository.
 *
 * @since 1.0
 */
var NamespaceHttp = /** @class */ (function (_super) {
    __extends(NamespaceHttp, _super);
    /**
     * Constructor
     * @param url
     * @param networkHttp
     */
    function NamespaceHttp(url, networkHttp) {
        var _this = this;
        networkHttp = networkHttp == null ? new NetworkHttp_1.NetworkHttp(url) : networkHttp;
        _this = _super.call(this, url, networkHttp) || this;
        _this.namespaceRoutesApi = new proximax_nem2_library_1.NamespaceRoutesApi(_this.apiClient);
        return _this;
    }
    /**
     * Gets the NamespaceInfo for a given namespaceId
     * @param namespaceId - Namespace id
     * @returns Observable<NamespaceInfo>
     */
    NamespaceHttp.prototype.getNamespace = function (namespaceId) {
        var _this = this;
        return this.getNetworkTypeObservable().pipe(operators_1.mergeMap(function (networkType) { return rxjs_1.from(_this.namespaceRoutesApi.getNamespace(namespaceId.toHex())).pipe(operators_1.map(function (namespaceInfoDTO) {
            return new NamespaceInfo_1.NamespaceInfo(namespaceInfoDTO.meta.active, namespaceInfoDTO.meta.index, namespaceInfoDTO.meta.id, namespaceInfoDTO.namespace.type, namespaceInfoDTO.namespace.depth, _this.extractLevels(namespaceInfoDTO.namespace), new NamespaceId_1.NamespaceId(namespaceInfoDTO.namespace.parentId), PublicAccount_1.PublicAccount.createFromPublicKey(namespaceInfoDTO.namespace.owner, networkType), new UInt64_1.UInt64(namespaceInfoDTO.namespace.startHeight), new UInt64_1.UInt64(namespaceInfoDTO.namespace.endHeight));
        })); }));
    };
    /**
     * Gets array of NamespaceInfo for an account
     * @param address - Address
     * @param queryParams - (Optional) Query params
     * @returns Observable<NamespaceInfo[]>
     */
    NamespaceHttp.prototype.getNamespacesFromAccount = function (address, queryParams) {
        var _this = this;
        return this.getNetworkTypeObservable().pipe(operators_1.mergeMap(function (networkType) { return rxjs_1.from(_this.namespaceRoutesApi.getNamespacesFromAccount(address.plain(), queryParams != null ? queryParams : {})).pipe(operators_1.map(function (namespaceInfosDTO) {
            return namespaceInfosDTO.map(function (namespaceInfoDTO) {
                return new NamespaceInfo_1.NamespaceInfo(namespaceInfoDTO.meta.active, namespaceInfoDTO.meta.index, namespaceInfoDTO.meta.id, namespaceInfoDTO.namespace.type, namespaceInfoDTO.namespace.depth, _this.extractLevels(namespaceInfoDTO.namespace), new NamespaceId_1.NamespaceId(namespaceInfoDTO.namespace.parentId), PublicAccount_1.PublicAccount.createFromPublicKey(namespaceInfoDTO.namespace.owner, networkType), new UInt64_1.UInt64(namespaceInfoDTO.namespace.startHeight), new UInt64_1.UInt64(namespaceInfoDTO.namespace.endHeight));
            });
        })); }));
    };
    /**
     * Gets array of NamespaceInfo for different account
     * @param addresses - Array of Address
     * @param queryParams - (Optional) Query params
     * @returns Observable<NamespaceInfo[]>
     */
    NamespaceHttp.prototype.getNamespacesFromAccounts = function (addresses, queryParams) {
        var _this = this;
        var publicKeysBody = {
            addresses: addresses.map(function (address) { return address.plain(); }),
        };
        return this.getNetworkTypeObservable().pipe(operators_1.mergeMap(function (networkType) { return rxjs_1.from(_this.namespaceRoutesApi.getNamespacesFromAccounts(publicKeysBody, queryParams != null ? queryParams : {})).pipe(operators_1.map(function (namespaceInfosDTO) {
            return namespaceInfosDTO.map(function (namespaceInfoDTO) {
                return new NamespaceInfo_1.NamespaceInfo(namespaceInfoDTO.meta.active, namespaceInfoDTO.meta.index, namespaceInfoDTO.meta.id, namespaceInfoDTO.namespace.type, namespaceInfoDTO.namespace.depth, _this.extractLevels(namespaceInfoDTO.namespace), new NamespaceId_1.NamespaceId(namespaceInfoDTO.namespace.parentId), PublicAccount_1.PublicAccount.createFromPublicKey(namespaceInfoDTO.namespace.owner, networkType), new UInt64_1.UInt64(namespaceInfoDTO.namespace.startHeight), new UInt64_1.UInt64(namespaceInfoDTO.namespace.endHeight));
            });
        })); }));
    };
    /**
     * Gets array of NamespaceName for different namespaceIds
     * @param namespaceIds - Array of namespace ids
     * @returns Observable<NamespaceName[]>
     */
    NamespaceHttp.prototype.getNamespacesName = function (namespaceIds) {
        var namespaceIdsBody = {
            namespaceIds: namespaceIds.map(function (id) { return id.toHex(); }),
        };
        return rxjs_1.from(this.namespaceRoutesApi.getNamespacesNames(namespaceIdsBody)).pipe(operators_1.map(function (namespaceNamesDTO) {
            return namespaceNamesDTO.map(function (namespaceNameDTO) {
                return new NamespaceName_1.NamespaceName(new NamespaceId_1.NamespaceId(namespaceNameDTO.namespaceId), namespaceNameDTO.name, namespaceNameDTO.parentId ? new NamespaceId_1.NamespaceId(namespaceNameDTO.parentId) : undefined);
            });
        }));
    };
    NamespaceHttp.prototype.extractLevels = function (namespace) {
        var result = [];
        if (namespace.level0) {
            result.push(new NamespaceId_1.NamespaceId(namespace.level0));
        }
        if (namespace.level1) {
            result.push(new NamespaceId_1.NamespaceId(namespace.level1));
        }
        if (namespace.level2) {
            result.push(new NamespaceId_1.NamespaceId(namespace.level2));
        }
        return result;
    };
    return NamespaceHttp;
}(Http_1.Http));
exports.NamespaceHttp = NamespaceHttp;
//# sourceMappingURL=NamespaceHttp.js.map