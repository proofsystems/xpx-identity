"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var proximax_nem2_library_1 = require("proximax-nem2-library");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var PublicAccount_1 = require("../model/account/PublicAccount");
var BlockchainScore_1 = require("../model/blockchain/BlockchainScore");
var BlockchainStorageInfo_1 = require("../model/blockchain/BlockchainStorageInfo");
var BlockInfo_1 = require("../model/blockchain/BlockInfo");
var UInt64_1 = require("../model/UInt64");
var Http_1 = require("./Http");
var CreateTransactionFromDTO_1 = require("./transaction/CreateTransactionFromDTO");
/**
 * Blockchain http repository.
 *
 * @since 1.0
 */
var BlockchainHttp = /** @class */ (function (_super) {
    __extends(BlockchainHttp, _super);
    /**
     * Constructor
     * @param url
     */
    function BlockchainHttp(url) {
        var _this = _super.call(this, url) || this;
        _this.blockchainRoutesApi = new proximax_nem2_library_1.BlockchainRoutesApi(_this.apiClient);
        return _this;
    }
    /**
     * Gets a BlockInfo for a given block height
     * @param height - Block height
     * @returns Observable<BlockInfo>
     */
    BlockchainHttp.prototype.getBlockByHeight = function (height) {
        return rxjs_1.from(this.blockchainRoutesApi.getBlockByHeight(height)).pipe(operators_1.map(function (blockDTO) {
            var networkType = parseInt(blockDTO.block.version.toString(16).substr(0, 2), 16);
            return new BlockInfo_1.BlockInfo(blockDTO.meta.hash, blockDTO.meta.generationHash, new UInt64_1.UInt64(blockDTO.meta.totalFee), blockDTO.meta.numTransactions, blockDTO.block.signature, PublicAccount_1.PublicAccount.createFromPublicKey(blockDTO.block.signer, networkType), networkType, parseInt(blockDTO.block.version.toString(16).substr(2, 2), 16), // Tx version
            blockDTO.block.type, new UInt64_1.UInt64(blockDTO.block.height), new UInt64_1.UInt64(blockDTO.block.timestamp), new UInt64_1.UInt64(blockDTO.block.difficulty), blockDTO.block.previousBlockHash, blockDTO.block.blockTransactionsHash);
        }));
    };
    /**
     * Gets array of transactions included in a block for a block height
     * @param height - Block height
     * @param queryParams - (Optional) Query params
     * @returns Observable<Transaction[]>
     */
    BlockchainHttp.prototype.getBlockTransactions = function (height, queryParams) {
        return rxjs_1.from(this.blockchainRoutesApi.getBlockTransactions(height, queryParams != null ? queryParams : {})).pipe(operators_1.map(function (transactionsDTO) {
            return transactionsDTO.map(function (transactionDTO) {
                return CreateTransactionFromDTO_1.CreateTransactionFromDTO(transactionDTO);
            });
        }));
    };
    /**
     * Gets array of BlockInfo for a block height with limit
     * @param height - Block height from which will be the first block in the array
     * @param limit - Number of blocks returned
     * @returns Observable<BlockInfo[]>
     */
    BlockchainHttp.prototype.getBlocksByHeightWithLimit = function (height, limit) {
        if (limit === void 0) { limit = 1; }
        return rxjs_1.from(this.blockchainRoutesApi.getBlocksByHeightWithLimit(height, limit)).pipe(operators_1.map(function (blocksDTO) {
            return blocksDTO.map(function (blockDTO) {
                var networkType = parseInt(blockDTO.block.version.toString(16).substr(0, 2), 16);
                return new BlockInfo_1.BlockInfo(blockDTO.meta.hash, blockDTO.meta.generationHash, new UInt64_1.UInt64(blockDTO.meta.totalFee), blockDTO.meta.numTransactions, blockDTO.block.signature, PublicAccount_1.PublicAccount.createFromPublicKey(blockDTO.block.signer, networkType), networkType, parseInt(blockDTO.block.version.toString(16).substr(2, 2), 16), // Tx version
                blockDTO.block.type, new UInt64_1.UInt64(blockDTO.block.height), new UInt64_1.UInt64(blockDTO.block.timestamp), new UInt64_1.UInt64(blockDTO.block.difficulty), blockDTO.block.previousBlockHash, blockDTO.block.blockTransactionsHash);
            });
        }));
    };
    /**
     * Gets current blockchain height
     * @returns Observable<UInt64>
     */
    BlockchainHttp.prototype.getBlockchainHeight = function () {
        return rxjs_1.from(this.blockchainRoutesApi.getBlockchainHeight()).pipe(operators_1.map(function (heightDTO) {
            return new UInt64_1.UInt64(heightDTO.height);
        }));
    };
    /**
     * Gets current blockchain score
     * @returns Observable<BlockchainScore>
     */
    BlockchainHttp.prototype.getBlockchainScore = function () {
        return rxjs_1.from(this.blockchainRoutesApi.getBlockchainScore()).pipe(operators_1.map(function (blockchainScoreDTO) {
            return new BlockchainScore_1.BlockchainScore(new UInt64_1.UInt64(blockchainScoreDTO.scoreLow), new UInt64_1.UInt64(blockchainScoreDTO.scoreHigh));
        }));
    };
    /**
     * Gets blockchain storage info.
     * @returns Observable<BlockchainStorageInfo>
     */
    BlockchainHttp.prototype.getDiagnosticStorage = function () {
        return rxjs_1.from(this.blockchainRoutesApi.getDiagnosticStorage()).pipe(operators_1.map(function (blockchainStorageInfoDTO) {
            return new BlockchainStorageInfo_1.BlockchainStorageInfo(blockchainStorageInfoDTO.numBlocks, blockchainStorageInfoDTO.numTransactions, blockchainStorageInfoDTO.numAccounts);
        }));
    };
    return BlockchainHttp;
}(Http_1.Http));
exports.BlockchainHttp = BlockchainHttp;
//# sourceMappingURL=BlockchainHttp.js.map