"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var proximax_nem2_library_1 = require("proximax-nem2-library");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var AccountInfo_1 = require("../model/account/AccountInfo");
var Address_1 = require("../model/account/Address");
var MultisigAccountGraphInfo_1 = require("../model/account/MultisigAccountGraphInfo");
var MultisigAccountInfo_1 = require("../model/account/MultisigAccountInfo");
var PublicAccount_1 = require("../model/account/PublicAccount");
var Mosaic_1 = require("../model/mosaic/Mosaic");
var MosaicId_1 = require("../model/mosaic/MosaicId");
var UInt64_1 = require("../model/UInt64");
var Http_1 = require("./Http");
var NetworkHttp_1 = require("./NetworkHttp");
var CreateTransactionFromDTO_1 = require("./transaction/CreateTransactionFromDTO");
/**
 * Account http repository.
 *
 * @since 1.0
 */
var AccountHttp = /** @class */ (function (_super) {
    __extends(AccountHttp, _super);
    /**
     * Constructor
     * @param url
     * @param networkHttp
     */
    function AccountHttp(url, networkHttp) {
        var _this = this;
        networkHttp = networkHttp == null ? new NetworkHttp_1.NetworkHttp(url) : networkHttp;
        _this = _super.call(this, url, networkHttp) || this;
        _this.accountRoutesApi = new proximax_nem2_library_1.AccountRoutesApi(_this.apiClient);
        return _this;
    }
    /**
     * Gets an AccountInfo for an account.
     * @param address Address
     * @returns Observable<AccountInfo>
     */
    AccountHttp.prototype.getAccountInfo = function (address) {
        return rxjs_1.from(this.accountRoutesApi.getAccountInfo(address.plain())).pipe(operators_1.map(function (accountInfoDTO) {
            return new AccountInfo_1.AccountInfo(accountInfoDTO.meta, Address_1.Address.createFromEncoded(accountInfoDTO.account.address), new UInt64_1.UInt64(accountInfoDTO.account.addressHeight), accountInfoDTO.account.publicKey, new UInt64_1.UInt64(accountInfoDTO.account.publicKeyHeight), accountInfoDTO.account.mosaics.map(function (mosaicDTO) { return new Mosaic_1.Mosaic(new MosaicId_1.MosaicId(mosaicDTO.id), new UInt64_1.UInt64(mosaicDTO.amount)); }));
        }));
    };
    /**
     * Gets AccountsInfo for different accounts.
     * @param addresses List of Address
     * @returns Observable<AccountInfo[]>
     */
    AccountHttp.prototype.getAccountsInfo = function (addresses) {
        var accountIdsBody = {
            addresses: addresses.map(function (address) { return address.plain(); }),
        };
        return rxjs_1.from(this.accountRoutesApi.getAccountsInfo(accountIdsBody)).pipe(operators_1.map(function (accountsInfoMetaDataDTO) {
            return accountsInfoMetaDataDTO.map(function (accountInfoDTO) {
                return new AccountInfo_1.AccountInfo(accountInfoDTO.meta, Address_1.Address.createFromEncoded(accountInfoDTO.account.address), new UInt64_1.UInt64(accountInfoDTO.account.addressHeight), accountInfoDTO.account.publicKey, new UInt64_1.UInt64(accountInfoDTO.account.publicKeyHeight), accountInfoDTO.account.mosaics.map(function (mosaicDTO) { return new Mosaic_1.Mosaic(mosaicDTO.id, mosaicDTO.amount); }));
            });
        }));
    };
    /**
     * Gets a MultisigAccountInfo for an account.
     * @param address - User address
     * @returns Observable<MultisigAccountInfo>
     */
    AccountHttp.prototype.getMultisigAccountInfo = function (address) {
        var _this = this;
        return this.getNetworkTypeObservable().pipe(operators_1.mergeMap(function (networkType) { return rxjs_1.from(_this.accountRoutesApi.getAccountMultisig(address.plain())).pipe(operators_1.map(function (multisigAccountInfoDTO) {
            return new MultisigAccountInfo_1.MultisigAccountInfo(PublicAccount_1.PublicAccount.createFromPublicKey(multisigAccountInfoDTO.multisig.account, networkType), multisigAccountInfoDTO.multisig.minApproval, multisigAccountInfoDTO.multisig.minRemoval, multisigAccountInfoDTO.multisig.cosignatories
                .map(function (cosigner) { return PublicAccount_1.PublicAccount.createFromPublicKey(cosigner, networkType); }), multisigAccountInfoDTO.multisig.multisigAccounts
                .map(function (multisigAccount) { return PublicAccount_1.PublicAccount.createFromPublicKey(multisigAccount, networkType); }));
        })); }));
    };
    /**
     * Gets a MultisigAccountGraphInfo for an account.
     * @param address - User address
     * @returns Observable<MultisigAccountGraphInfo>
     */
    AccountHttp.prototype.getMultisigAccountGraphInfo = function (address) {
        var _this = this;
        return this.getNetworkTypeObservable().pipe(operators_1.mergeMap(function (networkType) { return rxjs_1.from(_this.accountRoutesApi.getAccountMultisigGraph(address.plain())).pipe(operators_1.map(function (multisigAccountGraphInfosDTO) {
            var multisigAccounts = new Map();
            multisigAccountGraphInfosDTO.map(function (multisigAccountGraphInfoDTO) {
                multisigAccounts.set(multisigAccountGraphInfoDTO.level, multisigAccountGraphInfoDTO.multisigEntries.map(function (multisigAccountInfoDTO) {
                    return new MultisigAccountInfo_1.MultisigAccountInfo(PublicAccount_1.PublicAccount.createFromPublicKey(multisigAccountInfoDTO.multisig.account, networkType), multisigAccountInfoDTO.multisig.minApproval, multisigAccountInfoDTO.multisig.minRemoval, multisigAccountInfoDTO.multisig.cosignatories
                        .map(function (cosigner) { return PublicAccount_1.PublicAccount.createFromPublicKey(cosigner, networkType); }), multisigAccountInfoDTO.multisig.multisigAccounts
                        .map(function (multisigAccountDTO) { return PublicAccount_1.PublicAccount.createFromPublicKey(multisigAccountDTO, networkType); }));
                }));
            });
            return new MultisigAccountGraphInfo_1.MultisigAccountGraphInfo(multisigAccounts);
        })); }));
    };
    /**
     * Gets an array of confirmed transactions for which an account is signer or receiver.
     * @param publicAccount - User public account
     * @param queryParams - (Optional) Query params
     * @returns Observable<Transaction[]>
     */
    AccountHttp.prototype.transactions = function (publicAccount, queryParams) {
        return rxjs_1.from(this.accountRoutesApi.transactions(publicAccount.publicKey, queryParams != null ? queryParams : {})).pipe(operators_1.map(function (transactionsDTO) {
            return transactionsDTO.map(function (transactionDTO) {
                return CreateTransactionFromDTO_1.CreateTransactionFromDTO(transactionDTO);
            });
        }));
    };
    /**
     * Gets an array of transactions for which an account is the recipient of a transaction.
     * A transaction is said to be incoming with respect to an account if the account is the recipient of a transaction.
     * @param publicAccount - User public account
     * @param queryParams - (Optional) Query params
     * @returns Observable<Transaction[]>
     */
    AccountHttp.prototype.incomingTransactions = function (publicAccount, queryParams) {
        return rxjs_1.from(this.accountRoutesApi.incomingTransactions(publicAccount.publicKey, queryParams != null ? queryParams : {})).pipe(operators_1.map(function (transactionsDTO) {
            return transactionsDTO.map(function (transactionDTO) {
                return CreateTransactionFromDTO_1.CreateTransactionFromDTO(transactionDTO);
            });
        }));
    };
    /**
     * Gets an array of transactions for which an account is the sender a transaction.
     * A transaction is said to be outgoing with respect to an account if the account is the sender of a transaction.
     * @param publicAccount - User public account
     * @param queryParams - (Optional) Query params
     * @returns Observable<Transaction[]>
     */
    AccountHttp.prototype.outgoingTransactions = function (publicAccount, queryParams) {
        return rxjs_1.from(this.accountRoutesApi.outgoingTransactions(publicAccount.publicKey, queryParams != null ? queryParams : {})).pipe(operators_1.map(function (transactionsDTO) {
            return transactionsDTO.map(function (transactionDTO) {
                return CreateTransactionFromDTO_1.CreateTransactionFromDTO(transactionDTO);
            });
        }));
    };
    /**
     * Gets the array of transactions for which an account is the sender or receiver and which have not yet been included in a block.
     * Unconfirmed transactions are those transactions that have not yet been included in a block.
     * Unconfirmed transactions are not guaranteed to be included in any block.
     * @param publicAccount - User public account
     * @param queryParams - (Optional) Query params
     * @returns Observable<Transaction[]>
     */
    AccountHttp.prototype.unconfirmedTransactions = function (publicAccount, queryParams) {
        return rxjs_1.from(this.accountRoutesApi.unconfirmedTransactions(publicAccount.publicKey, queryParams != null ? queryParams : {})).pipe(operators_1.map(function (transactionsDTO) {
            return transactionsDTO.map(function (transactionDTO) {
                return CreateTransactionFromDTO_1.CreateTransactionFromDTO(transactionDTO);
            });
        }));
    };
    /**
     * Gets an array of transactions for which an account is the sender or has sign the transaction.
     * A transaction is said to be aggregate bonded with respect to an account if there are missing signatures.
     * @param publicAccount - User public account
     * @param queryParams - (Optional) Query params
     * @returns Observable<AggregateTransaction[]>
     */
    AccountHttp.prototype.aggregateBondedTransactions = function (publicAccount, queryParams) {
        return rxjs_1.from(this.accountRoutesApi.partialTransactions(publicAccount.publicKey, queryParams != null ? queryParams : {})).pipe(operators_1.map(function (transactionsDTO) {
            return transactionsDTO.map(function (transactionDTO) {
                return CreateTransactionFromDTO_1.CreateTransactionFromDTO(transactionDTO);
            });
        }));
    };
    return AccountHttp;
}(Http_1.Http));
exports.AccountHttp = AccountHttp;
//# sourceMappingURL=AccountHttp.js.map