import { MosaicInfo } from '../model/mosaic/MosaicInfo';
/**
 * Class representing mosaic view information
 */
export declare class MosaicView {
    readonly mosaicInfo: MosaicInfo;
    /**
     * The parent namespace name
     */
    readonly namespaceName: string;
    /**
     * The mosaic name
     */
    readonly mosaicName: string;
    /**
     * @internal
     * @param mosaicInfo
     * @param namespaceName
     * @param mosaicName
     */
    constructor(/**
                 * The mosaic information
                 */ mosaicInfo: MosaicInfo, 
    /**
     * The parent namespace name
     */
    namespaceName: string, 
    /**
     * The mosaic name
     */
    mosaicName: string);
    /**
     * Namespace and mosaic description
     * @returns {string}
     */
    fullName(): string;
}
