"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var MosaicAmountView_1 = require("./MosaicAmountView");
var MosaicView_1 = require("./MosaicView");
/**
 * Mosaic service
 */
var MosaicService = /** @class */ (function () {
    /**
     * Constructor
     * @param accountHttp
     * @param mosaicHttp
     * @param namespaceHttp
     */
    function MosaicService(accountHttp, mosaicHttp, namespaceHttp) {
        this.accountHttp = accountHttp;
        this.mosaicHttp = mosaicHttp;
        this.namespaceHttp = namespaceHttp;
    }
    /**
     * Get mosaic view given mosaicIds
     * @param mosaicIds - The ids of the mosaics
     * @returns {Observable<MosaicView[]>}
     */
    MosaicService.prototype.mosaicsView = function (mosaicIds) {
        var _this = this;
        return rxjs_1.of(mosaicIds).pipe(operators_1.mergeMap(function (_) { return _this.mosaicHttp.getMosaics(mosaicIds); }), operators_1.mergeMap(function (_) { return _; }), operators_1.mergeMap(function (mosaicInfo) { return _this.mosaicHttp.getMosaicsName([mosaicInfo.mosaicId]).pipe(operators_1.map(function (mosaicsName) {
            return { mosaicInfo: mosaicInfo, mosaicName: mosaicsName[0].name };
        })); }), operators_1.mergeMap(function (_) { return _this.namespaceHttp.getNamespacesName([_.mosaicInfo.namespaceId]).pipe(operators_1.map(function (namespacesName) {
            return new MosaicView_1.MosaicView(_.mosaicInfo, namespacesName[0].name, _.mosaicName);
        })); }), operators_1.toArray());
    };
    /**
     * Get mosaic amount view given mosaic array
     * @param mosaics
     * @returns {Observable<MosaicAmountView[]>}
     */
    MosaicService.prototype.mosaicsAmountView = function (mosaics) {
        var _this = this;
        return rxjs_1.of(mosaics).pipe(operators_1.mergeMap(function (_) { return _; }), operators_1.mergeMap(function (mosaic) { return _this.mosaicsView([mosaic.id]).pipe(operators_1.filter(function (_) { return _.length !== 0; }), operators_1.map(function (mosaicViews) {
            return new MosaicAmountView_1.MosaicAmountView(mosaicViews[0].mosaicInfo, mosaicViews[0].namespaceName, mosaicViews[0].mosaicName, mosaic.amount);
        }), operators_1.toArray()); }));
    };
    /**
     * Get balance mosaics in form of MosaicAmountViews for a given account address
     * @param address - Account address
     * @returns {Observable<MosaicAmountView[]>}
     */
    MosaicService.prototype.mosaicsAmountViewFromAddress = function (address) {
        var _this = this;
        return rxjs_1.of(address).pipe(operators_1.mergeMap(function (_) { return _this.accountHttp.getAccountInfo(_); }), operators_1.mergeMap(function (_) { return _this.mosaicsAmountView(_.mosaics); }));
    };
    return MosaicService;
}());
exports.MosaicService = MosaicService;
//# sourceMappingURL=MosaicService.js.map