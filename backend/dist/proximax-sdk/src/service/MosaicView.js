"use strict";
/*
 * Copyright 2018 NEM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Class representing mosaic view information
 */
var MosaicView = /** @class */ (function () {
    /**
     * @internal
     * @param mosaicInfo
     * @param namespaceName
     * @param mosaicName
     */
    function MosaicView(/**
                 * The mosaic information
                 */ mosaicInfo, 
    /**
     * The parent namespace name
     */
    namespaceName, 
    /**
     * The mosaic name
     */
    mosaicName) {
        this.mosaicInfo = mosaicInfo;
        this.namespaceName = namespaceName;
        this.mosaicName = mosaicName;
    }
    /**
     * Namespace and mosaic description
     * @returns {string}
     */
    MosaicView.prototype.fullName = function () {
        return this.namespaceName + ':' + this.mosaicName;
    };
    return MosaicView;
}());
exports.MosaicView = MosaicView;
//# sourceMappingURL=MosaicView.js.map