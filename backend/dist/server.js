"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Account_1 = require("./proximax-sdk/src/model/account/Account");
var NetworkType_1 = require("./proximax-sdk/src/model/blockchain/NetworkType");
var PublicAccount_1 = require("./proximax-sdk/src/model/account/PublicAccount");
var ModifyMultisigAccountTransaction_1 = require("./proximax-sdk/src/model/transaction/ModifyMultisigAccountTransaction");
var Deadline_1 = require("./proximax-sdk/src/model/transaction/Deadline");
var MultisigCosignatoryModification_1 = require("./proximax-sdk/src/model/transaction/MultisigCosignatoryModification");
var MultisigCosignatoryModificationType_1 = require("./proximax-sdk/src/model/transaction/MultisigCosignatoryModificationType");
var TransactionHttp_1 = require("./proximax-sdk/src/infrastructure/TransactionHttp");
var url = 'http://45.63.116.86:3000';
var transactionHttp = new TransactionHttp_1.TransactionHttp(url);
var privateKeyOfficers = "39E0C490EED83C1F29793808A60002F461D44EA7F5EBB6E08FAB819769FEBFC3";
var accountOfficers = Account_1.Account.createFromPrivateKey(privateKeyOfficers, NetworkType_1.NetworkType.MIJIN_TEST);
var mainOfficerPublicKey = 'F1734B2B1FC334CFFC109D15A8F69ABE6A26282A25BE22F889055CB1DBF24F96';
var mainOfficer = PublicAccount_1.PublicAccount.createFromPublicKey(mainOfficerPublicKey, NetworkType_1.NetworkType.MIJIN_TEST);
// const cosignatory2PublicKey = '1D722E14768C0A85C39D93D4F590E73922D264435F749113C07DC94949E37049';
// const cosignatory2 = PublicAccount.createFromPublicKey(cosignatory2PublicKey, NetworkType.MIJIN_TEST);
var convertIntoMultisigTransaction = ModifyMultisigAccountTransaction_1.ModifyMultisigAccountTransaction.create(Deadline_1.Deadline.create(), 1, 1, [
    new MultisigCosignatoryModification_1.MultisigCosignatoryModification(MultisigCosignatoryModificationType_1.MultisigCosignatoryModificationType.Add, mainOfficer),
], NetworkType_1.NetworkType.MIJIN_TEST);
var signedTransaction = accountOfficers.sign(convertIntoMultisigTransaction);
transactionHttp
    .announce(signedTransaction)
    .subscribe(function (x) { return console.log(x); }, function (err) { return console.error(err); });
console.log("Server is running on localhost:" + 3000);
//# sourceMappingURL=server.js.map