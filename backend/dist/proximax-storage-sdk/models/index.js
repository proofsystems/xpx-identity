"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var StorageContractDto = /** @class */ (function () {
    function StorageContractDto(_a) {
        var publicKey = _a.publicKey, clientPublicKey = _a.clientPublicKey, status = _a.status, fileSize = _a.fileSize, _b = _a.replicationFactor, replicationFactor = _b === void 0 ? 1 : _b, _c = _a.verifiersFactor, verifiersFactor = _c === void 0 ? 1 : _c, blockDuration = _a.blockDuration, _d = _a.pdpSchema, pdpSchema = _d === void 0 ? "APDP" : _d, _e = _a.preferableVerifiers, preferableVerifiers = _e === void 0 ? [] : _e, verifiersPubKey = _a.verifiersPubKey, replicatorsPubKey = _a.replicatorsPubKey, transactionHash = _a.transactionHash;
        this.publicKey = publicKey;
        this.clientPublicKey = clientPublicKey;
        this.status = status;
        this.fileSize = fileSize;
        this.replicationFactor = replicationFactor;
        this.verifiersFactor = verifiersFactor;
        this.blockDuration = blockDuration;
        this.pdpSchema = pdpSchema;
        this.preferableVerifiers = preferableVerifiers;
        this.verifiersPubKey = verifiersPubKey;
        this.replicatorsPubKey = replicatorsPubKey;
        this.transactionHash = transactionHash;
    }
    return StorageContractDto;
}());
exports.StorageContractDto = StorageContractDto;
var RestMethods;
(function (RestMethods) {
    RestMethods["CreateContract"] = "/contract/create";
    RestMethods["UploadFile"] = "/contract/upload";
    RestMethods["SignTx"] = "/contract/sign";
    RestMethods["DownloadFile"] = "/contract/download";
    RestMethods["ContractInfo"] = "/contract";
})(RestMethods = exports.RestMethods || (exports.RestMethods = {}));
var CreateContractRequest = /** @class */ (function () {
    function CreateContractRequest(public_key, client_public_key, file_size, replication_factor, verification_factor, duration, pdp_schema, verifiers) {
        this.public_key = public_key;
        this.client_public_key = client_public_key;
        this.file_size = file_size;
        this.replication_factor = replication_factor;
        this.verification_factor = verification_factor;
        this.duration = duration;
        this.pdp_schema = pdp_schema;
        this.verifiers = verifiers;
    }
    return CreateContractRequest;
}());
exports.CreateContractRequest = CreateContractRequest;
//# sourceMappingURL=index.js.map