/// <reference types="node" />
export declare class StorageContractDto {
    publicKey: any;
    clientPublicKey: any;
    status?: number;
    fileSize?: number;
    replicationFactor?: number;
    verifiersFactor?: number;
    blockDuration?: number;
    pdpSchema?: string;
    preferableVerifiers?: any;
    verifiersPubKey?: any;
    replicatorsPubKey?: any;
    transactionHash?: string;
    constructor({ publicKey, clientPublicKey, status, fileSize, replicationFactor, verifiersFactor, blockDuration, pdpSchema, preferableVerifiers, verifiersPubKey, replicatorsPubKey, transactionHash, }: StorageContractDto);
}
export declare enum RestMethods {
    CreateContract = "/contract/create",
    UploadFile = "/contract/upload",
    SignTx = "/contract/sign",
    DownloadFile = "/contract/download",
    ContractInfo = "/contract"
}
export declare class CreateContractRequest {
    public_key: string;
    client_public_key: string;
    file_size: number;
    replication_factor: number;
    verification_factor: number;
    duration: number;
    pdp_schema: string;
    verifiers: string[];
    constructor(public_key: string, client_public_key: string, file_size: number, replication_factor: number, verification_factor: number, duration: number, pdp_schema: string, verifiers: string[]);
}
export interface IUploadFileResponse {
    transaction_hash: string;
}
export interface KeyPair {
    privateKey: Buffer;
    publicKey: Buffer;
}
