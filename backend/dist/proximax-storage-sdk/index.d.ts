/// <reference types="node" />
import { Account } from "../proximax-sdk/src/model/model";
import { StorageContractDto } from "./models";
interface IProximaXStorage {
    createContract(contract: StorageContractDto): Promise<void>;
    uploadFile(contractPublicKey: Buffer, userAccount: Account, filePath: string): Promise<string>;
    signTx(contractPubKey: Buffer, txHash: string, accounts: Account[]): Promise<void>;
    downloadFile(contractPublicKey: Buffer): Promise<any>;
    getContractInfo(contractPublicKey: Buffer): Promise<StorageContractDto>;
}
declare class ProximaXStorage implements IProximaXStorage {
    private httpClient;
    constructor(address: string);
    createContract(contract: StorageContractDto): Promise<void>;
    uploadFile(contractPublicKey: Buffer, userAccount: Account, filePath: string): Promise<string>;
    signTx(contractPubKey: Buffer, txHash: string, accounts: Account[]): Promise<void>;
    downloadFile(contractPublicKey: Buffer): Promise<any>;
    getContractInfo(contractPublicKey: Buffer): Promise<StorageContractDto>;
}
export { ProximaXStorage, IProximaXStorage, };
