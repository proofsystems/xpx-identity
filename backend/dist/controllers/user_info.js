"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var path = require("path");
var fs = require("fs");
var axios_1 = require("axios");
var Account_1 = require("../proximax-sdk/src/model/account/Account");
var NetworkType_1 = require("../proximax-sdk/src/model/blockchain/NetworkType");
var PublicAccount_1 = require("../proximax-sdk/src/model/account/PublicAccount");
var ModifyMultisigAccountTransaction_1 = require("../proximax-sdk/src/model/transaction/ModifyMultisigAccountTransaction");
var Deadline_1 = require("../proximax-sdk/src/model/transaction/Deadline");
var MultisigCosignatoryModification_1 = require("../proximax-sdk/src/model/transaction/MultisigCosignatoryModification");
var MultisigCosignatoryModificationType_1 = require("../proximax-sdk/src/model/transaction/MultisigCosignatoryModificationType");
var PlainMessage_1 = require("../proximax-sdk/src/model/transaction/PlainMessage");
var XPX_1 = require("../proximax-sdk/src/model/mosaic/XPX");
var TransferTransaction_1 = require("../proximax-sdk/src/model/transaction/TransferTransaction");
var models_1 = require("../proximax-storage-sdk/models");
var Listener_1 = require("../proximax-sdk/src/infrastructure/Listener");
var TransactionHttp_1 = require("../proximax-sdk/src/infrastructure/TransactionHttp");
var proximax_storage_sdk_1 = require("../proximax-storage-sdk");
var operators_1 = require("rxjs/operators");
var models_2 = require("../models");
var AggregateTransaction_1 = require("../proximax-sdk/src/model/transaction/AggregateTransaction");
var LockFundsTransaction_1 = require("../proximax-sdk/src/model/transaction/LockFundsTransaction");
var UInt64_1 = require("../proximax-sdk/src/model/UInt64");
var url = 'http://45.63.116.86:3000';
var waitFor = function (address, signedTransaction) { return __awaiter(_this, void 0, void 0, function () {
    var listener, waitForResultOfMultisig, w;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                listener = new Listener_1.Listener(url);
                return [4 /*yield*/, listener.open()];
            case 1:
                _a.sent();
                waitForResultOfMultisig = function () { return new Promise(function (res, rej) { return listener
                    .confirmed(address)
                    .pipe(operators_1.filter(function (transaction) { return (transaction.transactionInfo !== undefined && transaction.transactionInfo.hash === signedTransaction.hash); }))
                    .subscribe(function (ignored) {
                    console.log("Transaction confirmed");
                    res("ok");
                }, function (error) { return rej(error); }); }); };
                w = waitForResultOfMultisig();
                transactionHttp.announce(signedTransaction);
                return [4 /*yield*/, w];
            case 2:
                _a.sent();
                listener.close();
                return [2 /*return*/];
        }
    });
}); };
var transactionHttp = new TransactionHttp_1.TransactionHttp(url);
var storage = new proximax_storage_sdk_1.ProximaXStorage("http://45.63.116.86:8080");
var accountOfficersPublicKey = "1DF9E353B3B69161BA648BC858A5DA1B35BA4D124ED4D9188523FD9D4153D86B";
exports.storeUserInfoController = function (ctx) { return __awaiter(_this, void 0, void 0, function () {
    var _a, clientPublicKey, _b, officerPrivateKey, users, image, userInfo, account, accountOfficer, publicAccountOfficers, accountClient, convertIntoMultisigTransaction, signedTransaction, transferTransaction, signedTransferTransaction, contractAccount, pathToFile, fileStat, contract, txHash, _c, e_1;
    return __generator(this, function (_d) {
        switch (_d.label) {
            case 0:
                _d.trys.push([0, 8, , 9]);
                _a = ctx.request.body, clientPublicKey = _a.publicKey, _b = _a.officerPrivateKey, officerPrivateKey = _b === void 0 ? "F1F3FCFF91F7E353DA8EE24A3261A3686EDFC38297477F23BC06A6C997939C1E" : _b, users = _a.userInfo, image = _a.image;
                userInfo = JSON.parse(users);
                console.log(userInfo);
                account = Account_1.Account.generateNewAccount(NetworkType_1.NetworkType.MIJIN_TEST);
                accountOfficer = Account_1.Account.createFromPrivateKey(officerPrivateKey, NetworkType_1.NetworkType.MIJIN_TEST);
                publicAccountOfficers = PublicAccount_1.PublicAccount.createFromPublicKey(accountOfficersPublicKey, NetworkType_1.NetworkType.MIJIN_TEST);
                accountClient = PublicAccount_1.PublicAccount.createFromPublicKey(clientPublicKey, NetworkType_1.NetworkType.MIJIN_TEST);
                convertIntoMultisigTransaction = ModifyMultisigAccountTransaction_1.ModifyMultisigAccountTransaction.create(Deadline_1.Deadline.create(), 1, 1, [
                    new MultisigCosignatoryModification_1.MultisigCosignatoryModification(MultisigCosignatoryModificationType_1.MultisigCosignatoryModificationType.Add, publicAccountOfficers),
                    new MultisigCosignatoryModification_1.MultisigCosignatoryModification(MultisigCosignatoryModificationType_1.MultisigCosignatoryModificationType.Add, accountClient),
                ], NetworkType_1.NetworkType.MIJIN_TEST);
                console.log("MULTISIG ACCOUNT", account.publicKey);
                signedTransaction = account.sign(convertIntoMultisigTransaction);
                return [4 /*yield*/, waitFor(account.address, signedTransaction)];
            case 1:
                _d.sent();
                transferTransaction = TransferTransaction_1.TransferTransaction.create(Deadline_1.Deadline.create(), account.address, [XPX_1.XPX.createRelative(10)], PlainMessage_1.PlainMessage.create('Welcome To ProximaX'), NetworkType_1.NetworkType.MIJIN_TEST);
                signedTransferTransaction = accountOfficer.sign(transferTransaction);
                return [4 /*yield*/, waitFor(accountOfficer.address, signedTransferTransaction)];
            case 2:
                _d.sent();
                contractAccount = Account_1.Account.generateNewAccount(NetworkType_1.NetworkType.MIJIN_TEST);
                pathToFile = path.resolve(__dirname, "../../files/" + contractAccount.publicKey + ".txt");
                fs.writeFileSync(pathToFile, JSON.stringify({ userInfo: userInfo, image: image }));
                fileStat = fs.statSync(pathToFile);
                contract = new models_1.StorageContractDto({
                    clientPublicKey: Buffer.from(account.publicKey, "hex"),
                    publicKey: Buffer.from(contractAccount.publicKey, "hex"),
                    blockDuration: 360,
                    fileSize: fileStat.size,
                });
                // save file
                return [4 /*yield*/, storage.createContract(contract)];
            case 3:
                // save file
                _d.sent();
                return [4 /*yield*/, storage.uploadFile(contract.publicKey, account, pathToFile)];
            case 4:
                txHash = _d.sent();
                console.log("file was upload");
                return [4 /*yield*/, storage.signTx(contract.publicKey, txHash, [contractAccount, accountOfficer])];
            case 5:
                _d.sent();
                console.log("transaction was signed by contract account");
                return [4 /*yield*/, models_2.userTable.addUser({
                        publicKey: clientPublicKey,
                        fullName: userInfo.fullName,
                        psn: account.publicKey,
                    })];
            case 6:
                _d.sent();
                console.log("user was added");
                _c = ctx;
                return [4 /*yield*/, storage.getContractInfo(contract.publicKey)];
            case 7:
                _c.body = _d.sent();
                return [3 /*break*/, 9];
            case 8:
                e_1 = _d.sent();
                console.log(e_1);
                ctx.body = e_1;
                return [3 /*break*/, 9];
            case 9: return [2 /*return*/];
        }
    });
}); };
exports.readUserInfoController = function (ctx) { return __awaiter(_this, void 0, void 0, function () {
    var publicKey, user, contracts, file, e_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 4, , 5]);
                publicKey = ctx.params.publicKey;
                return [4 /*yield*/, models_2.userTable.findUsers(publicKey)];
            case 1:
                user = (_a.sent())[0];
                return [4 /*yield*/, axios_1.default.get(url + "/account/" + user.psn + "/contracts")];
            case 2:
                contracts = (_a.sent()).data;
                return [4 /*yield*/, storage.downloadFile(contracts[0].contract.multisig)];
            case 3:
                file = _a.sent();
                file.fileContent.userInfo.id = user.psn;
                ctx.body = file;
                return [3 /*break*/, 5];
            case 4:
                e_2 = _a.sent();
                ctx.body = {
                    error: "No User"
                };
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); };
exports.recoverUserInfoController = function (ctx) { return __awaiter(_this, void 0, void 0, function () {
    var _a, fullName, clientPublicKey, _b, officerPrivateKey, user, officerAccount, multisigAccount, newClientAccount, modifyMultisigAccountTransaction, aggregateTransaction, signedTransaction, lockFundsTransaction, lockFundsTransactionSigned, e_3;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                _c.trys.push([0, 5, , 6]);
                _a = ctx.request.body, fullName = _a.fullName, clientPublicKey = _a.publicKey, _b = _a.officerPrivateKey, officerPrivateKey = _b === void 0 ? "F1F3FCFF91F7E353DA8EE24A3261A3686EDFC38297477F23BC06A6C997939C1E" : _b;
                return [4 /*yield*/, models_2.userTable.findUsers(fullName)];
            case 1:
                user = (_c.sent())[0];
                console.log(user);
                officerAccount = Account_1.Account.createFromPrivateKey(officerPrivateKey, NetworkType_1.NetworkType.MIJIN_TEST);
                multisigAccount = PublicAccount_1.PublicAccount.createFromPublicKey(user.psn, NetworkType_1.NetworkType.MIJIN_TEST);
                newClientAccount = PublicAccount_1.PublicAccount.createFromPublicKey(clientPublicKey, NetworkType_1.NetworkType.MIJIN_TEST);
                modifyMultisigAccountTransaction = ModifyMultisigAccountTransaction_1.ModifyMultisigAccountTransaction.create(Deadline_1.Deadline.create(), 0, 0, [
                    new MultisigCosignatoryModification_1.MultisigCosignatoryModification(MultisigCosignatoryModificationType_1.MultisigCosignatoryModificationType.Add, newClientAccount),
                    new MultisigCosignatoryModification_1.MultisigCosignatoryModification(MultisigCosignatoryModificationType_1.MultisigCosignatoryModificationType.Remove, PublicAccount_1.PublicAccount.createFromPublicKey(user.publicKey, NetworkType_1.NetworkType.MIJIN_TEST)),
                ], NetworkType_1.NetworkType.MIJIN_TEST);
                aggregateTransaction = AggregateTransaction_1.AggregateTransaction.createBonded(Deadline_1.Deadline.create(), [modifyMultisigAccountTransaction.toAggregate(multisigAccount)], NetworkType_1.NetworkType.MIJIN_TEST);
                signedTransaction = officerAccount.sign(aggregateTransaction);
                lockFundsTransaction = LockFundsTransaction_1.LockFundsTransaction.create(Deadline_1.Deadline.create(), XPX_1.XPX.createRelative(10), UInt64_1.UInt64.fromUint(480), signedTransaction, NetworkType_1.NetworkType.MIJIN_TEST);
                lockFundsTransactionSigned = officerAccount.sign(lockFundsTransaction);
                return [4 /*yield*/, waitFor(officerAccount.address, lockFundsTransactionSigned)];
            case 2:
                _c.sent();
                console.log("lock funds was committed");
                return [4 /*yield*/, waitFor(officerAccount.address, signedTransaction)];
            case 3:
                _c.sent();
                console.log("modify was committed");
                return [4 /*yield*/, models_2.userTable.changeUser(user.publicKey, clientPublicKey)];
            case 4:
                _c.sent();
                console.log("user was changed");
                ctx.body = "";
                return [3 /*break*/, 6];
            case 5:
                e_3 = _c.sent();
                console.log(e_3);
                ctx.body = e_3;
                return [3 /*break*/, 6];
            case 6: return [2 /*return*/];
        }
    });
}); };
//# sourceMappingURL=user_info.js.map