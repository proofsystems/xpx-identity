export declare const storeUserInfoController: (ctx: any) => Promise<void>;
export declare const readUserInfoController: (ctx: any) => Promise<void>;
export declare const recoverUserInfoController: (ctx: any) => Promise<void>;
