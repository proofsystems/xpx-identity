export declare class UserDb {
    collection: any;
    constructor(ip: string, login: string, password: string);
    addUser(user: {
        publicKey: string;
        fullName: string;
        psn: string;
    }): Promise<void>;
    changeUser(oldPublicKey: string, newPublicKey: string): Promise<void>;
    getAllUsers(): Promise<any>;
    findUsers(key: any): Promise<any>;
}
export declare const userTable: UserDb;
