
import * as path from "path";
import * as fs from "fs";
import axios from "axios";
import {Account} from "./proximax-sdk/src/model/account/Account";
import {NetworkType} from "./proximax-sdk/src/model/blockchain/NetworkType";
import {PublicAccount} from "./proximax-sdk/src/model/account/PublicAccount";
import {ModifyMultisigAccountTransaction} from "./proximax-sdk/src/model/transaction/ModifyMultisigAccountTransaction";
import {Deadline} from "./proximax-sdk/src/model/transaction/Deadline";
import {MultisigCosignatoryModification} from "./proximax-sdk/src/model/transaction/MultisigCosignatoryModification";
import {MultisigCosignatoryModificationType} from "./proximax-sdk/src/model/transaction/MultisigCosignatoryModificationType";
import {PlainMessage} from "./proximax-sdk/src/model/transaction/PlainMessage";
import {XPX} from "./proximax-sdk/src/model/mosaic/XPX";
import {TransferTransaction} from "./proximax-sdk/src/model/transaction/TransferTransaction";
import {StorageContractDto} from "./proximax-storage-sdk/models";
import {Listener} from "./proximax-sdk/src/infrastructure/Listener";
import {TransactionHttp} from "./proximax-sdk/src/infrastructure/TransactionHttp";
import {ProximaXStorage} from "./proximax-storage-sdk";
import {filter} from "rxjs/operators";
import {userTable} from "./models";
import {AggregateTransaction} from "./proximax-sdk/src/model/transaction/AggregateTransaction";
import {LockFundsTransaction} from "./proximax-sdk/src/model/transaction/LockFundsTransaction";
import {UInt64} from "./proximax-sdk/src/model/UInt64";

const url = 'http://45.63.116.86:3000';
const transactionHttp = new TransactionHttp(url);

const privateKeyOfficers = "39E0C490EED83C1F29793808A60002F461D44EA7F5EBB6E08FAB819769FEBFC3";
const accountOfficers = Account.createFromPrivateKey(privateKeyOfficers, NetworkType.MIJIN_TEST);

const mainOfficerPublicKey = 'F1734B2B1FC334CFFC109D15A8F69ABE6A26282A25BE22F889055CB1DBF24F96';
const mainOfficer = PublicAccount.createFromPublicKey(mainOfficerPublicKey, NetworkType.MIJIN_TEST);
// const cosignatory2PublicKey = '1D722E14768C0A85C39D93D4F590E73922D264435F749113C07DC94949E37049';
// const cosignatory2 = PublicAccount.createFromPublicKey(cosignatory2PublicKey, NetworkType.MIJIN_TEST);

const convertIntoMultisigTransaction = ModifyMultisigAccountTransaction.create(
  Deadline.create(),
  1,
  1,
  [
    new MultisigCosignatoryModification(
      MultisigCosignatoryModificationType.Add,
      mainOfficer,
    ),
  ],
  NetworkType.MIJIN_TEST,
);

const signedTransaction = accountOfficers.sign(convertIntoMultisigTransaction);

transactionHttp
  .announce(signedTransaction)
  .subscribe(x => console.log(x), (err) => console.error(err));
