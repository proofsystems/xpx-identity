import { MongoClient } from "mongodb";

export class UserDb {
    collection: any;

    constructor(ip: string, login: string, password: string) {
        const mongoClient = new MongoClient("mongodb://" + login + ":" + password + "@" + ip + "/", { useNewUrlParser: true });
        mongoClient.connect().then(client => {
            const db = client.db("usersdb");
            this.collection = db.collection("users");
        });
    }

    async addUser(user: { publicKey: string, fullName: string, psn: string }) {
        await this.collection.insertOne(user);
    }

    async changeUser(oldPublicKey: string, newPublicKey: string) {
        await this.collection.updateOne({
            publicKey: oldPublicKey
        }, {
            "$set": { publicKey: newPublicKey }
        });
    }

    async getAllUsers() {
        return await this.collection.find({}).toArray();
    }

    async findUsers(key) {
        let query = {
            $or:[
                { "publicKey" : new RegExp(key + ".*") },
                { "fullName" : new RegExp(key + ".*") },
                { "psn" : new RegExp(key + ".*") }
            ]
        };

        return await this.collection.find(query).toArray();
    }
}

export const userTable = new UserDb("45.63.116.86:27017", "482.solutions", "Hello482World");
