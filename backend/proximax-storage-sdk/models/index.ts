export class StorageContractDto {
    publicKey;
    clientPublicKey;
    status?: number;
    fileSize?: number;
    replicationFactor?: number;
    verifiersFactor?: number;
    blockDuration?: number;
    pdpSchema?: string;
    preferableVerifiers?;
    verifiersPubKey?;
    replicatorsPubKey?;
    transactionHash?: string;

    constructor({
                    publicKey,
                    clientPublicKey,
                    status,
                    fileSize,
                    replicationFactor = 1,
                    verifiersFactor = 1,
                    blockDuration,
                    pdpSchema = "APDP",
                    preferableVerifiers = [],
                    verifiersPubKey,
                    replicatorsPubKey,
                    transactionHash,
    }: StorageContractDto) {
        this.publicKey = publicKey;
        this.clientPublicKey = clientPublicKey;
        this.status = status;
        this.fileSize = fileSize;
        this.replicationFactor = replicationFactor;
        this.verifiersFactor = verifiersFactor;
        this.blockDuration = blockDuration;
        this.pdpSchema = pdpSchema;
        this.preferableVerifiers = preferableVerifiers;
        this.verifiersPubKey = verifiersPubKey;
        this.replicatorsPubKey = replicatorsPubKey;
        this.transactionHash = transactionHash;
    }
}

export enum RestMethods {
    CreateContract = "/contract/create",
    UploadFile = "/contract/upload",
    SignTx = "/contract/sign",
    DownloadFile = "/contract/download",
    ContractInfo = "/contract",
}

export class CreateContractRequest {
    constructor(
        public public_key: string,
        public client_public_key: string,
        public file_size: number,
        public replication_factor: number,
        public verification_factor: number,
        public duration: number,
        public pdp_schema: string,
        public verifiers: string[],
    ) {}
}

export interface IUploadFileResponse {
    transaction_hash: string
}

export interface KeyPair {
    privateKey: Buffer;
    publicKey: Buffer;
}