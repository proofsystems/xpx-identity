import axios from "axios";
import * as fs from "fs";
import * as path from "path";
import * as FormData from "form-data";
import { Account } from "../proximax-sdk/src/model/model";

import {StorageContractDto, RestMethods, CreateContractRequest} from "./models";

interface IProximaXStorage {
    createContract(contract: StorageContractDto): Promise<void>;
    uploadFile(contractPublicKey: Buffer, userAccount: Account, filePath: string): Promise<string>;
    signTx(contractPubKey: Buffer, txHash: string, accounts: Account[]): Promise<void>;
    downloadFile(contractPublicKey: Buffer): Promise<any>;
    getContractInfo(contractPublicKey: Buffer): Promise<StorageContractDto>;
}

class ProximaXStorage implements IProximaXStorage {
    private httpClient;

    constructor(
        address: string,
    ) {
        this.httpClient = axios.create({ baseURL: address });
    }

    async createContract(contract: StorageContractDto): Promise<void> {
        await this.httpClient.post(RestMethods.CreateContract, new CreateContractRequest(
            contract.publicKey.toString("hex"),
            contract.clientPublicKey.toString("hex"),
            contract.fileSize,
            contract.replicationFactor,
            contract.verifiersFactor,
            contract.blockDuration,
            contract.pdpSchema,
            contract.preferableVerifiers
        ));
    }

    async uploadFile(contractPublicKey: Buffer, userAccount: Account, filePath: string): Promise<string> {
        const form = new FormData();
        form.append("file", fs.readFileSync(path.resolve(filePath)), {
            filename: path.basename(filePath)
        });
        const signature = userAccount.signData(contractPublicKey.toString("hex"));
        const {
            data: { transaction_hash }
        } = await this.httpClient.post(RestMethods.UploadFile, form, {
            params: {
                contractPublicKey: contractPublicKey.toString("hex")
            },
            headers: {
                'content-type': `multipart/form-data; boundary=${form.getBoundary()}`,
                'Signed-ContractClient-PubKey': signature
            }
        });
        return transaction_hash;
    }

    async signTx(contractPubKey: Buffer, txHash: string, accounts: Account[]): Promise<void> {
        const cosignatures = accounts.map(account => ({
            signer_pubKey: account.publicKey,
            signed_tx_hash: account.signData(txHash),
        }));
        const body = {
            contract_pubKey: contractPubKey.toString("hex"),
            cosignatures
        };
        console.log(body);
        await this.httpClient.post(RestMethods.SignTx, body);
    };

    async downloadFile(contractPublicKey: Buffer): Promise<any> {
        const response = await this.httpClient.get(RestMethods.DownloadFile, {
            params: {
                contractPublicKey: contractPublicKey.toString("hex")
            }
        });
        return {
            fileContent: response.data,
            fileName: response.headers['content-disposition'].split("; ")[1].split("=")[1]
        }
    };

    async getContractInfo(contractPublicKey: Buffer): Promise<StorageContractDto> {
        const response = await this.httpClient.get(RestMethods.ContractInfo, {
            params: {
                contractPublicKey: contractPublicKey.toString("hex")
            }
        });
        return response.data
    };
}

export {
    ProximaXStorage,
    IProximaXStorage,
};