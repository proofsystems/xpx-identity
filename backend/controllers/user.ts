import {userTable} from "../models";

export const findUserController = async (ctx: any) => {
    const { search } = ctx.request.query;
    console.log(search);
    ctx.body = await userTable.findUsers(search);
};

export const getAllUsersController = async (ctx: any) => {
    ctx.body = await userTable.getAllUsers();
};