import * as path from "path";
import * as fs from "fs";
import axios from "axios";
import {Account} from "../proximax-sdk/src/model/account/Account";
import {NetworkType} from "../proximax-sdk/src/model/blockchain/NetworkType";
import {PublicAccount} from "../proximax-sdk/src/model/account/PublicAccount";
import {ModifyMultisigAccountTransaction} from "../proximax-sdk/src/model/transaction/ModifyMultisigAccountTransaction";
import {Deadline} from "../proximax-sdk/src/model/transaction/Deadline";
import {MultisigCosignatoryModification} from "../proximax-sdk/src/model/transaction/MultisigCosignatoryModification";
import {MultisigCosignatoryModificationType} from "../proximax-sdk/src/model/transaction/MultisigCosignatoryModificationType";
import {PlainMessage} from "../proximax-sdk/src/model/transaction/PlainMessage";
import {XPX} from "../proximax-sdk/src/model/mosaic/XPX";
import {TransferTransaction} from "../proximax-sdk/src/model/transaction/TransferTransaction";
import {StorageContractDto} from "../proximax-storage-sdk/models";
import {Listener} from "../proximax-sdk/src/infrastructure/Listener";
import {TransactionHttp} from "../proximax-sdk/src/infrastructure/TransactionHttp";
import {ProximaXStorage} from "../proximax-storage-sdk";
import {filter} from "rxjs/operators";
import {userTable} from "../models";
import {AggregateTransaction} from "../proximax-sdk/src/model/transaction/AggregateTransaction";
import {LockFundsTransaction} from "../proximax-sdk/src/model/transaction/LockFundsTransaction";
import {UInt64} from "../proximax-sdk/src/model/UInt64";

const url = 'http://45.63.116.86:3000';

const waitFor = async (address, signedTransaction) => {
    const listener = new Listener(url);
    await listener.open();
    const waitForResultOfMultisig = () => new Promise((res, rej) => listener
        .confirmed(address)
        .pipe(
            filter(transaction => (
                    transaction.transactionInfo !== undefined && transaction.transactionInfo.hash === signedTransaction.hash
                )
            )
        )
        .subscribe(
            ignored => {
                console.log("Transaction confirmed");
                res("ok");
            },
            error => rej(error)
        )
    );

    const w = waitForResultOfMultisig();
    transactionHttp.announce(signedTransaction);
    await w;
    listener.close();
};

const transactionHttp = new TransactionHttp(url);
const storage = new ProximaXStorage("http://45.63.116.86:8080");
const accountOfficersPublicKey = "1DF9E353B3B69161BA648BC858A5DA1B35BA4D124ED4D9188523FD9D4153D86B";

export const storeUserInfoController = async (ctx: any) => {
    try {
        const {
            publicKey: clientPublicKey,
            officerPrivateKey = "F1F3FCFF91F7E353DA8EE24A3261A3686EDFC38297477F23BC06A6C997939C1E",
            userInfo: users,
            image,
            fingerprint
        } = ctx.request.body;

        const userInfo = JSON.parse(users);
        console.log(userInfo);

        const account = Account.generateNewAccount(NetworkType.MIJIN_TEST);
        const accountOfficer = Account.createFromPrivateKey(officerPrivateKey, NetworkType.MIJIN_TEST);
        const publicAccountOfficers = PublicAccount.createFromPublicKey(accountOfficersPublicKey, NetworkType.MIJIN_TEST);
        const accountClient = PublicAccount.createFromPublicKey(clientPublicKey, NetworkType.MIJIN_TEST);

        const convertIntoMultisigTransaction = ModifyMultisigAccountTransaction.create(
            Deadline.create(),
            1,
            1,
            [
                new MultisigCosignatoryModification(
                    MultisigCosignatoryModificationType.Add,
                    publicAccountOfficers,
                ),
                new MultisigCosignatoryModification(
                    MultisigCosignatoryModificationType.Add,
                    accountClient,
                ),
            ],
            NetworkType.MIJIN_TEST,
        );

        console.log("MULTISIG ACCOUNT", account.publicKey);

        const signedTransaction = account.sign(convertIntoMultisigTransaction);

        await waitFor(account.address, signedTransaction);

        const transferTransaction = TransferTransaction.create(
            Deadline.create(),
            account.address,
            [XPX.createRelative(10)],
            PlainMessage.create('Welcome To ProximaX'),
            NetworkType.MIJIN_TEST,
        );

        const signedTransferTransaction = accountOfficer.sign(transferTransaction);

        await waitFor(accountOfficer.address, signedTransferTransaction);

        const contractAccount = Account.generateNewAccount(NetworkType.MIJIN_TEST);

        const pathToFile = path.resolve(__dirname, `../../files/${contractAccount.publicKey}.txt`);

        fs.writeFileSync(pathToFile, JSON.stringify({ userInfo, image, fingerprint: JSON.parse(fingerprint) }));

        const fileStat = fs.statSync(pathToFile);

        const contract = new StorageContractDto({
            clientPublicKey: Buffer.from(account.publicKey, "hex"),
            publicKey: Buffer.from(contractAccount.publicKey, "hex"),
            blockDuration: 360,
            fileSize: fileStat.size,
        });

        // save file
        await storage.createContract(contract);
        const txHash = await storage.uploadFile(contract.publicKey, account, pathToFile);
        console.log("file was upload");
        await storage.signTx(contract.publicKey, txHash, [contractAccount, accountOfficer]);
        console.log("transaction was signed by contract account");

        await userTable.addUser({
            publicKey: clientPublicKey,
            fullName: userInfo.fullName,
            psn: account.publicKey,
        });
        console.log("user was added");

        ctx.body = await storage.getContractInfo(contract.publicKey);
    } catch (e) {
        console.log(e);
        ctx.body = e;
    }

};

export const readUserInfoController = async (ctx: any) => {
    try {
        const { publicKey } = ctx.params;
        const [ user ] = await userTable.findUsers(publicKey);
        const { data: contracts } = await axios.get(`${url}/account/${user.psn}/contracts`);
        const file = await storage.downloadFile(contracts[0].contract.multisig);
        file.fileContent.userInfo.id = user.psn;
        ctx.body = file;
    } catch (e) {
        ctx.body = {
            error: "No User"
        };
    }
};

export const recoverUserInfoController = async (ctx: any) => {
    try {
        const {
            fullName,
            publicKey: clientPublicKey,
            officerPrivateKey = "F1F3FCFF91F7E353DA8EE24A3261A3686EDFC38297477F23BC06A6C997939C1E",
        } = ctx.request.body;
        console.log(officerPrivateKey);
        const [ user ] = await userTable.findUsers(fullName);
        console.log(user);

        const officerAccount = Account.createFromPrivateKey(officerPrivateKey, NetworkType.MIJIN_TEST);

        const multisigAccount = PublicAccount.createFromPublicKey(user.psn, NetworkType.MIJIN_TEST);

        const newClientAccount = PublicAccount.createFromPublicKey(clientPublicKey, NetworkType.MIJIN_TEST);

        const modifyMultisigAccountTransaction = ModifyMultisigAccountTransaction.create(
            Deadline.create(),
            0,
            0,
            [
                new MultisigCosignatoryModification(
                    MultisigCosignatoryModificationType.Add,
                    newClientAccount,
                ),
                new MultisigCosignatoryModification(
                    MultisigCosignatoryModificationType.Remove,
                    PublicAccount.createFromPublicKey(user.publicKey, NetworkType.MIJIN_TEST),
                ),
            ],
            NetworkType.MIJIN_TEST,
        );

        const aggregateTransaction = AggregateTransaction.createBonded(
            Deadline.create(),
            [modifyMultisigAccountTransaction.toAggregate(multisigAccount)],
            NetworkType.MIJIN_TEST,
        );

        const signedTransaction = officerAccount.sign(aggregateTransaction);

        console.log("aggregate transaction was signed");

        const lockFundsTransaction = LockFundsTransaction.create(
            Deadline.create(),
            XPX.createRelative(10),
            UInt64.fromUint(480),
            signedTransaction,
            NetworkType.MIJIN_TEST
        );

        const lockFundsTransactionSigned = officerAccount.sign(lockFundsTransaction);

        console.log("lock fund was signed and will send");

        await waitFor(officerAccount.address, lockFundsTransactionSigned);

        console.log("lock funds was committed");

        await waitFor(officerAccount.address, signedTransaction);

        console.log("modify was committed");

        await userTable.changeUser(user.publicKey, clientPublicKey);
        console.log("user was changed");

        ctx.body = "";
    } catch (e) {
        console.log(e);
        ctx.body = e;
    }
};