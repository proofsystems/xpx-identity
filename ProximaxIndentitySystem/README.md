# How to run
**This is snap of ProximaX with pre-generated account of officer**
To restart snapped ProximaX you can run follow command:
```
sudo ./relaunchIndentity.sh
```

It will launch local Blockchain, MongoDb with users and Storage layer of ProximaX.
All IPs in config files are hardcoded with `45.63.116.86`. If you want to change location snapped ProximaX, please change it in whole project  