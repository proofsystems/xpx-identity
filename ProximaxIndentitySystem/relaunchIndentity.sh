#!/usr/bin/env bash
./relaunchBlockChain.sh

cd mongodb
docker-compose down
rm -R data
docker-compose up -d
cd -

cd Storage
docker-compose down
docker-compose up -d
cd -