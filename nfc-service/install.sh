#!/bin/bash
# display user home

if [[ "$OSTYPE" == "darwin"* ]]; then
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null 2> /dev/null

    printf "\nInstalling boost\n\n"
    brew install boost

    printf "\nInstalling pcsc-lite\n\n"
    brew install pcsc-lite

    printf "\nInstalling cmake\n\n"
    brew install cmake
else
    apt-get install -y build-essential \
        && apt-get install -y systemd \
        && apt-get install -y cmake \
        && apt-get install -y libboost-system-dev \
        && apt-get install -y libpcsclite-dev \
        && apt-get install -y libusb-0.1-4 \
        && apt-get install -y git \
        && apt-get install -y pcscd \
        && apt-get install -y pcsc-tools
fi

if [[ ! -d "exec" ]]; then
    mkdir exec
    git clone https://github.com/lessandro/nbis.git
    cd nbis
    make clean
    mkdir temp
    ./setup.sh $(pwd)/temp --without-X11 --64
    make config
    make it
    cp imgtools/bin/cwsq ../exec/
    cp bozorth3/bin/bozorth3 ../exec/
    cp mindtct/bin/mindtct ../exec/
    cd -
    rm -rf nbis
fi