#Test MIFARE CLASSIC
This program need for show what we can store and read on smart card MIFARE Classic 4K.

##Commands:


* help - show this help page.
* gen_key - generate keypair which valid for ProximaX blockchain.
* store_public - store public key in second sector of smart card.
* store_private - store private key in third sector of smart card.
* get_public - read public key from second sector of smart card.
* get_private - read private key from third sector of smart card.
* activate - activate proxima account with help sending transfer transaction with empty message.
* exit - exit from this programm.

## Building

To install all dependencies you can run `install.sh` script(Supported OS: MacOS and Ubuntu).
```bash
./install.sh
mkdir build && cd build
cmake ..
make
```

##TODO:

Complete websockets part
