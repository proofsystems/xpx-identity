/*
* MIT License
*
* Copyright (c) 2018 Derick Felix <derickfelix@zoho.com>
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
#include "Bozorth3.h"
#include "Utils.h"
#include <vector>

Bozorth3::Bozorth3(const std::string& source, const std::string& dest)
: m_SourceName(source), m_DestName(dest)
{
  m_Command << "exec" << SEPARATOR << "bozorth3 -m1 " << m_SourceName << ".xyt" << " " << m_DestName << ".xyt";
}

Bozorth3::~Bozorth3()
{
  std::vector<std::string> extensions = {".xyt"};
  LOG("Cleaning up files")
  for (const auto& s : extensions) {
    remove((m_SourceName + s).c_str());
    remove((m_DestName + s).c_str());
  }
}

int Bozorth3::Execute()
{
  LOG("Executing Bozorth3")
  auto s = Fcmb::exec(m_Command.str().c_str());
  if (s.empty()) {
    std::cout << "Failed to execute Bozorth3!" << std::endl;
  } else {
    LOG("Done!")
  }
  return stoi(s);
}
