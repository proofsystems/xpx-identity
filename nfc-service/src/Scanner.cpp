/*
* MIT License
*
* Copyright (c) 2018 Derick Felix <derickfelix@zoho.com>
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
#include "Scanner.h"
#include "Utils.h"
#include <sstream>

#include <cstring>
#ifdef _WIN32
    #include <Windows.h>
#else
    #include <chrono>
    #include <thread>
#endif

Scanner::Scanner()
{
    m_Device = ftrScanOpenDevice();

    if(m_Device == NULL)
    {
        if (ftrScanGetLastError() == 21) std::cout << "Permission denied, are you root?" << std::endl;
        throw ScannerException("Failed to open device!");
    }
}

Scanner::~Scanner()
{
    LOG("Device closed!")
    if (m_Device != NULL) ftrScanCloseDevice(m_Device);
    if (m_Buffer != NULL) free(m_Buffer);
}

std::vector<uint8_t> Scanner::ScanImage()
{
    if(!ftrScanGetImageSize(m_Device, &m_ImageSize))
    {
        throw ScannerException("Failed to get image size");
    }

    LOG("Image size caught: ")
    LOG(m_ImageSize.nImageSize)

    m_Buffer = (unsigned char *) malloc(m_ImageSize.nImageSize);

    std::cout << "Please put your finger on the scanner: " << std::endl;

    while(true)
    {
        while(true)
        {
			if( ftrScanIsFingerPresent( m_Device, NULL ) )
				break;
			Fcmb::sleep(700);
        }

        LOG("Capturing fingerprint...")

        if(ftrScanGetFrame(m_Device, m_Buffer, NULL))
        {
            LOG("Done\nWriting to file...")
            return WriteBmpFile(m_ImageSize.nWidth, m_ImageSize.nHeight);
        }
        else
        {
            unsigned long error = ftrScanGetLastError();
            // in case of moveable finger there is a way to try again
            if (error == FTR_ERROR_MOVABLE_FINGER)
            {
                ShowError(error);
            }
        }
		Fcmb::sleep(100);
    }
}

std::vector<uint8_t> Scanner::WriteBmpFile(int width, int height)
{
	BITMAPINFO *pDIBHeader;
	BITMAPFILEHEADER    bmfHeader;
	int iCyc;

	// allocate memory for a DIB header
	if( (pDIBHeader = (BITMAPINFO *)malloc( sizeof( BITMAPINFO ) + sizeof( RGBQUAD ) * 255 )) == NULL )
	{
		throw ScannerException("Alloc memory failed! - Unable to write to file!!\n");
	}
	memset(  (void *)pDIBHeader, 0, sizeof( BITMAPINFO ) + sizeof( RGBQUAD ) * 255 );
	// fill the DIB header
	pDIBHeader->bmiHeader.biSize                    = sizeof( BITMAPINFOHEADER );
	pDIBHeader->bmiHeader.biWidth                 = width;
	pDIBHeader->bmiHeader.biHeight                = height;
	pDIBHeader->bmiHeader.biPlanes                = 1;
	pDIBHeader->bmiHeader.biBitCount            = 8;		// 8bits gray scale bmp
	pDIBHeader->bmiHeader.biCompression     = 0;		// BI_RGB = 0;
	// initialize logical and DIB grayscale
	for( iCyc = 0; iCyc < 256; iCyc++ )
	{
		pDIBHeader->bmiColors[iCyc].rgbBlue = pDIBHeader->bmiColors[iCyc].rgbGreen = pDIBHeader->bmiColors[iCyc].rgbRed = (unsigned char)iCyc;
	}
	// set BITMAPFILEHEADER structure
	//((char *)(&bmfHeader.bfType))[0] = 'B';
	//((char *)(&bmfHeader.bfType))[1] = 'M';
	bmfHeader.bfType = 0x42 + 0x4D * 0x100;
	bmfHeader.bfSize = 14 + sizeof( BITMAPINFO ) + sizeof( RGBQUAD ) * 255 + width * height;	//sizeof( BITMAPFILEHEADER ) = 14
	bmfHeader.bfOffBits = 14 + pDIBHeader->bmiHeader.biSize + sizeof( RGBQUAD ) * 256;

	std::stringstream buffer;

	//buffer.write(  (const char*)&bmfHeader, 1, sizeof(BITMAPFILEHEADER), fp );
	buffer.write(  (const char*)&bmfHeader.bfType, sizeof(unsigned short int) );
	buffer.write(  (const char*)&bmfHeader.bfSize, sizeof(unsigned int) );
	buffer.write(  (const char*)&bmfHeader.bfReserved1, sizeof(unsigned short int) );
	buffer.write(  (const char*)&bmfHeader.bfReserved2, sizeof(unsigned short int) );
	buffer.write(  (const char*)&bmfHeader.bfOffBits, sizeof(unsigned int) );
	//buffer.write(  (const char*)pDIBHeader, sizeof( BITMAPINFO ) + sizeof( RGBQUAD ) * 255);
	buffer.write(  (const char*)&pDIBHeader->bmiHeader.biSize, sizeof(unsigned int) );
	buffer.write(  (const char*)&pDIBHeader->bmiHeader.biWidth, sizeof(int) );
	buffer.write(  (const char*)&pDIBHeader->bmiHeader.biHeight, sizeof(int) );
	buffer.write(  (const char*)&pDIBHeader->bmiHeader.biPlanes, sizeof(unsigned short int) );
	buffer.write(  (const char*)&pDIBHeader->bmiHeader.biBitCount, sizeof(unsigned short int) );
	buffer.write(  (const char*)&pDIBHeader->bmiHeader.biCompression, sizeof(unsigned int) );
	buffer.write(  (const char*)&pDIBHeader->bmiHeader.biSizeImage, sizeof(unsigned int) );
	buffer.write(  (const char*)&pDIBHeader->bmiHeader.biXPelsPerMeter, sizeof(int) );
	buffer.write(  (const char*)&pDIBHeader->bmiHeader.biYPelsPerMeter, sizeof(int) );
	buffer.write(  (const char*)&pDIBHeader->bmiHeader.biClrUsed, sizeof(unsigned int) );
	buffer.write(  (const char*)&pDIBHeader->bmiHeader.biClrImportant, sizeof(unsigned int) );
	for( iCyc=0; iCyc<256; iCyc++ )
	{
		buffer.write(  (const char*)&pDIBHeader->bmiColors[iCyc].rgbBlue, sizeof(unsigned char) );
		buffer.write(  (const char*)&pDIBHeader->bmiColors[iCyc].rgbGreen, sizeof(unsigned char) );
		buffer.write(  (const char*)&pDIBHeader->bmiColors[iCyc].rgbRed, sizeof(unsigned char) );
		buffer.write(  (const char*)&pDIBHeader->bmiColors[iCyc].rgbReserved, sizeof(unsigned char) );
	}
	//
	// copy fingerprint image
	unsigned char *cptrData;
	unsigned char *cptrDIBData;
	unsigned char *pDIBData;

	pDIBData = (unsigned char *)malloc( height * width);
	memset( (void *)pDIBData, 0, height * width );

	cptrData = m_Buffer + (height - 1) * width;
	cptrDIBData = pDIBData;
	for( iCyc = 0; iCyc < height; iCyc++ )
	{
		memcpy( cptrDIBData, cptrData, width );
		cptrData = cptrData - width;
		cptrDIBData = cptrDIBData + width;
	}
	buffer.write(  (const char*)pDIBData, width * height);
	printf("\n");
	free( pDIBData );
	free( pDIBHeader );

	auto s = buffer.str();
	std::vector<uint8_t> result(s.begin(), s.end());

	return result;
}

void Scanner::ShowError(unsigned long error)
{
	std::cout << "Failed to get image:" << std::endl;
	switch(error)
	{
		case 0:
			std::cout << "OK" ;
			break;
		case FTR_ERROR_EMPTY_FRAME:	// ERROR_EMPTY
			std::cout << "- Empty frame -" << std::endl;
			break;
		case FTR_ERROR_NO_FRAME:
			std::cout <<    "- No frame -" << std::endl;
			break;
		case FTR_ERROR_USER_CANCELED:
			std::cout << "- User canceled -" << std::endl;
			break;
		case FTR_ERROR_HARDWARE_INCOMPATIBLE:
			std::cout << "- Incompatible hardware -" << std::endl;
			break;
		case FTR_ERROR_FIRMWARE_INCOMPATIBLE:
			std::cout << "- Incompatible firmware -" << std::endl;
			break;
		case FTR_ERROR_INVALID_AUTHORIZATION_CODE:
			std::cout << "- Invalid authorization code -" << std::endl;
			break;
		default:
			std::cout << "Unknown return code - " << error << std::endl;
	}
}