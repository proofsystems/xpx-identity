#ifdef __APPLE__
#include <PCSC/winscard.h>
#include <PCSC/wintypes.h>
#else
#include <winscard.h>
#endif

#include <iostream>
#include <stdio.h>
#include <sstream>
#include <cstring>
#include <cstdlib>
#include <vector>

#include "websocketpp/base64/base64.hpp"
#include "websocketpp/config/asio_no_tls.hpp"
#include "websocketpp/server.hpp"
#include "websocketpp/frame.hpp"

#include "json/json.hpp"

#include "Bozorth3.h"
#include "Cwsq.h"
#include "Mindtct.h"
#include "Scanner.h"
#include "Utils.h"
#include <iostream>
#include <sstream>
#include <vector>

#define HALF_OF_KEY 16

using json = nlohmann::json;


BYTE loadKeyA[]  = { 0xFF, 0x82, 0x00, 0x00, 0x06, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
BYTE auth[]      = { 0xFF, 0x86, 0x00, 0x00, 0x05, 0x01, 0x00};
BYTE authSector2[]    = { 0x04, 0x60, 0x00 };
BYTE authSector3[]    = { 0x08, 0x60, 0x00 };


typedef websocketpp::server<websocketpp::config::asio> server;
using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;
typedef server::message_ptr message_ptr;


void on_message(server* s, websocketpp::connection_hdl hdl, message_ptr msg);
void on_connected(server* s, websocketpp::connection_hdl hdl);
void generateReadCommand(BYTE numBlock, BYTE size, LPBYTE readyCommand);
void generateWriteCommand(BYTE numBlock, BYTE size, unsigned char* data, LPBYTE readyCommand);
void generateCommand(LPBYTE cmd, int cmdLen, LPBYTE data, int dataLen, LPBYTE readyCommand);
std::vector<uint8_t> hexToByte(std::string key);
std::string byteToHex(uint8_t* data, unsigned long size);
void showError(DWORD rv);
std::vector<BYTE> sendRequest(SCARDHANDLE hCard, LPBYTE cmd, int cmdLen, bool key);
std::vector<uint8_t> getImage();
bool compareFingerPrints(const std::vector<uint8_t>& left, const std::vector<uint8_t>& right);

int main() {
	srand(time(0));
    server server;

    try {
        // Set logging settings
        server.set_access_channels(websocketpp::log::alevel::all);
        server.clear_access_channels(websocketpp::log::alevel::frame_payload);

        // Initialize Asio
        server.init_asio();

        // Register our message handler
        server.set_message_handler(bind(&on_message,&server,::_1,::_2));
        server.set_open_handler(bind(&on_connected,&server,::_1));

        // Listen on port 9002
        server.listen(9002);
        std::cout << "Listening on 9002" << std::endl;
        // Start the server accept loop
        server.start_accept();
        // Start the ASIO io_service run loop
        server.run();
    } catch (websocketpp::exception const & e) {
        std::cout << e.what() << std::endl;
    } catch (...) {
        std::cout << "other exception" << std::endl;
    }


    return 0;
}

void on_message(server* s, websocketpp::connection_hdl hdl, message_ptr msg){

    json result;
	bool flagError = false;
	auto data = json::parse(msg->get_payload());
	std::string method = data["method"].get<std::string>();

	if (method == "read_fingerprint") {
		try
		{
			auto fingerprint = getImage();
			result["status"] = "Successfull Reading";
			result["result"] = websocketpp::base64_encode(fingerprint.data(), fingerprint.size());
		}
		catch (const ScannerException& e)
		{
			result["status"] = "Error in reading";
			result["error"] = e.what();
		}
	} else if (method == "match_fingerprint") {
		std::vector<std::string> verify = data["params"]["verify"].get<std::vector<std::string>>();
		std::string proof = data["params"]["proof"].get<std::string>();

		std::string rightBytes = websocketpp::base64_decode(proof);
		bool verified = false;
		for (const auto& verifier : verify) {
			std::string leftBytes = websocketpp::base64_decode(verifier);

			try
			{
				result["status"] = "Successfull Matching";
				verified |= compareFingerPrints(
						std::vector<uint8_t>(leftBytes.begin(), leftBytes.end()),
						std::vector<uint8_t>(rightBytes.begin(), rightBytes.end())
				);
				result["result"] = verified;
			}
			catch (...)
			{
				result["status"] = "Error in matching";
				break;
			}
		}
	} else {
		std::vector<BYTE> temp;

		DWORD rv;
		SCARDCONTEXT hContext;
		LPSTR mszReaders;
		DWORD dwReaders;
		SCARDHANDLE hCard;
		DWORD dwActiveProtocol;
		char* readers[16];
		rv = SCardEstablishContext(SCARD_SCOPE_SYSTEM, NULL, NULL, &hContext);

		if(rv != SCARD_S_SUCCESS) {
			printf("Driver not available");
			result["status"] = "Driver not available";
			flagError = true;
		}

#ifdef SCARD_AUTOALLOCATE
		dwReaders = SCARD_AUTOALLOCATE;

		rv = SCardListReaders(hContext, NULL, (LPTSTR)&mszReaders, &dwReaders);
#else
	rv = SCardListReaders(hContext, NULL, NULL, &dwReaders);

mszReaders = (LPSTR)calloc(dwReaders, sizeof(char));
rv = SCardListReaders(hContext, NULL, mszReaders, &dwReaders);
#endif

		if(!flagError){
			int nbReaders = 0;
			char* ptr = mszReaders;
			while (*ptr != '\0')
			{
				printf("%d: %s\n", nbReaders, ptr);
				readers[nbReaders] = ptr;
				ptr += strlen(ptr)+1;
				nbReaders++;
			}

			rv = SCardConnect(hContext, readers[0], SCARD_SHARE_SHARED, SCARD_PROTOCOL_ANY, &hCard, &dwActiveProtocol);
			while(rv != SCARD_S_SUCCESS){
				rv = SCardConnect(hContext, readers[0], SCARD_SHARE_SHARED, SCARD_PROTOCOL_ANY, &hCard, &dwActiveProtocol);
			}
		}


		SCardBeginTransaction(hCard);

		temp = sendRequest(hCard, loadKeyA, sizeof(loadKeyA), false);
		if (temp.empty()) {
			result["status"] = "Problem with loading key to reader";
			flagError = true;
		}
		printf("\n");

		if(!flagError){
			if(method == "store"){
				auto publicKey = hexToByte(data["params"]["public_key"].get<std::string>());
				auto privateKey = hexToByte(data["params"]["private_key"].get<std::string>());

				if (privateKey.size() != 2 * HALF_OF_KEY) {
					std::cout << "Size of private key is wrong" << std::endl;
				}

				if (publicKey.size() != 2 * HALF_OF_KEY) {
					std::cout << "Size of public key is wrong" << std::endl;
				}

				int sizeCommand = sizeof(auth) + sizeof(authSector2);
				LPBYTE commande = (LPBYTE)malloc(sizeCommand);
				generateCommand(auth, sizeof(auth), authSector2, sizeof(authSector2), commande);
				sendRequest(hCard, commande, sizeCommand, false);

				std::cout << std::endl;

				std::array<uint8_t, HALF_OF_KEY> data;
				std::cout << publicKey.size() / 2;
				for(int i = 0; i < HALF_OF_KEY; ++i){
					data[i] = publicKey[i];
				}

				commande = (LPBYTE)malloc(21);
				generateWriteCommand(0x04, 0x10, data.data(), commande);
				sendRequest(hCard, commande, 21, false);

				std::cout << std::endl;

				for(int i = HALF_OF_KEY; i < HALF_OF_KEY * 2; ++i){
					data[i - HALF_OF_KEY] = publicKey[i];
				}

				commande = (LPBYTE)malloc(21);
				generateWriteCommand(0x05, 0x10, data.data(), commande);
				sendRequest(hCard, commande, 21, false);

				sizeCommand = sizeof(auth) + sizeof(authSector3);
				commande = (LPBYTE)malloc(sizeCommand);
				generateCommand(auth, sizeof(auth), authSector3, sizeof(authSector3), commande);
				sendRequest(hCard, commande, sizeCommand, false);

				std::cout << std::endl;

				std::cout << privateKey.size()/2;
				for(int i = 0; i < HALF_OF_KEY; ++i){
					data[i] = privateKey[i];
				}

				commande = (LPBYTE)malloc(21);
				generateWriteCommand(0x08, 0x10, data.data(), commande);
				sendRequest(hCard, commande, 21, false);

				std::cout << std::endl;

				for(int i = HALF_OF_KEY; i < HALF_OF_KEY * 2; ++i){
					data[i - HALF_OF_KEY] = privateKey[i];
				}

				commande = (LPBYTE)malloc(21);
				generateWriteCommand(0x09, 0x10, data.data(), commande);
				sendRequest(hCard, commande, 21, false);

				result["status"] = "Stored to card";

				std::cout << std::endl;
				free(commande);
			}
			else if(method == "read_public"){
				std::vector<BYTE> publicKey;
				int sizeCommand = sizeof(auth) + sizeof(authSector2);
				LPBYTE commande = (LPBYTE)malloc(sizeCommand);
				generateCommand(auth, sizeof(auth), authSector2, sizeof(authSector2), commande);
				sendRequest(hCard, commande, sizeCommand, false);

				std::cout << std::endl;
				std::cout << "Public key: " << std::endl;
				commande = (LPBYTE)malloc(5);
				generateReadCommand(0x04, 0x10, commande);
				temp = sendRequest(hCard, commande, 5, true);
				if(temp.empty()){
					result["status"] = "Error in reading public key";
					flagError = true;
				}
				else{
					publicKey.insert(publicKey.end(), temp.begin(), temp.end());
				}

				commande = (LPBYTE)malloc(5);
				generateReadCommand(0x05, 0x10, commande);
				temp = sendRequest(hCard, commande, 5, true);
				if(temp.empty()){
					result["status"] = "Error in reading public key";
					flagError = true;
				}
				else{
					publicKey.insert(publicKey.end(), temp.begin(), temp.end());
				}

				if(!flagError) {
					result["status"] = "Readed public key";
					result["key"] = byteToHex(publicKey.data(), publicKey.size());
				}

				std::cout << std::endl;
				free(commande);
			}
			else if(method == "read_private"){
				std::vector<BYTE> privateKey;
				int sizeCommand = sizeof(auth) + sizeof(authSector3);
				LPBYTE commande = (LPBYTE)malloc(sizeCommand);
				generateCommand(auth, sizeof(auth), authSector3, sizeof(authSector3), commande);
				sendRequest(hCard, commande, sizeCommand, false);

				std::cout << std::endl;
				std::cout << "Private key: " << std::endl;
				commande = (LPBYTE)malloc(5);
				generateReadCommand(0x08, 0x10, commande);
				temp = sendRequest(hCard, commande, 5, true);
				if(temp.empty()){
					result["status"] = "Error in reading private key";
					flagError = true;
				}
				else{
					privateKey.insert(privateKey.end(), temp.begin(), temp.end());
				}

				commande = (LPBYTE)malloc(5);
				generateReadCommand(0x09, 0x10, commande);
				temp = sendRequest(hCard, commande, 5, true);
				if(temp.empty()){
					result["status"] = "Error in reading";
				}
				else{
					privateKey.insert(privateKey.end(), temp.begin(), temp.end());
				}

				if(!flagError) {
					result["status"] = "Readed private key";
					result["key"] = byteToHex(privateKey.data(), privateKey.size());
				}

				std::cout << std::endl;
				free(commande);
			}
			else {
				result["status"] = "Method is not available";
			}
		}

		SCardEndTransaction(hCard, SCARD_UNPOWER_CARD);
		#ifdef SCARD_AUTOALLOCATE
			rv = SCardFreeMemory(hContext, mszReaders);

		#else
			free(mszReaders);
		#endif

			SCardReleaseContext(hContext);
	}

    try {
        s->send(hdl, result.dump(), msg->get_opcode());
    } catch (websocketpp::exception const & e) {
		std::cout << "Echo failed because: " << "(" << e.what() << ")" << std::endl;
	}
}

void on_connected(server* s, websocketpp::connection_hdl hdl){
    try {
    	std::cout << "Connected" << std::endl;
        s->send(hdl, "Hello", websocketpp::frame::opcode::value::text);
    } catch (websocketpp::exception const & e) {
        std::cout << "Echo failed because: "
                  << "(" << e.what() << ")" << std::endl;
    }
}

void generateCommand(LPBYTE cmd, int cmdLen, LPBYTE data, int dataLen, LPBYTE readyCommand){
    for(int i = 0; i < cmdLen; ++i){
        readyCommand[i] = cmd[i];
    }
    for(int i = 0; i < dataLen; ++i){
        readyCommand[cmdLen+i] = data[i];
    }
}

std::string byteToHex(uint8_t* data, unsigned long size){
    std::stringstream temp;
    for(int i = 0; i<size; i++){
        if(data[i] < 16) temp << 0 << std::hex << +data[i];
        else temp << std::hex << +data[i];
    }
    return temp.str();
}

std::vector<uint8_t> hexToByte(std::string key) {
    std::vector<uint8_t> bytes;
    for (int i = 0; i * 2 < key.size(); i++)
    {
        char public_buf[3] = {
                key.at(i * 2),
                key.at(i * 2 + 1),'\0'
        };
        bytes.insert(bytes.end(), (uint8_t) strtoul(public_buf, 0, 16));
    }
    return bytes;
}

void showError(DWORD rv){
    switch(rv){
        case SCARD_E_INSUFFICIENT_BUFFER : std::cout << "cbRecvLength was not large enough for the card response. The expected size is now in cbRecvLength" << std::endl; break;
        case SCARD_E_INVALID_HANDLE : std::cout << "Invalid hCard handle" << std::endl; break;
        case SCARD_E_INVALID_PARAMETER : std::cout << "pbSendBuffer or pbRecvBuffer or pcbRecvLength or pioSendPci is null" << std::endl; break;
        case SCARD_E_INVALID_VALUE : std::cout << "Invalid Protocol, reader name, etc" << std::endl; break;
        case SCARD_E_NO_SERVICE : std::cout << "The server is not running" << std::endl; break;
        case SCARD_E_NOT_TRANSACTED : std::cout << "APDU exchange not successful" << std::endl; break;
        case SCARD_E_PROTO_MISMATCH : std::cout << "Connect protocol is different than desired" << std::endl; break;
        case SCARD_E_READER_UNAVAILABLE : std::cout << "The reader has been removed" << std::endl; break;
        case SCARD_F_COMM_ERROR : std::cout << "An internal communications error has been detected" << std::endl; break;
        case SCARD_W_RESET_CARD : std::cout << "The card has been reset by another application" << std::endl; break;
        case SCARD_W_REMOVED_CARD : std::cout << "The card has been removed from the reader" << std::endl; break;
    }
}

std::vector<BYTE> sendRequest(SCARDHANDLE hCard, LPBYTE cmd, int cmdLen, bool key){
    LPBYTE rec = (LPBYTE)malloc(1000);
    DWORD  recLen = 1000;
    std::vector<BYTE> result;
    DWORD rv = SCardTransmit(hCard, SCARD_PCI_T1, cmd, cmdLen, NULL, rec, &recLen);
    if(rv != SCARD_S_SUCCESS){
        showError(rv);
        free(rec);
        return result;
    }

    for(int k = 0; k < recLen; ++k){
        if(k < recLen - 2) {
            printf("%02x ", rec[k]);
            result.insert(result.end(), rec[k]);
        }
        else if(!key){
            printf("%02x ", rec[k]);
            result.insert(result.end(), rec[k]);
        }
    }
    free(rec);

    return result;
}

void generateWriteCommand(BYTE numBlock, BYTE size, unsigned char* data, LPBYTE readyCommand){
    BYTE writeBinary[]  = { 0xFF, 0xD6, 0x00};
    BYTE param[] = { 0x00, 0x00 };
    param[0] = numBlock;
    param[1] = size;
    int comLen = sizeof(writeBinary) + 2;
    LPBYTE comm = (LPBYTE)malloc(comLen);
    LPBYTE dat = (LPBYTE)malloc(size);
    for(int i = 0; i<size; ++i){
        dat[i] = data[i];
    }
    generateCommand(writeBinary, sizeof(writeBinary), param, 2, comm);
    generateCommand(comm, comLen, dat, size, readyCommand);
}

void generateReadCommand(BYTE numBlock, BYTE size, LPBYTE readyCommand){
    BYTE readBinary[] = { 0xFF, 0xB0, 0x00};
    BYTE param[] = { 0x00, 0x00 };
    param[0] = numBlock;
    param[1] = size;
    generateCommand(readBinary, sizeof(readBinary), param, sizeof(param), readyCommand);
}

std::vector<uint8_t> getImage() {
	Scanner scanner;
	return scanner.ScanImage();
}

void preprocessFingerprint(const std::string& bitmap) {
	LOG(bitmap)
	Cwsq cwsq(bitmap);
	cwsq.Execute();

	Mindtct mindtct(bitmap);
	mindtct.Execute();
}

bool compareFingerPrints(const std::vector<uint8_t>& left, const std::vector<uint8_t>& right) {
	auto leftName = Fcmb::genRandomString(20);
	auto rightName = Fcmb::genRandomString(20);

	Fcmb::dumpBytesToFile(left, leftName);
	Fcmb::dumpBytesToFile(right, rightName);

	preprocessFingerprint(leftName);
	preprocessFingerprint(rightName);
	Bozorth3 bozorth3(leftName, rightName);

	remove(leftName.c_str());
	remove(rightName.c_str());

	return bozorth3.Execute() > 40;
}