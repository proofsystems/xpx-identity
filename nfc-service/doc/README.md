#Websockets for communicate with card reader
Service work on 9002 port.

## Store key:
###Request:
```json
  {
      "jsonrpc" : "2.0",
      "method" : "store",
      "params" : {
          "public_key"  : "YOUR_PUBLIC_KEY_TO_STORE",
          "private_key" : "YOUR_PRIVATE_KEY_TO_STORE",
      }
  }
```
###Response:
```json
  {
      "status" : "Stored to card",
  }
```

## Read public key:
###Request:
```json
  {
      "jsonrpc" : "2.0",
      "method"  : "read_public",
  }
```
###Response:
```json
  {
      "status" : "Readed public key",
      "key"    : "YOUR_PUBLIC_KEY",
  }
```

## Read private key:
###Request:
```json
  {
      "jsonrpc" : "2.0",
      "method"  : "read_private",
  }
```
###Response:
```json
  {
      "status" : "Readed private key",
      "key"    : "YOUR_PRIVATE_KEY",
  }
```