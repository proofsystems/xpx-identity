/*
* MIT License
*
* Copyright (c) 2018 Derick Felix <derickfelix@zoho.com>
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
#ifndef __Fcmb__Scanner__
#define __Fcmb__Scanner__

#include <iostream>
#include <vector>
#include <ftrScanAPI.h>

typedef struct tagBITMAPINFOHEADER{
	unsigned int	biSize;
	int			biWidth;
	int			biHeight;
	unsigned short int    biPlanes;
	unsigned short int	biBitCount;
	unsigned int	biCompression;
	unsigned int	biSizeImage;
	int			biXPelsPerMeter;
	int			biYPelsPerMeter;
	unsigned int     biClrUsed;
	unsigned int	biClrImportant;
} BITMAPINFOHEADER, *PBITMAPINFOHEADER;

typedef struct tagRGBQUAD {
	unsigned char	rgbBlue;
	unsigned char	rgbGreen;
	unsigned char	rgbRed;
	unsigned char	rgbReserved;
} RGBQUAD;

typedef struct tagBITMAPINFO {
	BITMAPINFOHEADER        bmiHeader;
	RGBQUAD                         bmiColors[1];
} BITMAPINFO, *PBITMAPINFO;

typedef struct tagBITMAPFILEHEADER {
	unsigned short int	bfType;
	unsigned int	bfSize;
	unsigned short int	bfReserved1;
	unsigned short int	bfReserved2;
	unsigned int	bfOffBits;
} BITMAPFILEHEADER, *PBITMAPFILEHEADER;

class ScannerException {
public:
    ScannerException(const std::string& message)
    : m_message(message)    { }

    std::string what() const { return m_message; }
private:
    std::string m_message;
};

class Scanner {
public:
    Scanner();
    Scanner(const Scanner& scanner) = delete;
    Scanner& operator=(const Scanner& scanner) = delete;
    ~Scanner();

	std::vector<uint8_t> ScanImage();
private:
    void ShowError(unsigned long error);
	std::vector<uint8_t> WriteBmpFile(int width, int height);
private:
    void *m_Device;
    unsigned char *m_Buffer;
    FTRSCAN_IMAGE_SIZE m_ImageSize;
};

#endif /* defined(__Fcmb__Scanner__) */
