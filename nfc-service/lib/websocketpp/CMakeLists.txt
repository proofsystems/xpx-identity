cmake_minimum_required(VERSION 3.2)
project(websocketpp)

file(GLOB websockets_SRC
        "./base/*.hpp"
        "./common/*.hpp"
        "./concurrency/*.hpp"
        "./config/*.hpp"
        "./extensions/*.hpp"
        "./http/*.hpp"
        "./impl/*.hpp"
        "./logger/*.hpp"
        "./message_buffer/*.hpp"
        "./processors/*.hpp"
        "./random/*.hpp"
        "./roles/*.hpp"
        "./sha1/*.hpp"
        "./transport/*.hpp"
        "./*.hpp")
        
add_library(websocketpp STATIC websockets_SRC)
set_property(TARGET websocketpp PROPERTY POSITION_INDEPENDENT_CODE ON)

target_link_libraries(external websocketpp)
