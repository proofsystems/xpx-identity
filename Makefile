run-ubuntu:
	cd ./nfc-service; ./install.sh
	docker-compose build
	docker-compose up -d
	docker exec nfc_service pcscd
run-webapp:
	docker build -t webapp -f ./Dockerfile .
	docker run -d -p 3000:3000 -it webapp
run-macos:
	docker build -t webapp -f ./Dockerfile .
	docker run --name webapp_container -d -p 3000:3000 -it webapp
	cd ./nfc-service/bin/macos/PCSC.app/Contents/MacOS; ./PCSC & >/dev/null 2>&1 &
stop-ubuntu:
	docker-compose down
	docker-compose rm
stop-macos:
	docker stop webapp_container
	docker rm webapp_container
	killall PCSC
