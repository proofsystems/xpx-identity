# How to run
**For run this project you should have "docker", "docker-compose" and "make" installing on your PC**
1. Clone this repository
2. Then open terminal in folder with this project
3. Command to run

Ubuntu:
```bash
sudo make run-ubuntu
```
 Mac OS:
```bash
sudo make run-macos

```
4. Open the browser in localhost:3000
5. Have fun

## Install docker

Mac OS:
https://runnable.com/docker/install-docker-on-macos

Ubuntu:
https://docs.docker.com/install/linux/docker-ce/ubuntu/