FROM node:10-alpine

WORKDIR /app

COPY frontend/tsconfig.json ./frontend/

COPY frontend/package*.json ./frontend/
RUN cd ./frontend && npm install && cd ..

COPY backend/tsconfig.json ./backend/
COPY backend/package*.json ./backend/
COPY backend/controllers ./backend/controllers
COPY backend/models ./backend/models
COPY backend/proximax-sdk ./backend/proximax-sdk
COPY backend/proximax-storage-sdk ./backend/proximax-storage-sdk
COPY backend/node_modules ./backend/node_modules
COPY backend/server.ts ./backend/
RUN mkdir ./backend/files

COPY frontend/public ./frontend/public
COPY frontend/src ./frontend/src

RUN cd ./frontend && npm run build && cd ..
RUN cd ./backend && npm run build && cd ..

EXPOSE 3000

CMD ["node", "./backend/dist/server.js"]